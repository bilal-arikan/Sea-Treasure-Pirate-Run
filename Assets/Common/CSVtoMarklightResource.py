import csv
import sys
import urllib.request
from io import StringIO,BytesIO

langCodes = ["key","en","tr","ja","zh","ko","de","hi","ru","es","ar"]

# 2. argumanı yani verilen dosya yolunu açar
file = open(sys.argv[1],"r",encoding="utf8")
reader = csv.DictReader(file, delimiter=',')

fileXml = open("Localization.xml","w",encoding="utf8")
fileVariables = open("L.cs","w",encoding="utf8")

print("Languages:")
print(langCodes)

fileXml.write('<ResourceDictionary Name="Loc" xmlns="MarkLight">\n')
fileVariables.write('public static partial class L {\n')

for row in reader:
    fileXml.write('\t<ResourceGroup Key="' + row[langCodes[0]] + '">\n')
    fileVariables.write('public static string '+ row[langCodes[0]] +'{get{return GetText("'+ row[langCodes[0]] +'");}}\n')

    for i in range( 1,11 ):
        #print(langCodes[i])
        fileXml.write('\t\t<Resource Language="' + langCodes[i] + '" Value="' + row[langCodes[i]] + '"/>\n')

    fileXml.write('\t</ResourceGroup>\n')

fileXml.write('</ResourceDictionary>\n')
fileVariables.write('}')
fileXml.close()
fileVariables.close()
print("SUCCESS")