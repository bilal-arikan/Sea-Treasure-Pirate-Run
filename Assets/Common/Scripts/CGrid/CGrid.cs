﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Common.Tools;

namespace Common.PathFinding
{
    [ExecuteInEditMode]
    [RequireComponent(typeof(SpriteRenderer))]
    public class CGrid : Singleton<CGrid>
    {

        public const float ToSquare = 1.4142135623f;

        [Header("Input Settings")]
        public bool ShowNodes = true;
        public float UnitSize = 3f;

        //[Space(25)]
        public float UnitSizeDiagonal
        {
            get
            {
                return UnitSize * ToSquare;
            }
        }
        [Header("Result Settings")]

        public int Width;
        public int Height;
        public Node[,] Nodes;
        public Vector2 TopRight;
        public Vector2 TopLeft;
        public Vector2 BottomLeft;
        public Vector2 BottomRight;
        Renderer rend; // Bounds için


        protected override void Awake()
        {
            base.Awake();
            rend = transform.GetComponent<SpriteRenderer>();
        }

        private void OnEnable()
        {
            TopRight = (rend.bounds.center + rend.bounds.extents);
            BottomLeft = (rend.bounds.center - rend.bounds.extents);
            TopLeft = TopRight - new Vector2(rend.bounds.size.x, 0);
            BottomRight = BottomLeft + new Vector2(rend.bounds.size.x, 0);


            Width = Mathf.Abs((int)((TopRight - BottomLeft).x / UnitSize));
            Height = Mathf.Abs((int)((TopRight - BottomLeft).y / UnitSize));
            //Debug.Log("W:" + Width + " H:" + Height);
            Nodes = new Node[Width, Height];

            for (int x = 0; x < Width; x++)
            {
                for (int y = 0; y < Height; y++)
                {
                    Vector2 pointWorldPos = BottomLeft + new Vector2(x * UnitSize + UnitSize / 2, y * UnitSize + UnitSize / 2);

                    Node node = new Node(x, y, pointWorldPos, this);
                    //node.Accesible = UnityEngine.Random.value > 0.3f;
                    Nodes[x, y] = node;
                }
            }
            foreach (Node n in Nodes)
            {
                // Initialize Neighbours
                n.Initialize();
            }

            if (Application.isPlaying)
                rend.enabled = false;

            //Debug.Log(TopRight + " : " + BottomLeft + " : " + TopLeft + " : " + BottomRight);
            //Debug.Log("Sum:" + Nodes.Length + " Bound:" + rend.bounds);
        }

        public Node WorldToGrid(Vector2 pos)
        {
            Vector2 distance = pos - BottomLeft;

            int X = (int)(distance.x / UnitSize);
            int Y = (int)(distance.y / UnitSize);

            //Debug.Log("WtoG  " + X + ":" + Y + "  :" + distance + ":" + Nodes.Length);
            return GetNode(X, Y);
        }

        public Vector2? GridToWorld(int X, int Y)
        {
            Node n = GetNode(X, Y);

            if (n != null)
                return n.Position;
            else
                return null;
        }

        public Node GetNode(int x, int y)
        {
            if (x < 0 || x >= Width || y < 0 || y >= Height)
            {
                return null;
            }
            else
                return Nodes[x, y];
        }




        public void OnDrawGizmos()
        {
            Gizmos.color = Color.red;
            /*Gizmos.DrawLine(TopLeft, BottomLeft);
            Gizmos.DrawLine(TopRight, BottomRight);*/

            Gizmos.DrawCube(new Vector3(TopLeft.x, TopLeft.y, transform.position.z), Vector3.one);
            Gizmos.DrawCube(new Vector3(TopRight.x, TopRight.y, transform.position.z), Vector3.one);
            Gizmos.DrawCube(new Vector3(BottomLeft.x, BottomLeft.y, transform.position.z), Vector3.one);
            Gizmos.DrawCube(new Vector3(BottomRight.x, BottomRight.y, transform.position.z), Vector3.one);

            if (ShowNodes && Nodes != null)
            {
                Gizmos.color = Color.white;
                // Dikey Çizgiler
                for(int i = 0; i<=Width; i++)
                {
                    Gizmos.DrawLine(
                        new Vector3(BottomLeft.x + (i* UnitSize),BottomLeft.y,transform.position.z),
                        new Vector3(TopLeft.x + (i * UnitSize), TopLeft.y, transform.position.z) );
                }
                // Yatay Çizgiler
                for (int j = 0; j <= Height; j++)
                {
                    Gizmos.DrawLine(
                        new Vector3(BottomLeft.x, BottomLeft.y + (j * UnitSize), transform.position.z),
                        new Vector3(BottomRight.x, BottomRight.y + (j * UnitSize), transform.position.z));
                }

                /*foreach (Node n in Nodes)
                {
                    Gizmos.DrawCube(n.Position, Vector3.one * NodeBoxSize);
                }*/
            }
            
        }

        // Debug Text çizmek için
        /*static public void DebugDrawrawString(string text, Vector3 worldPos, Color colour)
        {
            UnityEditor.Handles.BeginGUI();

            var restoreColor = GUI.color;

            GUI.color = colour;
            var view = UnityEditor.SceneView.currentDrawingSceneView;
            Vector3 screenPos = view.camera.WorldToScreenPoint(worldPos);
            //Vector3 screenPos = Camera.main.WorldToScreenPoint(worldPos);

            if (screenPos.y < 0 || screenPos.y > Screen.height || screenPos.x < 0 || screenPos.x > Screen.width || screenPos.z < 0)
            {
                GUI.color = restoreColor;
                UnityEditor.Handles.EndGUI();
                return;
            }

            Vector2 size = GUI.skin.label.CalcSize(new GUIContent(text));
            GUI.Label(new Rect(screenPos.x - (size.x / 2), -screenPos.y + view.position.height + 4, size.x, size.y), text);
            GUI.color = restoreColor;
            UnityEditor.Handles.EndGUI();
        }*/
    }
}
