﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using Common.Tools;
using System.Collections;

namespace Common.PathFinding
{
    public class PathFinder : Singleton<PathFinder>
    {
        public CGrid GridToFollow;
        public LineRenderer LineRend;

        public bool ShowAccesibles = true;
        public bool ShowUnAccesibles = true;
        public float NodeLength = 5;
        public int CoroutineStepMode = 60;
        public bool IsCalculating = false;
        private bool DisableCalculating = false;
        private bool ReCalculate = false;

        //public List<Type> BlockObjectTypes = new List<Type>() { typeof(MiceCompany.Wall), typeof(MiceCompany.Damaging), typeof(Placable) };
        Node StartTestNode;
        public GameObject TargetObject;
        public LayerMask DetectWallsLayerMask;
        Node TargetObjectNode;
        List<Node> AllMapAccesiables = new List<Node>();
        List<Node> AllUnAccesiables = new List<Node>();

        void Start()
        {
            CalculateAllPathsToTarget();
        }

        void Update()
        {
            //Pathfinding demo
            /*if (Input.GetKeyDown(KeyCode.Mouse1))
            {
                if (TargetObject != null)
                {
                    //Convert mouse click point to grid coordinates
                    Vector2 worldPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                    StartTestNode = GridToFollow.WorldToGrid(worldPos);
                    TargetObjectNode = GridToFollow.WorldToGrid(TargetObject.transform.position);

                    if (TargetObjectNode != null && StartTestNode != null)
                    {
                        List<Node> path = FindWayToTarget(StartTestNode);
                        DrawPath(path);
                    }
                    else
                    {
                        DrawPath(null);
                    }
                }
                else
                {
                    Debug.LogError("Target belirlenmeli");
                }
            }*/
        }

        public void CalculateAllPathsToTarget()
        {
            if (IsCalculating)
            {
                DisableCalculating = true;
                ReCalculate = true;
                return;
            }

            if (TargetObject == null)
            {
                Debug.LogWarning("PathFinder: Target Null !!!");
                return;
            }

            TargetObjectNode = GridToFollow.WorldToGrid(TargetObject.transform.position);
            if (TargetObjectNode != null)
            {
                foreach (Node n in GridToFollow.Nodes)
                    n.Accesible = true;

                IsCalculating = true;
                StartCoroutine(FindAllAccesibles(TargetObjectNode));
            }
        }

        public Direction? FindDirectionToTarget(Vector2 start)
        {
            if (AllMapAccesiables == null)
            {
                throw new UnityException("PathFinder: Haritadaki Nodelar hesaplanmamış !!!");
            }

            Node CurrentN = GridToFollow.WorldToGrid(start);

            if (AllMapAccesiables.Contains(CurrentN))
                return CurrentN.ToTargetDirection;
            else
                return null;
        }

        public List<Node> FindWayToTarget(Vector2 start)
        {
            Node CurrentN = GridToFollow.WorldToGrid(start);
            Node TargetN = GridToFollow.WorldToGrid(TargetObject.transform.position);

            if (TargetObject != null && TargetN != null && CurrentN != null)
            {
                return FindWayToTarget(CurrentN);
            }
            else
            {
                return null;
            }

        }

        public List<Node> FindWayToTarget(Node start)
        {
            if (AllMapAccesiables == null)
            {
                Debug.LogError("# Haritadaki Nodelar hesaplanmamış !!!");
                return null;
            }
            List<Node> path = new List<Node>();

            if (AllMapAccesiables.Contains(start))
            {
                path.Add(start);
                Node current = start.ToTargetNode;
                while (current != null)
                {
                    path.Add(current);
                    current = current.ToTargetNode;
                }
                return path;
            }
            else
            {
                Debug.Log("# Yol Bulunamadı");
                return null;
            }
        }

        public IEnumerator FindAllAccesibles(Node target)
        {
            List<Node> AccesibleNodes = new List<Node>();
            AccesibleNodes.Add(target);

            RaycastHit2D[] hits;


            for (int i = 0; i < AccesibleNodes.Count; i++)// Accesible.Count sürekli artar
            {
                // Nodemiz Blocke eden bir nesnenin içindemi
                hits = Physics2D.RaycastAll(AccesibleNodes[i].Position, Vector2.zero,DetectWallsLayerMask.value); //MapItemLayer
                foreach (RaycastHit2D h in hits)
                    // obje Collisionsa ve RigidBodysi var ve Staticse blocking objedir
                    if (!h.collider.isTrigger && (h.rigidbody == null || h.rigidbody.bodyType == RigidbodyType2D.Static))
                    {
                        AccesibleNodes[i].Accesible = false;
                        goto End;
                    }
                End:

                if (AccesibleNodes[i].Accesible)
                {
                    for (int j = 0; j < 8; j++)// Her Accesible için çevresine bakılır
                    {
                        if (AccesibleNodes[i].Around[j] != null) // Nullsa (sınırdışı gibi) eklenmez
                        {
                            if (!AccesibleNodes.Contains(AccesibleNodes[i].Around[j])) // Zaten (Accesiblelara) eklendiyse tekrar eklenmemesi için
                            {
                                if (AccesibleNodes[i].Around[j].Accesible) // Erişilemiyosa (Duvarın içinde olması gibi) eklenmez
                                {
                                    //Debug.Log(i + ". " + AccesibleNodes[i].AroundDirections[j]);


                                    hits = Physics2D.RaycastAll(
                                        AccesibleNodes[i].Position, // Current Node positionu alınır
                                        Directions.Vectors[AccesibleNodes[i].AroundDirections[j]], // Directiona göre Vector2 alınır
                                        Vector2.Distance(AccesibleNodes[i].Position, AccesibleNodes[i].Around[j].Position),
                                        DetectWallsLayerMask.value);
                                    foreach (RaycastHit2D h in hits)
                                    {
                                        //foreach (Type t in BlockObjectTypes)
                                        //{
                                        // BlockObjecsi varsa 
                                        if (!h.collider.isTrigger && ( h.rigidbody == null || h.rigidbody.bodyType == RigidbodyType2D.Static))
                                        {
                                            goto End2;

                                            /*// ve hedef ile Node arasındaysa
                                            if (h.collider.OverlapPoint(AccesibleNodes[i].Position))
                                            {
                                            }*/

                                        }
                                        //}
                                        //Debug.Log(i + ". ray trace !!!");
                                    }
                                    // Eğer kod buraya kadar geldiyse AccesibleNodesa ekle
                                    AccesibleNodes[i].Around[j].ToTargetNode = AccesibleNodes[i]; //Hedefe giderken takip edilmesi gereken Node
                                    AccesibleNodes[i].Around[j].ToTargetDirection = Directions.Reverse(AccesibleNodes[i].AroundDirections[j]);
                                    AccesibleNodes.Add(AccesibleNodes[i].Around[j]); // Erişilebilir Nodelere ekle

                                    // AccesibleNodesa ekleme
                                    End2:;
                                }
                            }
                            else
                            {
                                //Debug.Log(i + ". Mevcut");
                            }
                        }
                        else
                        {
                            //Debug.Log(i + ". Null "+ i+":"+j + " "+ AccesibleNodes[i].Around.Count(t => t != null));
                        }
                    }

                    /*int accessCountFromAround = 0;
                    foreach (Node n in AccesibleNodes[i].Around)
                    {
                        if (n.ToTargetNode != null && n.ToTargetNode == AccesibleNodes[i])
                            accessCountFromAround++;
                    }
                    if(accessCountFromAround < 2)
                    {
                        Debug.Log("Atıldı " + accessCountFromAround);
                        AccesibleNodes.Remove(AccesibleNodes[i]);
                        AccesibleNodes[i].Accesible = false;
                    }*/

                }
                if (i % CoroutineStepMode == 0)
                {
                    if (DisableCalculating)
                    {
                        DisableCalculating = false;
                        Debug.Log("Calculating Canceled");

                        if (ReCalculate)
                        {
                            ReCalculate = false;
                            IsCalculating = false;
                            Debug.Log("Recalculating...");
                            CalculateAllPathsToTarget();
                        }

                        goto CancelCalculating;
                    }
                    yield return null;
                }
            }


            //Debug.Log(AccesibleNodes.Count + " Count");
            //Debug.Log(GridToFollow.Width * GridToFollow.Height + " Count");

            AllMapAccesiables = AccesibleNodes.FindAll(n => n.Accesible);

            foreach (Node n in CGrid.Instance.Nodes)
                if (!n.Accesible)
                    AllUnAccesiables.Add(n);
            AllUnAccesiables.AddRange(AccesibleNodes.FindAll(n => !n.Accesible));

            Debug.Log("PathFinder: All Paths Calculated");
            IsCalculating = false;

            CancelCalculating:;

            //return AccesibleNodes.FindAll(n => n.Accesible);
        }


        public void DrawPath(List<Node> path)
        {
            if (LineRend != null && path != null)
            {
                LineRend.startColor = Color.yellow;
                LineRend.endColor = Color.cyan;
                LineRend.positionCount = path.Count;
                for (int i = 0; i < path.Count; i++)
                {
                    LineRend.SetPosition(i, path[i].Position);
                }
            }
            else
            {
                Debug.Log("LineRenderer yada Path Null !!!");
            }
        }

        public void OnDrawGizmos()
        {
            if (GridToFollow != null && GridToFollow.Nodes != null && (ShowAccesibles || ShowUnAccesibles))
            {
                if (IsCalculating)
                {
                    foreach (Node n in GridToFollow.Nodes)
                        if (ShowUnAccesibles && !n.Accesible)
                        {
                            Gizmos.color = Color.red;
                            Gizmos.DrawRay(n.Position, Directions.Vectors[n.ToTargetDirection] * NodeLength);
                        }
                        else if (ShowUnAccesibles && n.Accesible)
                        {
                            Gizmos.color = Color.yellow;
                            Gizmos.DrawRay(n.Position, Directions.Vectors[n.ToTargetDirection] * NodeLength);
                        }
                }
                else
                {
                    foreach (Node n in AllMapAccesiables)
                    {
                        if (ShowAccesibles)
                        {
                            Gizmos.color = Color.blue;
                            Gizmos.DrawRay(n.Position, Directions.Vectors[n.ToTargetDirection] * NodeLength);
                        }
                    }
                    foreach (Node n in AllUnAccesiables)
                    {
                        if (ShowUnAccesibles && !n.Accesible)
                        {
                            Gizmos.color = Color.red;
                            Gizmos.DrawRay(n.Position, Directions.Vectors[n.ToTargetDirection] * NodeLength);
                        }
                        else if (ShowUnAccesibles && n.Accesible)
                        {
                            Gizmos.color = Color.yellow;
                            Gizmos.DrawRay(n.Position, Directions.Vectors[n.ToTargetDirection] * NodeLength);
                        }
                    }
                }

            }

        }
    }
}
