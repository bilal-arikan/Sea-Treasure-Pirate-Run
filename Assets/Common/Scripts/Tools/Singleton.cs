//***********************************************************************//
// Copyright (C) 2017 Bilal Ar�kan. All Rights Reserved.
// Author: Bilal Ar�kan
// Time  : 04.11.2017   
//***********************************************************************//
using UnityEngine;

/// <summary>
/// Singleton pattern.
/// </summary>
//[ExecuteInEditMode]
public class Singleton<T> : MonoBehaviour where T : Component
{
    protected static T _instance;

    /// <summary>
    /// Singleton design pattern
    /// </summary>
    /// <value>The instance.</value>
    public static T Instance
    {
        get
        {
            /*if (_instance == null)
            {
                //_instance = FindObjectOfType<T> ();
                if (_instance == null)
                {
                    GameObject obj = new GameObject ("_"+typeof(T).Name);
                    //obj.hideFlags = HideFlags.HideAndDontSave;
                    _instance = obj.AddComponent<T> ();
                }
            }*/
            return _instance;
        }
    }

    public Singleton() : base()
    {
        _instance = this as T;
    }

    /// <summary>
    /// On awake, we initialize our instance. Make sure to call base.Awake() in override if you need awake.
    /// </summary>
    protected virtual void Awake()
    {
        if (_instance != null && _instance != this && Application.isPlaying)
            // Kendini yok et
            Destroy(gameObject);
        else
            _instance = this as T;
    }
}
