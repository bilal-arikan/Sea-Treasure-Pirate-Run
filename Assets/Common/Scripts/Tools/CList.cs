﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Common
{
    [Serializable]
    public class CList
    {

    }

    [Serializable]
    public class CList<TKey> : CList, /*ISerializationCallbackReceiver,*/
        IList<TKey>, ICollection<TKey>, IEnumerable<TKey>, IEnumerable
    {
        /// <summary>
        /// The Actual HashSet object instance
        /// you don't have to access this field to use your HashSet
        /// </summary>
        //public List<TKey> Instance = new List<TKey>();
        public List<TKey> Instance
        {
            get
            {
                return _key;
            }
        }

        [SerializeField]
        List<TKey> _key = new List<TKey>();

        /// <summary>
        /// Field for telling the inspector what kind of type the key values are
        /// intentionally unused
        /// </summary>
        [SerializeField]
        TKey _keyType;

        [SerializeField]
        int _selected;
        public int Selected
        {
            get
            {
                return _selected;
            }
        }

        [SerializeField]
        bool reset;

        /*public static implicit operator Array(CList<TKey> from)
        {
            return from.Instance.ToArray();
        }

        void ISerializationCallbackReceiver.OnBeforeSerialize()
        {
            _key.Clear();
            _key.AddRange(Instance);
        }

        void ISerializationCallbackReceiver.OnAfterDeserialize()
        {
            if (reset)
            {
                _key.Clear();
                reset = false;
            }
            Instance.Clear();
            Instance.AddRange(_key);
        }*/

        #region HashSetAPI
        public int Count
        {
            get
            {
                return (Instance).Count;
            }
        }

        public bool IsReadOnly
        {
            get
            {
                return false;
            }
        }

        public TKey this[int index]
        {
            get
            {
                return (Instance)[index];
            }
            set
            {
                (Instance)[index] = value;
            }
        }

        public void Add(TKey item)
        {
            (Instance).Add(item);
        }

        public void Clear()
        {
            (Instance).Clear();
        }

        public bool Contains(TKey item)
        {
            return (Instance).Contains(item);
        }

        public void CopyTo(TKey[] array, int arrayIndex)
        {
            (Instance).CopyTo(array, arrayIndex);
        }

        public bool Remove(TKey item)
        {
            return (Instance).Remove(item);
        }

        public IEnumerator<TKey> GetEnumerator()
        {
            return (Instance).GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return (Instance).GetEnumerator();
        }

        public int IndexOf(TKey item)
        {
            return (Instance).IndexOf(item);
        }

        public void Insert(int index, TKey item)
        {
            (Instance).Insert(index, item);
        }

        public void RemoveAt(int index)
        {
            (Instance).RemoveAt(index);
        }
        #endregion

    }
}
