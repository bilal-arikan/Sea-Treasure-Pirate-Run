﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Common
{
    [Serializable]
    public class CArray
    {

    }

    [Serializable]
    public class CArray<TKey> : CArray, /*ISerializationCallbackReceiver,*/ IEnumerable
        ,IEnumerable<TKey>, ICollection<TKey>
    {
        /// <summary>
        /// The Actual HashSet object instance
        /// you don't have to access this field to use your HashSet
        /// </summary>
        public TKey[] Instance
        {
            get
            {
                return _key;
            }
        }


        [SerializeField]
        TKey[] _key = new TKey[0];

        /// <summary>
        /// Field for telling the inspector what kind of type the key values are
        /// intentionally unused
        /// </summary>
        [SerializeField]
        TKey _keyType;

        [SerializeField]
        int _selected;
        public int Selected
        {
            get
            {
                return _selected;
            }
        }

        [SerializeField]
        bool reset;

        public static implicit operator Array(CArray<TKey> from)
        {
            return from.Instance;
        }

        /*void ISerializationCallbackReceiver.OnBeforeSerialize()
        {
            if (_keyArray.Length != _key.Count)
            {
                _keyArray = _key.ToArray();
            }
        }

        void ISerializationCallbackReceiver.OnAfterDeserialize()
        {
            if (_keyArray.Length != _key.Count)
            {
                _keyArray = _key.ToArray();
            }
        }*/

        #region HashSetAPI
        public int Count
        {
            get
            {
                return (Instance).Length;
            }
        }
        public int Length
        {
            get
            {
                return (Instance).Length;
            }
        }

        public bool IsReadOnly
        {
            get
            {
                return false;
            }
        }

        public TKey this[int index]
        {
            get
            {
                return (Instance)[index];
            }
            set
            {
                (Instance)[index] = value;
            }
        }

        public bool Contains(TKey item)
        {
            return (Instance).Contains(item);
        }

        public void CopyTo(TKey[] array, int arrayIndex)
        {
            (Instance).CopyTo(array, arrayIndex);
        }


        public IEnumerator GetEnumerator()
        {
            return (Instance).GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return (Instance).GetEnumerator();
        }

        public int IndexOf(TKey item)
        {
            return (Instance).IndexOf(item);
        }

        IEnumerator<TKey> IEnumerable<TKey>.GetEnumerator()
        {
            return (Instance).AsEnumerable<TKey>().GetEnumerator();
        }

        public void Add(TKey item)
        {
            Array.Resize(ref _key, _key.Length + 1);
            _key[_key.Length - 1] = item;
        }

        public void Clear()
        {
            Array.Resize(ref _key,0);
        }

        public bool Remove(TKey item)
        {
            int index = _key.IndexOf(item);
            if (index < 0)
                return false;
            else
            {
                for (int i = index; i < _key.Length - 1; i++)
                    _key[i] = _key[i + 1];
                Array.Resize(ref _key, _key.Length - 1);
                return true;
            }
        }

        #endregion

    }
}
