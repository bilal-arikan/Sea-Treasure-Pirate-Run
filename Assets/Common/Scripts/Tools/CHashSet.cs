﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using UnityEngine;

namespace Common
{
    [Serializable]
    public class CHashSet
    {

    }

    [Serializable]
    public class CHashSet<TKey> : CHashSet, ISerializationCallbackReceiver, 
        ICollection<TKey>, IEnumerable<TKey>, IEnumerable
    {
        /// <summary>
        /// The Actual HashSet object instance
        /// you don't have to access this field to use your HashSet
        /// </summary>
        public List<TKey> Instance
        {
            get
            {
                return _key;
            }
        }
        [SerializeField]
        List<TKey> _key = new List<TKey>();
        [SerializeField]
        List<int> _errorList = new List<int>();

        /// <summary>
        /// Field for telling the inspector what kind of type the key values are
        /// intentionally unused
        /// </summary>
        [SerializeField]
        TKey _keyType;

        [SerializeField]
        int _selected;
        public int Selected
        {
            get
            {
                return _selected;
            }
        }

        [SerializeField]
        bool reset;

        void ISerializationCallbackReceiver.OnBeforeSerialize()
        {
            if (_errorList.Count == 0)
            {
                //break dict into 2 list, then serialize 2 list
                _key.Clear();
                foreach (var hs in Instance)
                {
                    _key.Add(hs);
                }
            }
        }

        void ISerializationCallbackReceiver.OnAfterDeserialize()
        {
            if (reset)
            {
                _key.Clear();
                _errorList.Clear();
                reset = false;
            }
            Instance.Clear();

            int count = _key.Count;

            //first pass, looking for duplicates
            _errorList.Clear();
            var duplicateItems = from x in _key
                                 group x by x into grouped
                                 where grouped.Count() > 1
                                 select grouped.Key;
            var list = duplicateItems.ToList();

            foreach (var k in list)
            {
                var res = Enumerable.Range(0, _key.Count)
                                     .Where(i => EqualityComparer<TKey>.Default.Equals(_key.ElementAt(i), k))
                                     .ToList();
                _errorList.AddRange(res);
            }

            //second pass, look for null keys
            for (int i = 0; i < count; i++)
            {
                if (_key.ElementAt(i) == null)
                {
                    _errorList.Add(i);
                }

            }

            // no errors, populate our dict;
            for (var i = 0; i < count; i++)
            {
                if (_key.ElementAt(i) == null)
                {
                    _errorList.Add(i);
                }
                else
                {
                    if (!_errorList.Contains(i))
                        Instance.Add(_key.ElementAt(i));

                }
            }
        }

        #region HashSetAPI
        public TKey this[int index]
        {
            get
            {
                return (Instance).ElementAt(index);
            }
        }

        public int Count
        {
            get
            {
                return (Instance).Count;
            }
        }

        public bool IsReadOnly
        {
            get
            {
                return false;
            }
        }



        public void Add(TKey item)
        {
            (Instance).Add(item);
        }

        public void Clear()
        {
            (Instance).Clear();
        }

        public bool Contains(TKey item)
        {
            return (Instance).Contains(item);
        }

        public void CopyTo(TKey[] array, int arrayIndex)
        {
            (Instance).CopyTo(array, arrayIndex);
        }

        public bool Remove(TKey item)
        {
            return (Instance).Remove(item);
        }

        public IEnumerator<TKey> GetEnumerator()
        {
            return (Instance).GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return (Instance).GetEnumerator();
        }
        #endregion

    }
}
