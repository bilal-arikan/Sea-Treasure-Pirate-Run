﻿//***********************************************************************//
// Copyright (C) 2017 Bilal Arıkan. All Rights Reserved.
// Author: Bilal Arıkan
// Time  : 04.11.2017   
//***********************************************************************//
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Text;
using Common.Managers;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

namespace Common.Tools
{
    /// <summary>
    /// Belli bir replay kaydına ait Frameleri tutar
    /// </summary>
    [Serializable]
    public class ReplayData
    {

        public string Id;
        public long Size = 0;
        public bool IsRecording = false;
        public float RecordStartTime;
        public float TotalTime
        {
            get
            {
                return Frames[Frames.Count-1].Time;
            }
        }
        public List<ReplayFrame> Frames = new List<ReplayFrame>();

        /// <summary>
        /// Bellekteki boyutu
        /// </summary>
        /// <returns></returns>
        public long GetMemorySize()
        {
            using (Stream s = new MemoryStream())
            {
                BinaryFormatter formatter = new BinaryFormatter();
                formatter.Serialize(s, this);
                Size = s.Length;
            }
            return Size;
        }

        public ReplayFrame GetFrameValues(float time)
        {
            int i = FindNearestBottomFrame(time);
            if (i > -1)
                return ReplayFrame.Interpolate(Frames[i], Frames[i+1],time);
            else
                return null;
        }

        /// <summary>
        /// Binary search kullanarak arama yapıyor
        /// </summary>
        /// <param name="time"></param>
        /// <returns></returns>
        int FindNearestBottomFrame(float time)
        {
            if (Frames[Frames.Count - 1].Time < time)
                return -1;

            int possibleTopIndex = Frames.Count - 1;
            int possibleBottomIndex = 0;
            int possibleHalf;

            while (true)
            {
                possibleHalf = possibleBottomIndex +((possibleTopIndex - possibleBottomIndex) / 2);
                if (Frames[possibleHalf].Time == time)
                {
                    return possibleHalf;
                }
                else if(Frames[possibleHalf].Time < time)
                {
                    possibleTopIndex = possibleHalf;
                }
                else
                {
                    possibleBottomIndex = possibleHalf;
                }


                if (Frames[possibleBottomIndex].Time <= time && time < Frames[possibleBottomIndex + 1].Time)
                {
                    return possibleBottomIndex;
                }
            }
        }
    }
}
