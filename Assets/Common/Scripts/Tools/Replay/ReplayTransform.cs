﻿//***********************************************************************//
// Copyright (C) 2017 Bilal Arıkan. All Rights Reserved.
// Author: Bilal Arıkan
// Time  : 04.11.2017   
//***********************************************************************//
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Text;
using Common.Managers;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

namespace Common.Tools
{
    public class ReplayTransform : MonoBehaviour
    {
        public bool IsRecording
        {
            get
            {
                return Replays.TrueForAll(k => k.IsRecording == false);
            }
        }
        public bool IsPlaying
        {
            get
            {
                return CurrentlyPlaying != null;
            }
        }

        /// <summary>
        /// ReplayTransforma ait bütün Replaylar burada tutulur
        /// </summary>
        public List<ReplayData> Replays = new List<ReplayData>();
        /// <summary>
        /// Kayıt tamamlandığında çağrılır
        /// </summary>
        Action<ReplayData> OnRecordingStopped;
        /// <summary>
        /// Oynatma işi tamamlandığında çağrılır
        /// </summary>
        Action<ReplayData> OnPlayingStopped;
        /// <summary>
        /// PlayReplay() çağırılınca oynatılacak Replay buraya kaydedilir
        /// </summary>
        ReplayData CurrentlyPlaying = null;
        /// <summary>
        /// PlayReplay() çağırılınca başlangıç süresini kaydeder
        /// </summary>
        float PlayStartTime;
        /// <summary>
        /// Kayıt hassaslığı (Her RecordFPS frame de 1 kayıt eder)
        /// </summary>
        ulong RecordFPS = 5;


        void Update()
        {
            if (CurrentlyPlaying != null)
            {
                ReplayFrame frame = CurrentlyPlaying.GetFrameValues(Time.time - PlayStartTime);
                if(frame != null)
                {
                    transform.position = frame.Position;
                    transform.rotation = frame.Rotation;
                }
                else
                {
                    if(OnPlayingStopped != null)
                        OnPlayingStopped.Invoke(CurrentlyPlaying);
                    CurrentlyPlaying = null;
                }
            }
        }

        public ReplayData StartRecording()
        {
            ReplayData newReplay = new ReplayData();
            Replays.Add(newReplay);

            newReplay.IsRecording = true;
            newReplay.RecordStartTime = Time.time;

            if (transform != null)
                StartCoroutine(RecordCoroutine(newReplay));
            else
                Debug.LogError("Object NULL");

            return newReplay;
        }

        public void StopRecording(ReplayData toStop,Action<ReplayData> onStopped)
        {
            OnRecordingStopped = onStopped;
            toStop.IsRecording = false;
        }

        IEnumerator RecordCoroutine(ReplayData current)
        {

            ulong i = 0;
            while (current.IsRecording)
            {
                if (i == ulong.MaxValue)
                    i = 0;

                //Hassalık ayarı
                if(i % RecordFPS == 0)
                {
                    current.Frames.Add(new ReplayFrame()
                    {
                        Time = Time.time - current.RecordStartTime,
                        Position = transform.position,
                        Rotation = transform.rotation
                    });
                }


                i++;
            }
            yield return null;

            UIManager.Instance.ShowToast("ReplaySize: " + current.GetMemorySize());
            if(OnRecordingStopped != null)
                OnRecordingStopped.Invoke(current);
        }

        public void PlayReplay(ReplayData data, Action<ReplayData> onStopped)
        {
            if (IsRecording)
                Debug.LogError("Already a Record is saving data !!!");
            OnPlayingStopped = onStopped;
            PlayStartTime = Time.time;
            CurrentlyPlaying = data;
        }
    }
}
