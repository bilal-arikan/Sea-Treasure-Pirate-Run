﻿//***********************************************************************//
// Copyright (C) 2017 Bilal Arıkan. All Rights Reserved.
// Author: Bilal Arıkan
// Time  : 04.11.2017   
//***********************************************************************//
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Common.Tools
{
    [Serializable]
    public class ReplayFrame
    {
        public float Time;
        public float X;
        public float Y;
        public float Z;
        public float rotW;
        public float rotX;
        public float rotY;
        public float rotZ;

        public Vector3 Position
        {
            get
            {
                return new Vector3(X, Y, Z);
            }
            set
            {
                X = value.x;
                Y = value.y;
                Z = value.z;
            }
        }

        public Quaternion Rotation
        {
            get
            {
                return new Quaternion(rotX, rotY, rotZ, rotW);
            }
            set
            {
                rotX = value.x;
                rotY = value.y;
                rotZ = value.z;
                rotW = value.w;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="f1"></param>
        /// <param name="f2"></param>
        /// <param name="time">Time must be from start</param>
        /// <returns></returns>
        public static ReplayFrame Interpolate(ReplayFrame f1,ReplayFrame f2, float time)
        {
            if(time < f1.Time || time > f2.Time)
            {
                Debug.LogError("ReplayFrame Interpolate: Over:");
                return null;
            }

            float alpha = (f2.Time - f1.Time) / (time - f1.Time);

            ReplayFrame newFrame = new ReplayFrame();
            newFrame.Time = time;
            newFrame.Position = Vector3.Lerp(f1.Position, f2.Position, alpha);
            newFrame.Rotation = Quaternion.Slerp(f1.Rotation, f2.Rotation, alpha);
            return newFrame;
        }
    }
}
