﻿using System;
using System.Collections.Generic;

[Serializable]
public struct Point2 : IEquatable<Point2>
{
    [Newtonsoft.Json.JsonConstructor]
    public Point2(int x, int y)
    {
        this.x = x;
        this.y = y;
    }

    public int x;
    public int y;

    public override string ToString()
    {
        return x.ToString("00") + y.ToString("00");
    }

    public override int GetHashCode()
    {
        return x.GetHashCode() ^ y.GetHashCode();
    }

    public static Point2 FromString(string s)
    {
        Point2 p = new Point2();
        p.x = int.Parse(s.Substring(0, 2));
        p.y = int.Parse(s.Substring(2, 2));
        return p;
    }

    public override bool Equals(object obj)
    {
        return obj is Point2 && Equals((Point2)obj);
    }

    public bool Equals(Point2 other)
    {
        return x == other.x && y == other.y;
    }

    public static bool operator ==(Point2 first, Point2 second)
    {
        return second.x == first.x && second.y == first.y;
    }
    public static bool operator !=(Point2 first, Point2 second)
    {
        return second.x != first.x || second.y != first.y;
    }
}
