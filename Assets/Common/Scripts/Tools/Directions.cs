﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Common.Tools
{
    public static class Directions
    {
        public static Dictionary<Direction, Vector2> Vectors = new Dictionary<Direction, Vector2>()
        {
            { Direction.Right,new Vector2(1,0) },
            { Direction.TopRight,new Vector2(1,1) },
            { Direction.Top,new Vector2(0,1) },
            { Direction.TopLeft,new Vector2(-1,1) },
            { Direction.Left,new Vector2(-1,0) },
            { Direction.BottomLeft,new Vector2(-1,-1) },
            { Direction.Bottom,new Vector2(0,-1) },
            { Direction.BottomRight,new Vector2(1,-1) },
        };

        /// <summary>
        /// Degree (not radian)
        /// </summary>
        public static Dictionary<Direction, float> Angles = new Dictionary<Direction, float>()
        {
            { Direction.Right,     0f },
            { Direction.TopRight,  45f },
            { Direction.Top,       90f },
            { Direction.TopLeft,   135f },
            { Direction.Left,      180f },
            { Direction.BottomLeft,225f },
            { Direction.Bottom,    270f},
            { Direction.BottomRight,315f },
        };

        public static bool IsDiagonal(Direction r)
        {
            if ((int)r % 2 == 0)
                return false;
            else
                return true;
        }

        public static Direction Reverse(Direction d)
        {
            if ((int)d < 4)
                return (Direction)(d + 4);
            else
                return (Direction)(d - 4);
        }
    }

    public enum Direction
    {
        Right = 0,
        TopRight = 1,
        Top = 2,
        TopLeft = 3,
        Left = 4,
        BottomLeft = 5,
        Bottom = 6,
        BottomRight = 7
    }

}
