//***********************************************************************//
// Copyright (C) 2017 Bilal Ar�kan. All Rights Reserved.
// Author: Bilal Ar�kan
// Time  : 04.11.2017   
//***********************************************************************//
using UnityEngine;

namespace Common.Tools
{
	/// <summary>
	/// Persistent singleton.
	/// </summary>
	public class PersistentSingleton<T> : MonoBehaviour	where T : Component
	{
		protected static T _instance;

		/// <summary>
		/// Singleton design pattern
		/// </summary>
		/// <value>The instance.</value>
		public static T Instance
		{
			get
			{
				if (_instance == null)
				{
					//_instance = FindObjectOfType<T> ();
					if (_instance == null)
					{
						GameObject obj = new GameObject ("_"+_instance.GetType());
						//obj.hideFlags = HideFlags.HideAndDontSave;
						_instance = obj.AddComponent<T> ();
					}
				}
				return _instance;
			}
		}

        /*public PersistentSingleton() : base()
        {
            _instance = this as T;
        }*/

        /// <summary>
        /// On awake, we check if there's already a copy of the object in the scene. If there's one, we destroy it.
        /// </summary>
        protected virtual void Awake ()
		{
			if(_instance == null)
			{
				//If I am the first instance, make me the Singleton
				_instance = this as T;
			}
			else if(_instance != this)
			{
                //If a Singleton already exists and you find
                //another reference in scene, destroy it!
                Destroy(this.gameObject);
                return;
			}
            DontDestroyOnLoad(gameObject);
        }
    }
}
