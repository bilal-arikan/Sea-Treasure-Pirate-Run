﻿using Common.Managers;
using Common.Tools;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Events;
using System.Net;
using System.Net.Sockets;
using UnityEngine.UI;
using UnityEngine.Networking;

namespace Common
{
    public enum RequestMethod
    {
        GET,
        POST,
        PUT,
        DELETE
    }
    /// <summary>
    /// CROSS_PLATFORM_INPUT
    /// MOBILE_INPUT
    /// GooglePlayGames
    /// Facebook
    /// GameJolt
    /// FirebaseDatabase
    /// FirebaseAuthentication
    /// FirebaseMessaging
    /// FirebaseInvites
    /// IAP
    /// EVERYPLAY_ANDROID
    /// </summary>
    [AddComponentMenu("Common/CORE")]
    public class Core : Singleton<Core>
    {
        public static System.Threading.Thread MainThread = null;

        [SerializeField]
        bool isOneSceneGame;
        public static bool IsOneSceneGame
        {
            get
            {
                return Instance.isOneSceneGame;
            }
        }

        [Space]
        public Canvas FirstOpeningCanvas;
        public Scrollbar loadingBar;
        public Text pressAnyKey;

        [SerializeField]
        protected bool _isFocused = false;
        public bool IsFocused
        {
            get
            {
                return _isFocused;
            }
        }

        public float RequestTimeout = 5;
        public int CachedObjectCount = 0;
        public static bool IsApplicationQuitting = false;
        [SerializeField]
        protected List<Action> _pending = new List<Action>();

        public List<E> EventsToCallGarbageCollector = new List<E>()
        {
            E.AppStarted,
            E.GameFinished,
            E.GameStarted,
            E.GameOnPreparing,
            E.LevelLoadingStarted,
            E.LevelLoadingCompleted,
        };

        public HashSet<Type> MustBeLodedTypes = new HashSet<Type>()
        {
            typeof(EventManager),
            typeof(LevelLoadManager),
            typeof(PlayerManager),
            typeof(GameManager),
            typeof(UIManager),
            typeof(SoundManager)/*,
            typeof(IAPManager),
            typeof(AnalyticManager),
            typeof(DatabaseManager),
            typeof(SocialManager),
            typeof(TutorialManager),
            typeof(ConfigManager),
            typeof(LanguageManager),
            typeof(AdsManager),
            typeof(Managers.AuthenticationManager)*/
        };

        [Header("Editor Only")]
        public List<string> DefineSymbols;
        
        protected override void Awake()
        {
            MainThread = System.Threading.Thread.CurrentThread;

            base.Awake();

            if (FirstOpeningCanvas && !FirstOpeningCanvas.gameObject.activeInHierarchy)
                FirstOpeningCanvas.gameObject.SetActive(true);


            // Belirli eventlerde GB otomatik çağırılıyor
            EventsToCallGarbageCollector.ForEach((e) =>
            {
                EventManager.StartListening(e, System.GC.Collect);
            });
        }

        private void Start()
        {
            var ts = GetMissingTypes();
            StartCoroutine(LoadMissingTypes(ts, (p, s) =>
            {
                pressAnyKey.text = p.ToString();
                loadingBar.size = (float)p / (float)100;
            },
             () =>
             {
                 EventManager.TriggerEvent(E.AppStarted);
                 if (FirstOpeningCanvas)
                     FirstOpeningCanvas.gameObject.Destroy();
             }));
        }

        /// <summary>
        /// return notloaded types
        /// </summary>
        /// <returns></returns>
        public List<Type> GetMissingTypes()
        {
            List<Type> notloadeds = new List<Type>();
            foreach (var type in MustBeLodedTypes)
            {
                var obj = GameObject.FindObjectOfType(type);
                if(obj == null)
                {
                    notloadeds.Add(type);
                }
            }
            return notloadeds;
        }

        /// <summary>
        /// </summary>
        /// <param name="types"></param>
        /// <returns></returns>
        public IEnumerator LoadMissingTypes(List<Type> types, Action<int,string> progressed, Action completed)
        {
            if(types.Count == 0)
            {
                yield return new WaitForEndOfFrame();
                if (completed != null)
                    completed.Invoke();
                yield break; ;
            }

            yield return null;
            int progressStep = 100 / types.Count;

            for (int i  = 0; i < types.Count; i ++)
            {
                if (progressed != null)
                    progressed.Invoke(progressStep * (i), types[i].Name + " Loading ...");

                var obj = new GameObject(types[i].Name, types[i]);
                obj.transform.parent = transform;
                obj.isStatic = true;

                if (progressed != null)
                    progressed.Invoke(progressStep * (i + 1), types[i].Name + " Loaded");

                yield return new WaitForEndOfFrame();
            }

            if(completed != null)
                completed.Invoke();
        }


        /// <summary>
        /// Run action on Main Thread after seconds
        /// </summary>
        /// <param name="a"></param>
        public static void Invoke(Action a, float seconds = 0, bool realtime = true)
        {
            Instance.StartCoroutine(Instance.InvokeCo(a, seconds, realtime));
        }
        IEnumerator InvokeCo(Action a,float seconds,bool realtime = false)
        {
            if(seconds > 0)
            {
                if (realtime)
                    yield return new WaitForSecondsRealtime(seconds);
                else
                    yield return new WaitForSeconds(seconds);
            }
            else
            {
                yield return null;
            }

            a.Invoke();
        }

        /// <summary>
        /// Like Timer Component
        /// </summary>
        /// <param name="finished"></param>
        /// <param name="targetSeconds"></param>
        /// <param name="progressed"></param>
        /// <param name="realtime"></param>
        public static void Timer(Action finished, float targetSeconds, Action<float> progressed = null, bool realtime = false)
        {
            Instance.StartCoroutine(TimerCo(finished, progressed, targetSeconds, realtime));
        }
        static IEnumerator TimerCo(Action a, Action<float> progressed, float targetSeconds, bool realtime = false)
        {
            targetSeconds = Mathf.Clamp(targetSeconds, 0, float.MaxValue);
            float targetTime = realtime ? Time.unscaledTime + targetSeconds : Time.time - targetSeconds;

            while (realtime ? Time.unscaledTime < targetTime : Time.time < targetTime)
            {
                yield return new WaitForEndOfFrame();
                if (progressed != null)
                    progressed.Invoke(realtime ? targetTime - Time.unscaledTime : targetTime - Time.time);
            }
            a.Invoke();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="url"></param>
        /// <param name="result"></param>
        public static void DownloadImage(string url, Action<Sprite> result)
        {
            Core.Get(url, (www) =>
            {
                if (www != null && www.texture != null)
                {
                    var sprite = Sprite.Create(
                        www.texture,
                        new Rect(0, 0, www.texture.width, www.texture.height),
                        new Vector2(www.texture.width / 2, www.texture.height / 2));

                    result(sprite);
                }
                else
                    result(null);
            });
        }

        /// <summary>
        /// </summary>
        /// <returns></returns>
        public static DateTime GetNetworkTime()
        {
            try
            {
                const string ntpServer = "pool.ntp.org"; // time.nist.gov
                var ntpData = new byte[48];
                ntpData[0] = 0x1B; //LeapIndicator = 0 (no warning), VersionNum = 3 (IPv4 only), Mode = 3 (Client Mode)

                var addresses = Dns.GetHostEntry(ntpServer).AddressList;
                var ipEndPoint = new IPEndPoint(addresses[0], 123);
                var socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);

                socket.Connect(ipEndPoint);
                socket.Send(ntpData);
                socket.Receive(ntpData);
                socket.Close();

                ulong intPart = (ulong)ntpData[40] << 24 | (ulong)ntpData[41] << 16 | (ulong)ntpData[42] << 8 | (ulong)ntpData[43];
                ulong fractPart = (ulong)ntpData[44] << 24 | (ulong)ntpData[45] << 16 | (ulong)ntpData[46] << 8 | (ulong)ntpData[47];

                var milliseconds = (intPart * 1000) + ((fractPart * 1000) / 0x100000000L);
                var networkDateTime = (new DateTime(1900, 1, 1)).AddMilliseconds((long)milliseconds);

                return networkDateTime;
            }
            catch (Exception ex)
            {
                Debug.LogWarning("Sunucu Zaman bilgileri alınamadı !!! " + ex.GetType());
                return DateTime.Now;
            }
        }

        private void OnApplicationQuit()
        {
            IsApplicationQuitting = true;
        }
        void OnApplicationFocus(bool hasFocus)
        {
            _isFocused = hasFocus;
        }
        void OnApplicationPause(bool pauseStatus)
        {
            _isFocused = !pauseStatus;
        }

        /// <summary>
        /// REST api Get
        /// </summary>
        /// <param name="uri"></param>
        /// <param name="callback"></param>
        /// <param name="timeout"></param>
        public static void Get(string uri,Action<WWW> callback, float timeout = 10)
        {
            WWW www = new WWW(uri);
            Instance.StartCoroutine(RequestCo(www,callback,timeout));
        }
        public static void Post(string uri, Action<UnityWebRequest> callback, string postData = null, float timeout = 10)
        {
            UnityWebRequest req = UnityWebRequest.Post(uri,postData);
            Instance.StartCoroutine(RequestCo(req, callback, timeout));
        }
        public static void Put(string uri, Action<UnityWebRequest> callback, string bodyData = null, float timeout = 10)
        {
            UnityWebRequest req = UnityWebRequest.Put(uri, bodyData);
            Instance.StartCoroutine(RequestCo(req, callback, timeout));
        }
        public static void Delete(string uri, Action<UnityWebRequest> callback, float timeout = 10)
        {
            UnityWebRequest req = UnityWebRequest.Delete(uri);
            Instance.StartCoroutine(RequestCo(req, callback, timeout));
        }

        static IEnumerator RequestCo(UnityWebRequest req, Action<UnityWebRequest> callback, float timeout)
        {
            while (!req.isDone)
            {
                if (Time.time > timeout)
                {
                    callback(null);
                    yield break;
                }
                yield return new WaitForEndOfFrame();
            }
            callback(req);
        }
        static IEnumerator RequestCo(WWW req, Action<WWW> callback, float timeout)
        {
            while (!req.isDone)
            {
                if (Time.time > timeout)
                {
                    callback(null);
                    yield break;
                }
                yield return new WaitForEndOfFrame();
            }
            callback(req);
        }

        /// <summary>
        /// </summary>
        /// <param name="url"></param>
        public static void OpenURL(string url)
        {
#if UNITY_WEBGL
            Application.ExternalEval("window.open(" + url + ");");
#else
            Application.OpenURL(url);
#endif
        }

        /// CROSS_PLATFORM_INPUT
        /// MOBILE_INPUT
        /// Facebook
        /// GameJolt
        /// FirebaseDatabase
        /// FirebaseAuthentication
        /// FirebaseMessaging
        /// FirebaseInvites
        /// IAP
        /// EVERYPLAY_ANDROID
#region Implements
        public const bool Defined_GameJolt =
#if GameJolt
            true;
#else
            false;
#endif
        public const bool Defined_Facebook =
#if Facebook
            true;
#else
            false;
#endif
        public const bool Defined_IAP =
#if IAP
            true;
#else
            false;
#endif
        #endregion

        [Header("Framework Class Design")]
        public TextAsset FrameworkClasses;
    }

}
