﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MarkLight;
using MarkLight.Views.UI;

public class KeyValue : UIView
{

    public Label KeyLabel;
    public Label ValueLabel;

    public string Key
    {
        get
        {
            return KeyLabel.Text.Value;
        }
        set
        {
            KeyLabel.Text.Value = value;
        }
    }
    public string Value
    {
        get
        {
            return ValueLabel.Text.Value;
        }
        set
        {
            ValueLabel.Text.Value = value;
        }
    }
}
