﻿using System;
using System.Collections.Generic;
using MarkLight.Views;
using MarkLight.Views.UI;

public class FriendScoreSummary : ListItem
{
    public Image FriendImage;
    public Image Star1;
    public Image Star2;
    public Image Star3;

    public Label ScoreLabel;
    public Label FriendRanking;


}

