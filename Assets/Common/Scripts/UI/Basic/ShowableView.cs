﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MarkLight.Views.UI;
using MarkLight.Views;
public abstract class ShowableView : UIView
{
    public ViewAnimation AnimationShow;
    public ViewAnimation AnimationHide;

    public virtual void Show()
    {
        if (!IsVisible || !IsActive)
        {
            IsActive.Value = true;
            IsVisible.Value = true;

            if (AnimationShow != null)
                AnimationShow.StartAnimation();
        }
    }

    public virtual void Hide()
    {
        if (AnimationHide != null)
        {
            AnimationHide.StartAnimation();
        }
        else
        {
            OnHideCompleted();
        }
    }

    protected virtual void AnimationShowCompleted()
    {

    }

    protected virtual void AnimationHideCompleted()
    {
        OnHideCompleted();
    }

    protected virtual void OnHideCompleted()
    {
        IsVisible.Value = false;
        IsActive.Value = false;
    }
}
