﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using MarkLight.Views.UI;
using MarkLight;

public class ProgressiveBar : UIView
{
    public _Sprite Sprite;
    public _string Text;
    public Image Prog;

    public _ElementSize ProgValue;

    public float Value
    {
        get
        {
            return ProgValue.Value.Percent;
        }
        set
        {
            ProgValue.Value = new MarkLight.ElementSize(value, ElementSizeUnit.Percents);
            //Prog.Width = new _ElementSize() { InternalValue = ElementSize.FromPercents( (float)_Value/(float)MaxValue ) };
        }
    }
    public float StepValue = 0.1f;

    public override void Initialize()
    {
        base.Initialize();
        Value = 0.2f;
    }

    public void Step()
    {
        if(Value >= 1)
        {
            Value = 1;
        }
        else if(1 - Value < StepValue)
        {
            Value = 1;

        }
        else
        {
            Value += StepValue;
        }
    }
}
