﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Collections;
using MarkLight.Views.UI;
using UnityEngine;
using MarkLight.Views;
using MarkLight;

public class Toast : UIView
{
    public _string TextToShow;
    public _SpriteAsset ImageToShow;
    public ViewAnimation AnimationShow;
    public ViewAnimation AnimationHide;
    bool currentlyShowing = false;
    List<string> MoreTexts = new List<string>();
    List<Sprite> MoreSprites = new List<Sprite>();
    public Image ToastImage;
    public float defaultTimeToShow = 3;


    void Show(Sprite sprite,string text, bool callForNext = false)
    {
        if (!callForNext)
        {
            MoreTexts.Add(text);
            MoreSprites.Add(sprite);
        }

        if (!currentlyShowing)
        {
            currentlyShowing = true;
            TextToShow.Value = text;
            ImageToShow.Value = sprite;

            IsVisible.Value = true;

            if (AnimationShow != null)
                AnimationShow.StartAnimation();
        }
    }

    public void Show(Sprite sprite,string text)
    {
        if (!currentlyShowing)
            StartCoroutine(WaitCoroutine(defaultTimeToShow));

        Show(sprite,text,false);
    }

    //float i;
    private IEnumerator WaitCoroutine(float wait)
    {
        /*for (i=10; i>0;i--)
        {
            yield return new WaitForSecondsRealtime(wait/10);
        }*/
        yield return new WaitForSecondsRealtime(wait);
        Hide();
    }

    protected virtual void Hide()
    {
        if (AnimationHide != null)
        {
            AnimationHide.StartAnimation();
        }
        else
        {
            OnHideCompleted();
        }
    }

    protected virtual void AnimationShowCompleted()
    {

    }

    protected virtual void AnimationHideCompleted()
    {
        OnHideCompleted();
    }


    protected void OnHideCompleted()
    {
        IsVisible.Value = false;
        currentlyShowing = false;

        MoreTexts.RemoveAt(0);
        MoreSprites.RemoveAt(0);

        if (MoreTexts.Count > 0)
        {
            Show(MoreSprites[0],MoreTexts[0], true);
            StartCoroutine(WaitCoroutine(defaultTimeToShow));
        }
    }
}

