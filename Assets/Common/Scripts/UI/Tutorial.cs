﻿using Common.Tools;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace Common
{
    [ExecuteInEditMode]
    public class Tutorial : MonoBehaviour, IPointerClickHandler
    {
        [ReadOnly]
        public List<GameObject> TutorialParts = new List<GameObject>();
        [SerializeField]
        private int ActiveIndex;
        public int ActiveTutorial
        {
            get
            {
                ActiveIndex = TutorialParts.FindIndex(tp => tp.activeInHierarchy);
                return ActiveIndex;
            }
            set
            {
                ActiveIndex = value;

                foreach (var o in TutorialParts)
                    o.SetActive(false);

                if (value < TutorialParts.Count && value >= 0)
                {
                    TutorialParts[value].SetActive(true);
                }
            }
        }

        public UnityEvent Completed;

        private void Start()
        {
            TutorialParts.Clear();
            for (int i = 0; i < transform.childCount; i++)
                TutorialParts.Add(transform.GetChild(i).gameObject);
        }

        public void Next()
        {
            // Zaten sonuncu kısım gösteriliyosa, Tutorial tamamlanmıştır
            if(ActiveTutorial == TutorialParts.Count - 1)
            {
                Completed.Invoke();
                ActiveTutorial = -1;
            }
            else
            {
                ActiveTutorial = ActiveTutorial + 1;
            }
        }

        public void Previous()
        {
            // Zaten birinci kısım gösteriliyosa, daha öncesi olamaz
            if (ActiveTutorial == 0)
            {
                ActiveTutorial = -1;
            }
            else
            {
                ActiveTutorial = ActiveTutorial - 1;
            }
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            Next();
        }
    }
}
