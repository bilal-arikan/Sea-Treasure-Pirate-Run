//***********************************************************************//
// Copyright (C) 2017 Bilal Ar�kan. All Rights Reserved.
// Author: Bilal Ar�kan
// Time  : 04.11.2017   
//***********************************************************************//
using UnityEngine;
using MarkLight;
using MarkLight.Views.UI;


/// <summary>
/// Singleton pattern.
/// </summary>
//[ExecuteInEditMode]
public class SingletonView<T> : UIView where T : UIView
{
    public SingletonView()
    {
        _instance = this as T;
    }

    protected static T _instance;

    /// <summary>
    /// Singleton design pattern
    /// </summary>
    /// <value>The instance.</value>
    public static T Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<T>();
                /*if (_instance == null)
                {
                    GameObject obj = new GameObject ("_"+typeof(T).Name);
                    //obj.hideFlags = HideFlags.HideAndDontSave;
                    _instance = obj.AddComponent<T> ();
                }*/
            }
            return _instance;
        }
    }

    public override void Initialize()
    {
        _instance = this as T;
        base.Initialize();
    }

    /// <summary>
    /// On awake, we initialize our instance. Make sure to call base.Awake() in override if you need awake.
    /// </summary>
    protected virtual void Awake()
    {
        _instance = this as T;
    }
}

