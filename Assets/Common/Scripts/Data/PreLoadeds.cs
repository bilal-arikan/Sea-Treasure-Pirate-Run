﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using Common.Tools;
using System.Collections;

namespace Common.Data
{
    [ScriptOrder(-600)]
    public partial class PreLoadeds : Singleton<PreLoadeds>
    {
        public List<Sprite> Sprites = new List<Sprite>();
        public List<Tutorial> Tutorials = new List<Tutorial>();
        public List<GameObject> GameObjects = new List<GameObject>();

        public Dictionary<string, GameObject> AllPreLoadedPrefabs = new Dictionary<string, GameObject>();

    }
}
