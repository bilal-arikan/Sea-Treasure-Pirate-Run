﻿using Common.Managers;
using Common.Tools;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using UnityEngine;
#if GooglePlayGames
using GooglePlayGames;
using GooglePlayGames.BasicApi;
using System.Threading.Tasks;
#endif
#if GameJolt
using GameJolt.API;
using System.Collections;
#endif
#if Facebook
using Facebook.Unity;
#endif
using Arikan.Models;

namespace Common.Data
{
    [Serializable]
    public partial class Player
    {

        [JsonIgnore]
        public string ID
        {
            get
            {
                return AuthenticationManager.CurrentUserId;
            }
        }
        [JsonIgnore]
        public int Level
        {
            get
            {
                int level = 1;
                for(int i = 1; i< PlayerManager.Instance.LevelSteps.Length; i++)
                {
                    if (i >= PlayerManager.Instance.LevelSteps.Length)
                        level = PlayerManager.Instance.LevelSteps.Length;
                    else if (xp >= PlayerManager.Instance.LevelSteps[i])
                        level++;
                    else
                        break;
                }
                return level;
            }
        }

        [SerializeField/*,JsonIgnore*/]
        int xp = 0;

        public int Xp
        {
            get{return xp;}
            set
            {
                int old = Level;
                xp = value;
                if (Level != old && PlayerManager.Instance.PlayerLoaded)
                    EventManager.TriggerObjectEvent(E.PlayerLevelUp, Level);
            }
        }
        public int Money = 1000;
        public int SpecialMoney = 20;

        public bool DisabledAds = false;

        public List<Achievement> Achievements = new List<Achievement>();

        public LongHashSet GameOpenedDays = new LongHashSet();

        public PointHashSet WhatchedTutorials = new PointHashSet();

        public PublicPassedDict PassedSubChapterScores = new PublicPassedDict();
        public Dictionary<Point2, PassedSubCh> PassedSubChapterScores2 = new Dictionary<Point2, PassedSubCh>();

        public PointHashSet UnlockedSubChapters = new PointHashSet(); /*{
            new Point(1, 1),
            new Point(1, 2),
            new Point(1, 3),
            new Point(1, 4),
            //new Point(1, 5),
            new Point(1, 6),
            new Point(1, 7),
            new Point(1, 8),
            new Point(1, 9),
            //new Point(1, 10)
            new Point(1, 11),
            new Point(1, 12),
            new Point(1, 13),
            new Point(1, 14),
            //new Point(1, 5),
        }; // 5. 10. ve 15. bölüm hariç açıktır*/

        public float TotalPlayTime = 0;

        /*public void AddPassedChapter(int ch, int subch, PassedSubCh psc)
        {
            //Debug.Log("APC:" + ch + ":" + subch + ":" + PassedSubChapters.Count + ":"+ PassedSubChapters[ch].Count);

            // SubChapter mevcutsa
            if (PassedSubChapterScores.ContainsKey(new Point(ch, subch)))
            {
                // Skoru değştir
                PassedSubChapterScores[new Point(ch, subch)] = psc;
            }
            // Chapter mevcut deilse
            else
            {
                // Skoru ekle
                PassedSubChapterScores.Add(new Point(ch, subch), psc);
            }
            //Debug.Log("PSCs: " + ObjectSerializer.ToJson(PassedSubChapterScores, true));
        }*/
    }

}
