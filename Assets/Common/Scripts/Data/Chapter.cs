﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using MarkLight;
using MarkLight.Views.UI;
using Common.Managers;

namespace Common.Data
{
    [Serializable]
    [CreateAssetMenu(fileName = "Ch", menuName = "Common/Chapter")]
    public partial class Chapter : ScriptableObject
    {

        public int Index;

        /// <summary>
        /// Gets or sets the item at the specified index.
        /// </summary>
        public SubChapter this[int index]
        {
            get
            {
                return SubChapters[index];
            }
            set
            {
                SubChapters[index] = value;
            }
        }

        public int Count
        {
            get
            {
                return SubChapters.Count;
            }
        }
        public static int AllSubChCount
        {
            get
            {
                return LevelLoadManager.Instance.AllChapters.Count;
            }
        }

        public bool IsFirst
        {
            get
            {
                return Index == 1;
            }
        }
        public bool IsLast
        {
            get
            {
                return Index == LevelLoadManager.Instance.AllChapters.Count;
            }
        }



        public string Header;
        public string Summary;
        public Sprite FrontSpritePreLoaded;
        public Sprite MapSpritePreLoaded;

        public string ScoreTableName;

        [Serializable]
        public class SubChaptersDict : Dict<int, SubChapter> { }

        public SubChaptersDict SubChapters = new SubChaptersDict();

        private void OnEnable()
        {
            foreach (var sc in SubChapters.DictInstance)
            {
                sc.Value.Index = sc.Key;
                sc.Value.ParentChapter = this;
            }
        }

        public Chapter NextCh()
        {
            if (this.IsLast)
                return null;
            else
                return LevelLoadManager.Instance.AllChapters[Index + 1];
        }

        public Chapter PreviousCh()
        {
            if (this.IsFirst)
                return null;
            else
                return LevelLoadManager.Instance.AllChapters[Index - 1];
        }
    }
}
