﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Common.Data
{
    [Serializable]
    public class MusicData
    {
        public AudioClip MusicClip;
        public Sprite MusicSprite;
        public string CreditsString;
    }
}
