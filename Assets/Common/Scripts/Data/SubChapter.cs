﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using MarkLight;
using MarkLight.Views.UI;
using Common.Managers;
using UnityEngine.SceneManagement;

namespace Common.Data
{
    [Serializable]
    [CreateAssetMenu(fileName = "SubCh", menuName = "Common/Sub Chapter")]
    public partial class SubChapter : ScriptableObject
    {
        [SerializeField]
        int index;
        public int Index
        {
            get
            {
                return index;
            }
            set
            {
                index = value;
            }
        }

        public Point2 IndexPoint
        {
            get
            {
                if (ParentChapter != null)
                    return new Point2(ParentChapter.Index, Index);
                else
                    return new Point2(-1, -1);
            }
        }

        [NonSerialized]
        public Chapter ParentChapter;

        public int SceneBuildIndex;
        [SerializeField]
        UnityEngine.Object scene;
        /*public Scene Scene
        {
            get
            {
                return SceneManager.GetSceneByBuildIndex(SceneBuildIndex);
            }
        }*/

        public bool IsFirst
        {
            get
            {
                return IndexPoint.y == 1;
            }
        }
        public bool IsLast
        {
            get
            {
                return IndexPoint.y == LevelLoadManager.Instance.AllChapters[IndexPoint.x].Count;
            }
        }
        public string Header = "???Header";
        public string Summary = "???Summary";
        public Sprite FrontSprite;

        /*private void OnEnable()
        {
            //index = ParentChapter.SubChapters.FirstOrDefault(x => x.Value == this).Key;
            Debug.Log("SubChapter OnEnable:" + index);
        }*/

        public SubChapter NextSubCh()
        {
            //Debug.Log("NextSubCh:" + IndexPoint);
            // Son ch ve Son subch ise
            if (LevelLoadManager.Instance.AllChapters[IndexPoint.x].IsLast && this.IsLast)
            {
                return null;
            }
            else if (this.IsLast)
            {
                return LevelLoadManager.Instance.AllChapters[IndexPoint.x + 1][1];
            }
            else
            {
                return LevelLoadManager.Instance.AllChapters[IndexPoint.x][IndexPoint.y + 1];
            }
        }

        public SubChapter PreviousSubCh()
        {
            if (LevelLoadManager.Instance.AllChapters[IndexPoint.x].IsFirst && this.IsFirst)
            {
                return null;
            }
            else if (this.IsFirst)
            {
                // Önceki Chapterin son SubChapteri
                return LevelLoadManager.Instance.AllChapters[IndexPoint.x - 1][LevelLoadManager.Instance.AllChapters[IndexPoint.x - 1].Count];
            }
            else
            {
                return LevelLoadManager.Instance.AllChapters[IndexPoint.x][IndexPoint.y - 1];
            }
        }
    }
}