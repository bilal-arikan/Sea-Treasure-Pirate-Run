﻿// using Common.Managers;
// using System;
// using System.Collections.Generic;
// using UnityEngine;

// namespace Common.Data
// {
//     [Serializable]
//     //[CreateAssetMenu(fileName = "Ach-", menuName = "Common/Achievement")]
//     public class Achievement //: ScriptableObject
//     {
//         [Header("Achievement Hash Code")]
//         public string Name;
//         public string GooglePlayId;
//         public int GameJoltId;
//         [Space]
//         [NonSerialized]
//         public string Title;
//         [NonSerialized]
//         public string Description;
//         [NonSerialized]
//         public string ImageUrl;
//         [NonSerialized]
//         public Sprite Image;

//         public float Value = 0;
//         public float MaxValue = 100;
//         [NonSerialized]
//         public bool SendedToGooglePlay = false;
//         [NonSerialized]
//         public bool SendedToGameJolt = false;

//         public double Percent
//         {
//             get { return (Value / MaxValue) * 100; }
//         }
//         public bool IsCompleted
//         {
//             get { return Value >= MaxValue; }
//         }

//         /// <summary>
//         /// 
//         /// </summary>
//         /// <param name="ach"> Id si tamamlanan yüzde bilgisi</param>
//         /// <param name="desc">Başlık, tanım ve resim bilgisi</param>
//         public void Initialize(UnityEngine.SocialPlatforms.IAchievement ach, UnityEngine.SocialPlatforms.IAchievementDescription desc)
//         {
//             GooglePlayId = ach.id;
//             if (Value < (float)ach.percentCompleted)
//                 Value = (float)ach.percentCompleted;

//             // Başlık, Tanım ve Resmi ayarlıyoruz
//             if(desc != null)
//             {
//                 Title = desc.title;
//                 Description = desc.unachievedDescription; ;
//                 Image = Sprite.Create(
//                     desc.image,
//                     new Rect(0, 0, desc.image.width, desc.image.height),
//                     desc.image.texelSize / 2);
//             }
//         }
// #if GameCenter
//         public void Initialize(GK_AchievementTemplate ach)
//         {
//             GameCenterId = ach.Id;
//             if (Value < ach._progress)
//                 Value = ach._progress;

//             // Başlık, Tanım ve Resmi ayarlıyoruz
//             Title = ach.Title;
//             Description = ach.Description;
//             Image = Sprite.Create(
//                 ach.Texture,
//                 new Rect(0, 0, ach.Texture.width, ach.Texture.height),
//                 ach.Texture.texelSize / 2);
//         }
// #endif

//         public override int GetHashCode()
//         {
//             if (!string.IsNullOrEmpty( GooglePlayId))
//                 return GooglePlayId.GetHashCode();
//             if (GameJoltId != 0)
//                return GameJoltId.GetHashCode();
//             return Name.GetHashCode();
//         }



//         /// <summary>
//         /// Mevcut değeri doğrudan değiştirir
//         /// </summary>
//         /// <param name="newValue"></param>
//         public void UpdateValue(float newValue)
//         {
//             Value = Mathf.Clamp(newValue, 0, MaxValue);

//             Save();
//         }

//         /// <summary>
//         /// Mevcut değerin üzerine ekler
//         /// </summary>
//         /// <param name="addedValue"></param>
//         public void AddValue(float addedValue)
//         {
//             Value += addedValue;
//             Value = Mathf.Clamp(Value, 0, MaxValue);

//             Save();
//         }

//         /// <summary>
//         ///  Değeri Goole Play e gönderir
//         /// </summary>
//         void Save()
//         {
//             if (!SocialManager.Instance.IsSignedInGooglePlay)
//                 Debug.LogError("GooglePlay e giriş yapılmadı !");
//             else if (string.IsNullOrEmpty(GooglePlayId))
//                 throw new UnityException("Achievement ID si ayarlanmamış !");
//             else
//             {
//                 /*PlayGamesPlatform.Instance.ReportProgress(GooglePlayId, CurrentValue, (bool success) =>
//                 {
//                     if (success)
//                         Debug.Log("Achievement " + GooglePlayId + " Saved");
//                     else
//                         Debug.LogError("Achievement " + GooglePlayId + " Couldn't Saved !!");
//                 });*/
//             }
//         }

//     }
// }
