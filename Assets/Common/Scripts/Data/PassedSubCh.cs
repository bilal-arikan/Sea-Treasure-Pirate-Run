﻿using System;

namespace Common.Data
{
    [Serializable]
    public class PassedSubCh
    {
        public int Score;
        //public int Star;
        public float Time;

        public int StarCount(SubChapter sch)
        {
            return 1;
        }
    }

    [Serializable]
    public class FriendPassedSubCh
    {
        public int Rank = 0;
        public string Id;
        public Profile FriendProfile;
        public PassedSubCh FriendPSC;
    }
}
