﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MiddleScenePassing : MonoBehaviour {

    public int CurrentScene = 0;
    public string ToLoadScene;
    public LoadSceneMode Mode = LoadSceneMode.Single;
    public int WaitToUnload = 5;

	// Use this for initialization
	IEnumerator Start () {
        yield return null;

        if (!string.IsNullOrEmpty(ToLoadScene))
        {
            var ao = SceneManager.LoadSceneAsync(ToLoadScene, Mode);
            StartCoroutine(LoadSceneAsyncCo(ao));
                
        }
    }

    IEnumerator LoadSceneAsyncCo(AsyncOperation ao)
    {
        while (!ao.isDone)
        {
            yield return null;
        }
        yield return new WaitForSecondsRealtime(WaitToUnload);
        SceneManager.UnloadSceneAsync(CurrentScene);
    }
}
