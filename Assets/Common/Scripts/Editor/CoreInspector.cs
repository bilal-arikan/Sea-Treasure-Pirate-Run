﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEditor;
using UnityEngine;

namespace Common
{
    [CustomEditor(typeof(Core))]
    public class CoreInspector : Editor
    {
        Core c;

        private void OnEnable()
        {
            c = target as Core;

            var symbs = PlayerSettings.GetScriptingDefineSymbolsForGroup(EditorUserBuildSettings.selectedBuildTargetGroup);
            c.DefineSymbols.Clear();
            c.DefineSymbols.AddRange(symbs.Split(';'));
        }

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            
            if (GUILayout.Button("Apply Symbols"))
            {
                PlayerSettings.SetScriptingDefineSymbolsForGroup(EditorUserBuildSettings.selectedBuildTargetGroup, string.Join(";", c.DefineSymbols.ToArray()));
            }
        }
    }
}
