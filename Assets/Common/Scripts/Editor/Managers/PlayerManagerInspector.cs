﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEditor;
using UnityEngine;

namespace Common.Managers
{
    [CustomEditor(typeof(PlayerManager))]
    public class PlayerManagerInspector : Editor
    {
        PlayerManager m;

        private void OnEnable()
        {
            m = target as PlayerManager;
        }

        public override void OnInspectorGUI()
        {
            if(GUILayout.Button("Load Player"))
            {
                m.LoadPlayerData();
            }
            if(GUILayout.Button("Save Player"))
            {
                m.SavePlayerData();
            }
            if(GUILayout.Button("Load Profile"))
            {
                m.LoadPlayerProfile();
            }
            if(GUILayout.Button("Save Player"))
            {
                m.SavePlayerProfile();
            }
            base.OnInspectorGUI();

        }
    }
}
