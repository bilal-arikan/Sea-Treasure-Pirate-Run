﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEditor;
using UnityEngine;

namespace Common.Managers
{
    [CustomEditor(typeof(TutorialManager))]
    public class TutorialManagerInspector : Editor
    {
        TutorialManager m;

        private void OnEnable()
        {
            m = target as TutorialManager;
        }

        public override void OnInspectorGUI()
        {
            if(GUILayout.Button("Complete Tutorial"))
            {
                if(m.ActiveTutorial != null)
                {
                    m.ActiveTutorial.Completed.Invoke();
                    Debug.Log("Active Tutorial Completed");
                }
            }

            base.OnInspectorGUI();

        }
    }
}
