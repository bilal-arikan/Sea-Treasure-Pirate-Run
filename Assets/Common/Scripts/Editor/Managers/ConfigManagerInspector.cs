﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEditor;
using UnityEngine;

namespace Common.Managers
{
    [CustomEditor(typeof(ConfigManager))]
    public class ConfigManagerInspector : Editor
    {
        ConfigManager m;

        private void OnEnable()
        {
            m = target as ConfigManager;
        }

        public override void OnInspectorGUI()
        {
            if(GUILayout.Button("Fetch Configs"))
            {
                m.FetchConfigs();
            }
            if(GUILayout.Button("Get Current Version"))
            {
                Debug.Log(m.GetVersionNumber());
            }

            base.OnInspectorGUI();

        }
    }
}
