﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEditor;
using UnityEngine;

namespace Common.Managers
{
    [CustomEditor(typeof(EventManager))]
    public class EventManagerInspector : Editor
    {
        EventManager m;

        private void OnEnable()
        {
            m = target as EventManager;
        }

        public override void OnInspectorGUI()
        {

            base.OnInspectorGUI();

        }
    }
}
