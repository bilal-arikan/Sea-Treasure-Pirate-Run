﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEditor;
using UnityEngine;

namespace Common.Managers
{
    [CustomEditor(typeof(InputManager))]
    public class InputManagerInspector : Editor
    {
        InputManager m;

        private void OnEnable()
        {
            m = target as InputManager;
        }

        public override void OnInspectorGUI()
        {

            base.OnInspectorGUI();

        }
    }
}
