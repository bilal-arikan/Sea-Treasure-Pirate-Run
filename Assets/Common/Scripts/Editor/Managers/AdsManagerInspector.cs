﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEditor;
using UnityEngine;

namespace Common.Managers
{
    [CustomEditor(typeof(AdsManager))]
    public class AdsManagerInspector : Editor
    {
        AdsManager m;

        private void OnEnable()
        {
            m = target as AdsManager;
        }

        public override void OnInspectorGUI()
        {
            if (GUILayout.Button("Show Reward"))
            {
                m.TryShowReward(null, (r) =>
                {
                    Debug.Log("Reward Showed");
                });
            }

            base.OnInspectorGUI();

        }
    }
}