﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEditor;
using UnityEngine;

namespace Common.Managers
{
    [CustomEditor(typeof(LanguageManager))]
    public class LanguageManagerInspector : Editor
    {
        LanguageManager m;

        private void OnEnable()
        {
            m = target as LanguageManager;
        }

        public override void OnInspectorGUI()
        {

            base.OnInspectorGUI();

        }
    }
}
