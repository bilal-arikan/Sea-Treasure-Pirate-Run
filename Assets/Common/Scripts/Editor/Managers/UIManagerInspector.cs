﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEditor;
using UnityEngine;

namespace Common.Managers
{
    [CustomEditor(typeof(UIManager))]
    public class UIManagerInspector : Editor
    {
        UIManager m;

        private void OnEnable()
        {
            m = target as UIManager;
        }

        public override void OnInspectorGUI()
        {
            if (GUILayout.Button("Show Toast"))
            {
                m.ShowToast("Test Toast");
            }
            if (GUILayout.Button("Show Warning"))
            {
                m.ShowWarning("Test Warning");
            }
            if (GUILayout.Button("Show AskToUser"))
            {
                m.ShowAskToUser("Test AskToUser",(r)=>
                {
                    Debug.Log("Answer " + r);
                });
            }
            base.OnInspectorGUI();

        }
    }
}
