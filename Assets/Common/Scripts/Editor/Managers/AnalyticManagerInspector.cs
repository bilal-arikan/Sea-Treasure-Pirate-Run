﻿using Common.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEditor;
using UnityEngine;

namespace Common.Managers
{
    [CustomEditor(typeof(AnalyticManager))]
    public class AnalyticManagerInspector : Editor
    {
        AnalyticManager m;

        private void OnEnable()
        {
            m = target as AnalyticManager;
        }

        public override void OnInspectorGUI()
        {
            if (GUILayout.Button("Statistics To Json"))
            {
                ObjectSerializer.ToJson(m.CurrentPlayStatistics, true);
            }

            base.OnInspectorGUI();

        }
    }
}
