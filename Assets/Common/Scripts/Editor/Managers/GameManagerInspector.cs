﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEditor;
using UnityEngine;

namespace Common.Managers
{
    [CustomEditor(typeof(GameManager))]
    public class GameManagerInspector : Editor
    {
        GameManager m;

        private void OnEnable()
        {
            m = target as GameManager;
        }

        public override void OnInspectorGUI()
        {
            if(GUILayout.Button("Pause"))
            {
                m.Pause();
            }
            if (GUILayout.Button("UnPause"))
            {
                m.UnPause();
            }
            if (GUILayout.Button("TimeScale 1"))
            {
                m.SetTimeScale(1);
            }

            base.OnInspectorGUI();

        }
    }
}
