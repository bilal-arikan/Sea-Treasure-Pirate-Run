﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEditor;
using UnityEngine;

namespace Common.Managers
{
    [CustomEditor(typeof(SoundManager))]
    public class SoundManagerInspector : Editor
    {
        SoundManager m;

        private void OnEnable()
        {
            m = target as SoundManager;
        }

        public override void OnInspectorGUI()
        {
            if(GUILayout.Button("Mute Sfx"))
            {
                m.SfxOn = true;
            }
            if(GUILayout.Button("Unmute Sfx"))
            {
                m.SfxOn = false;
            }
            if (GUILayout.Button("Mute Music"))
            {
                m.MusicOn = true;
            }
            if(GUILayout.Button("Unmute Music"))
            {
                m.MusicOn = false;
            }

            base.OnInspectorGUI();

        }
    }
}
