﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEditor;
using UnityEngine;

namespace Common.Managers
{
    [CustomEditor(typeof(IAPManager))]
    public class IAPManagerInspector : Editor
    {
        IAPManager m;

        private void OnEnable()
        {
            m = target as IAPManager;
        }

        public override void OnInspectorGUI()
        {


            base.OnInspectorGUI();

        }
    }
}
