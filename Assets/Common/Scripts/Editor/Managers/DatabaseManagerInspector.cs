﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEditor;
using UnityEngine;

namespace Common.Managers
{
    [CustomEditor(typeof(DatabaseManager))]
    public class DatabaseManagerInspector : Editor
    {
        DatabaseManager m;

        private void OnEnable()
        {
            m = target as DatabaseManager;
        }

        public override void OnInspectorGUI()
        {
            if(GUILayout.Button("Reset Database"))
            {
                DatabaseManager.Local.Reset();
                Debug.Log("Database Reset");
            }

            base.OnInspectorGUI();

        }
    }
}
