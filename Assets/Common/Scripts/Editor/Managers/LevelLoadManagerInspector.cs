﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEditor;
using UnityEngine;

namespace Common.Managers
{
    [CustomEditor(typeof(LevelLoadManager))]
    public class LevelLoadManagerInspector : Editor
    {
        LevelLoadManager m;

        private void OnEnable()
        {
            m = target as LevelLoadManager;
        }

        public override void OnInspectorGUI()
        {

            base.OnInspectorGUI();

        }
    }
}
