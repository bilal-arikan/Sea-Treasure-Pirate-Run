﻿using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

public class CEnumerableDrawer : PropertyDrawer
{
    static SerializedProperty copied;

    protected string TypeName;

    protected ReorderableList _list;
    protected int _selected;
    protected string headerText;
    protected bool reordering;

    protected SerializedProperty prop;

    /// <summary>
    /// Initialize ReorderableList and assigning callbacks
    /// </summary>
    /// <param name="prop"></param>
    /// <returns></returns>
    protected virtual ReorderableList InitDisplay(SerializedProperty p)
    {
        ReorderableList inst = new ReorderableList(p.serializedObject, p.FindPropertyRelative("_key"), true, true, true, true);

        inst.onChangedCallback += OnChangedCallback;


        inst.onRemoveCallback += OnRemoveCallback;


        inst.onAddCallback += OnAddCallback;

        inst.elementHeightCallback = ElementHeightCallback;


        inst.onReorderCallback += OnReorderCallback;

        inst.drawElementCallback = DrawElementCallback;

        inst.drawHeaderCallback = DrawHeaderCallback;
        inst.drawFooterCallback = DrawFooterCallback;
        inst.onSelectCallback += OnSelectCallback;
        inst.onCanRemoveCallback += OnCanRemoveCallback;

        inst.showDefaultBackground = false;
        inst.draggable = true;
        //	    ReorderableList.defaultBehaviours;
        return inst;

    }

    protected virtual void OnAddCallback(ReorderableList list)
    {
        var localKeys = prop.FindPropertyRelative("_key");
        int idx = localKeys.arraySize;
        localKeys.InsertArrayElementAtIndex(idx);
        var key = localKeys.GetArrayElementAtIndex(idx);

        if (idx > 0)
            CopyValue(localKeys.GetArrayElementAtIndex(idx - 1), ref key);

        Activate(list.index);
    }

    protected virtual float ElementHeightCallback(int index)
    {
        float ret;
        if (prop != null)
        {
            ret = (index >= prop.FindPropertyRelative("_key").arraySize)
                ? EditorGUIUtility.singleLineHeight
                : EditorGUI.GetPropertyHeight(prop.FindPropertyRelative("_key").GetArrayElementAtIndex(index));
        }
        else
        {
            ret = EditorGUIUtility.singleLineHeight;
        }
        return ret;
    }

    protected virtual void Activate(int index)
    {
        var k = prop.FindPropertyRelative("_key");
        for (int i = 0; i < k.arraySize; i++)
        {
            if (i == index)
            {
                k.GetArrayElementAtIndex(i).isExpanded = true;
            }
            else
            {
                k.GetArrayElementAtIndex(i).isExpanded = false;
            }
        }
    }

    protected virtual void DeactivateAllChild(int index)
    {
        var k = prop.FindPropertyRelative("_key");
        _list.draggable = false;
        for (int i = 0; i < k.arraySize; i++)
        {
            k.GetArrayElementAtIndex(i).isExpanded = false;
        }
    }

    protected virtual void DrawElementCallback(Rect rect, int index, bool isActive, bool isFocused)
    {
        if (isActive)
        {
            Activate(index);
        }

        //float KVWidth = prop.FindPropertyRelative("_divider").floatValue + 0.5f;
        Rect left = new Rect(rect.x, rect.y, rect.width /* * KVWidth*/, rect.height);
        //Rect right = new Rect(rect.x + rect.width * KVWidth, rect.y, rect.width * (1f - KVWidth), rect.height);

        GUIContent emptylabel = new GUIContent();
        if (index < prop.FindPropertyRelative("_key").arraySize)
            EditorGUI.PropertyField(left, prop.FindPropertyRelative("_key").GetArrayElementAtIndex(index),
                emptylabel, true);
    }

    protected virtual void OnReorderCallback(ReorderableList list)
    {
        reordering = true;
        //prop.FindPropertyRelative("_key").MoveArrayElement(_selected, list.index);
        Activate(list.index);
    }

    protected virtual void OnChangedCallback(ReorderableList list)
    {
        reordering = false;
        _list = InitDisplay(prop);
    }

    protected virtual void DrawHeaderCallback(Rect rect)
    {
        GUILayout.BeginHorizontal();

        rect.width -= 240f;
        rect.x = 10;
        EditorGUI.LabelField(rect, headerText, EditorStyles.boldLabel);

        GUILayout.EndHorizontal();
    }

    protected virtual void DrawFooterCallback(Rect rect)
    {
        rect.height = 15;
        (new ReorderableList.Defaults()).DrawFooter(rect, _list);

        GUILayout.BeginHorizontal();

        // Reverse Button
        rect.width = 60;
        if (GUI.Button(rect, "Copy"))
        {
            var array = prop.FindPropertyRelative("_key");
            copied = array.Copy();
            Debug.Log("Copy:" + copied);
        }

        // Reverse Button
        rect.x = rect.width + rect.x;
        rect.width = 60;
        if (GUI.Button(rect, "Paste"))
        {
            var array = prop.FindPropertyRelative("_key");
            var temp = copied.Copy();
            if (temp != null)
                array = temp;
            Debug.Log("Paste:" + copied + ":" + temp != null);
        }

        // Reverse Button
        rect.x = rect.width + rect.x;
        if (GUI.Button(rect, "Reverse"))
        {
            var array = prop.FindPropertyRelative("_key");
            for (int i = 1; i < array.arraySize; i++)
            {
                array.MoveArrayElement(i, 0);
            }
        }

        // Clear Button
        rect.x = rect.width + rect.x;
        if (GUI.Button(rect, "Clear"))
        {
            if (EditorUtility.DisplayDialog("Clear Array", "Are you sure to Clear Array !!!", "Yes", "Cancel"))
            {
                prop.FindPropertyRelative("_key").ClearArray();
            }
        }

        GUILayout.EndHorizontal();
    }

    protected virtual void OnSelectCallback(ReorderableList list)
    {
        if (list == _list)
        {
            reordering = true;
            _selected = list.index;
            prop.FindPropertyRelative("_selected").intValue = list.index;
        }
    }

    protected virtual bool OnCanRemoveCallback(ReorderableList list)
    {
        return true;
    }

    protected virtual void OnRemoveCallback(ReorderableList list)
    {
        if (list.serializedProperty.arraySize == 1)
        {
            prop.FindPropertyRelative("_key").ClearArray();
        }
        else
        {
            prop.FindPropertyRelative("_key").DeleteArrayElementAtIndex(list.index);
        }
    }

    protected virtual void CopyValue(SerializedProperty type, ref SerializedProperty val)
    {
        switch (type.propertyType)
        {
            case SerializedPropertyType.AnimationCurve:
                val.animationCurveValue = type.animationCurveValue;
                break;
            case SerializedPropertyType.Boolean:
                val.boolValue = type.boolValue;
                break;
            case SerializedPropertyType.Bounds:
                val.boundsValue = type.boundsValue;
                break;
            case SerializedPropertyType.Character:
            case SerializedPropertyType.Generic:
            case SerializedPropertyType.Gradient:
            case SerializedPropertyType.LayerMask:
                //no good way to initialize a good base value
                var reset = val.FindPropertyRelative("reset");
                if (reset != null)
                {
                    reset.boolValue = true;
                }
                break;
            case SerializedPropertyType.ObjectReference:
                val.objectReferenceValue = type.objectReferenceValue;
                break;
            case SerializedPropertyType.Color:
                val.colorValue = type.colorValue;
                break;
            case SerializedPropertyType.Enum:
                val.enumValueIndex = type.enumValueIndex;
                break;
            case SerializedPropertyType.Float:
                val.floatValue = type.floatValue;
                break;
            case SerializedPropertyType.String:
                val.stringValue = type.stringValue;
                break;
            case SerializedPropertyType.Integer:
                val.intValue = type.intValue;
                break;
            case SerializedPropertyType.Quaternion:
                val.quaternionValue = type.quaternionValue;
                break;
            case SerializedPropertyType.Rect:
                val.rectValue = type.rectValue;
                break;
            case SerializedPropertyType.Vector2:
                val.vector2Value = type.vector2Value;
                break;
            case SerializedPropertyType.Vector3:
                val.vector3Value = type.vector3Value;
                break;
            case SerializedPropertyType.Vector4:
                val.vector4Value = type.vector4Value;
                break;
        }
    }

    public override void OnGUI(Rect pos, SerializedProperty p, GUIContent label)
    {

        headerText = label.text;

        if (p.isExpanded)
        {
            prop = p;
            if (p.depth == 0)
            {
                Rect reorderpos = new Rect(pos);
                reorderpos.y += EditorGUIUtility.singleLineHeight;

                _list = _list ?? InitDisplay(p);

                _list.DoList(reorderpos);
            }
            else
            {
                if (!reordering)
                    _list = InitDisplay(p);
                _list.DoList(pos);
            }
        }
        else if (p.depth > 0)
        {
            headerText += " [" + p.FindPropertyRelative("_key").arraySize + "]";
        }
        headerText += " <" + p.FindPropertyRelative("_keyType").propertyType + "> ("+TypeName+")";
        headerText = headerText.Replace("ObjectReference", "Object");

        p.isExpanded = EditorGUI.Foldout(pos, p.isExpanded, "");
        if (!p.isExpanded)
        {
            EditorGUI.LabelField(pos, headerText, EditorStyles.boldLabel);
        }

        if (GUI.changed)
        {

        }
    }
    public override float GetPropertyHeight(SerializedProperty p, GUIContent label)
    {
        return p.isExpanded ? InitDisplay(p).GetHeight() + base.GetPropertyHeight(p, label) : base.GetPropertyHeight(p, label);
    }
}
