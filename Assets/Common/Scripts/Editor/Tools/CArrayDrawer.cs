﻿using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

[CustomPropertyDrawer(typeof(Common.CArray),true)]
public class CArrayDrawer : CEnumerableDrawer
{

    void OnEnable()
    {
        TypeName = "ARRAY";
    }
}
