﻿using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

[CustomPropertyDrawer(typeof(Common.CHashSet),true)]
public class CHashSetDrawer : CEnumerableDrawer
{
    void OnEnable()
    {
        TypeName = "HASHSEt";
    }

    private List<int> _errorIDs = new List<int>();
    SerializedProperty _errors;

    void CheckForErrorsUpdate(SerializedProperty p)
    {
        _errors = p.FindPropertyRelative("_errorList");
        _errorIDs.Clear();
        if (_errors != null)
        {
            for (int i = 0; i < _errors.arraySize; i++)
            {
                _errorIDs.Add(_errors.GetArrayElementAtIndex(i).intValue);
            }
        }
    }

    /// <summary>
    /// Initialize ReorderableList and assigning callbacks
    /// </summary>
    /// <param name="prop"></param>
    /// <returns></returns>
    protected override ReorderableList InitDisplay(SerializedProperty p)
    {
        CheckForErrorsUpdate(p);

        return base.InitDisplay(p);
    }

    protected override void DrawElementCallback(Rect rect, int index, bool isActive, bool isFocused)
    {
        if (isActive)
        {
            Activate(index);
        }


        //float KVWidth = prop.FindPropertyRelative("_divider").floatValue + 0.5f;
        Rect left = new Rect(rect.x, rect.y, rect.width /* * KVWidth*/, rect.height);
        //Rect right = new Rect(rect.x + rect.width * KVWidth, rect.y, rect.width * (1f - KVWidth), rect.height);



        GUIContent emptylabel = new GUIContent();
        if (_errorIDs.Contains(index))
        {
            Color temp = GUI.color;
            GUI.color = Color.red;
            if (index < prop.FindPropertyRelative("_key").arraySize)
                EditorGUI.PropertyField(left, prop.FindPropertyRelative("_key").GetArrayElementAtIndex(index),
                    emptylabel, true);
            /*if (index < prop.FindPropertyRelative("_val").arraySize)
                EditorGUI.PropertyField(right, prop.FindPropertyRelative("_val").GetArrayElementAtIndex(index),
                    emptylabel, true);*/
            GUI.color = temp;
        }
        else
        {
            if (index < prop.FindPropertyRelative("_key").arraySize)
                EditorGUI.PropertyField(left, prop.FindPropertyRelative("_key").GetArrayElementAtIndex(index),
                    emptylabel, true);
            /*if (index < prop.FindPropertyRelative("_val").arraySize)
                EditorGUI.PropertyField(right, prop.FindPropertyRelative("_val").GetArrayElementAtIndex(index),
                    emptylabel, true);*/
        }
    }

    protected override void OnChangedCallback(ReorderableList list)
    {
        reordering = false;
        _list = InitDisplay(prop);
        CheckForErrorsUpdate(list.serializedProperty);
        if (_errors != null)
        {
            for (int i = 0; i < _errors.arraySize; i++)
            {
                _errorIDs.Add(_errors.GetArrayElementAtIndex(i).intValue);
            }
        }
    }

    protected override void OnRemoveCallback(ReorderableList list)
    {
        if (list.serializedProperty.arraySize == 1)
        {
            prop.FindPropertyRelative("_key").ClearArray();
            _errors.ClearArray();
        }
        else
        {
            prop.FindPropertyRelative("_key").DeleteArrayElementAtIndex(list.index);
            CheckForErrorsUpdate(prop);
        }
    }


    public override void OnGUI(Rect pos, SerializedProperty p, GUIContent label)
    {

        headerText = label.text;

        if (p.isExpanded)
        {
            prop = p;
            if (p.depth == 0)
            {
                Rect reorderpos = new Rect(pos);
                reorderpos.y += EditorGUIUtility.singleLineHeight;

                _list = _list ?? InitDisplay(p);

                _list.DoList(reorderpos);
            }
            else
            {
                if (!reordering)
                    _list = InitDisplay(p);
                _list.DoList(pos);
            }
        }
        else if (p.depth > 0)
        {
            headerText += " [" + p.FindPropertyRelative("_key").arraySize + "]";
        }
        headerText += " <" + p.FindPropertyRelative("_keyType").propertyType + "> (HASHSET)";
        headerText = headerText.Replace("ObjectReference", "Object");

        p.isExpanded = EditorGUI.Foldout(pos, p.isExpanded, "");
        if (!p.isExpanded)
        {
            EditorGUI.LabelField(pos, headerText, EditorStyles.boldLabel);
        }
    }
}
