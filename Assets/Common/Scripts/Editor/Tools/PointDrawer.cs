﻿//C# Example

using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomPropertyDrawer(typeof(Point2))]
internal class PointDrawer : PropertyDrawer
{
    public override void OnGUI(Rect rect, SerializedProperty property, GUIContent label)
    {
        label = EditorGUI.BeginProperty(rect, label, property);
        Rect contentPosition = EditorGUI.PrefixLabel(rect, label);
        EditorGUIUtility.labelWidth = 12f;
        contentPosition.width = contentPosition.width/2 - 6;
        contentPosition.height = 16;
        EditorGUI.indentLevel = 0;
        EditorGUI.PropertyField(contentPosition, property.FindPropertyRelative("x"), new GUIContent("X"));
        contentPosition.x += contentPosition.width + 2;
        EditorGUI.PropertyField(contentPosition, property.FindPropertyRelative("y"), new GUIContent("Y"));
        EditorGUI.EndProperty();
    }
}
