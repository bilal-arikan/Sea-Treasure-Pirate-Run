﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public GameObject ToSpawn;

    public void Change(GameObject newObjectToSpawn)
    {
        ToSpawn = newObjectToSpawn;
    }

    public void SpawnOnWorld(Vector3 worldPose)
    {
        if (ToSpawn)
            Instantiate(ToSpawn, worldPose, Quaternion.identity);
    }

    public void SpawnOnTransformPose(Transform worldPose)
    {
        if (ToSpawn)
            Instantiate(ToSpawn, worldPose.position, Quaternion.identity);
    }

    public void SpawnOnParent(Transform parent)
    {
        if (ToSpawn)
            Instantiate(ToSpawn, parent);
    }
}
