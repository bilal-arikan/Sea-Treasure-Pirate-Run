﻿using DG.Tweening;
using DG.Tweening.Plugins;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Events;

namespace Common
{
    public enum TransformVariable
    {
        Position,
        LocalPosition,
        Rotation,
        LocalRotation,
        Scale,
        LocalScale,
    }

    public enum PlayAnimationOn
    {
        Start,
        OnEnable,
        Manuel
    }

    [Serializable]
    public class TransformAnimation
    {
        public TransformVariable Variable;
        public Ease EaseFunc;
        /// <summary>
        /// true > Offset
        /// false > DirectValue
        /// </summary>
        public bool IsOffset;
        public Vector3 TargetValue;

        public Tweener ToTweener(Transform transform,float duration)
        {
            Tweener t = null;
            switch (Variable)
            {
                case TransformVariable.Position:
                    t = transform.DOMove(IsOffset ? transform.position + TargetValue : TargetValue, duration);
                    break;
                case TransformVariable.LocalPosition:
                    t = transform.DOLocalMove(IsOffset ? transform.localPosition + TargetValue : TargetValue, duration);
                    break;
                case TransformVariable.Rotation:
                    t = transform.DORotate(IsOffset ? transform.rotation.eulerAngles + TargetValue : TargetValue, duration);
                    break;
                case TransformVariable.LocalRotation:
                    t = transform.DOLocalRotate(IsOffset ? transform.localRotation.eulerAngles + TargetValue : TargetValue, duration);
                    break;
                case TransformVariable.Scale:
                    t = transform.DOScale(IsOffset ? transform.lossyScale + TargetValue : TargetValue, duration);
                    break;
                case TransformVariable.LocalScale:
                    t = transform.DOScale(IsOffset ? transform.localScale + TargetValue : TargetValue, duration);
                    break;
                default:
                    break;
            }

            if (t != null)
            {
                t.SetEase(EaseFunc);
                t.SetAutoKill(false);
                t.Rewind();
            }
            return t;
        }
    }

    [Serializable]
    public class SpriteAnimation
    {
        public Ease EaseFunc;
        public Color TargetColor = new Color(1, 1, 1, 0);

        public Tweener ToTweener(SpriteRenderer rend, float duration)
        {
            Tweener t = rend.DOColor(TargetColor, duration);

            if (t != null)
            {
                t.SetEase(EaseFunc);
                t.SetAutoKill(false);
                t.Rewind();
            }
            return t;
        }
    }

    public class SimpleAnimator : MonoBehaviour
    {
        public float Seconds;
        public bool RealTime;
        public PlayAnimationOn PlayOn = PlayAnimationOn.OnEnable;

        [Space]
        public List<TransformAnimation> TransformAnimations = new List<TransformAnimation>();
        public List<SpriteAnimation> SpriteAnimations = new List<SpriteAnimation>();

        public Tweener[] Tweeners;

        public UnityEvent AnimationFinished;
        public UnityEvent AnimationRewinded;

        public void Play()
        {
            if (Tweeners.Length > 0)
                Tweeners[0].OnComplete(new TweenCallback(AnimationFinished.Invoke));
            Tweeners.ForEach(tw => tw.Play());
        }
        public void Revert()
        {
            if (Tweeners.Length > 0)
                Tweeners[0].OnRewind(new TweenCallback(AnimationRewinded.Invoke));
            Tweeners.ForEach(tw => tw.SmoothRewind());
        }
        public void Play(UnityAction completed)
        {
            if (Tweeners.Length > 0)
                Tweeners[0].OnComplete(new TweenCallback(()=> { AnimationFinished.Invoke(); completed(); }));
            Tweeners.ForEach(tw => tw.Play());
        }
        public void Revert(UnityAction completed)
        {
            if (Tweeners.Length > 0)
                Tweeners[0].OnRewind(new TweenCallback(() => { AnimationRewinded.Invoke(); completed(); }));
            Tweeners.ForEach(tw => tw.SmoothRewind());
        }
        public void Restart()
        {
            if (Tweeners.Length > 0)
                Tweeners[0].OnComplete(null);
            Tweeners.ForEach(tw => tw.Restart());
        }
        public void Reset()
        {
            if (Tweeners.Length > 0)
                Tweeners[0].OnComplete(null);
            Tweeners.ForEach(tw => tw.Rewind());
        }
        public void Complete()
        {
            if (Tweeners.Length > 0)
                Tweeners[0].OnComplete(null);
            Tweeners.ForEach(tw => tw.Complete());
        }

        public void InitTweeners()
        {
            if (Tweeners != null)
                Tweeners.ForEach(t => t.Kill());
            // Create Transform Animations
            var tt = TransformAnimations.ConvertAll(a => a.ToTweener(transform,Seconds));

            // Create Sprite Animations
            var rend = GetComponent<SpriteRenderer>();
            if(rend)
                tt.AddRange(SpriteAnimations.ConvertAll(a => a.ToTweener(rend, Seconds)));

            Tweeners = tt.ToArray();
            //Debug.Log("Tweeners:" + Tweeners.Length);
        }

        private void Start()
        {
            if (PlayOn == PlayAnimationOn.Start)
                Play();
        }

        private void OnEnable()
        {
            InitTweeners();

            if (PlayOn == PlayAnimationOn.OnEnable)
                Play();
        }

        private void Update()
        {
            if(Input.GetKeyDown(KeyCode.Z))
                Play();
            if (Input.GetKeyDown(KeyCode.X))
                Play(() => { Debug.Log("Completed 1"); });
            if (Input.GetKeyDown(KeyCode.C))
                Revert();
            if (Input.GetKeyDown(KeyCode.V))
                Revert(() => { Debug.Log("Completed 2"); });
            if (Input.GetKeyDown(KeyCode.B))
                Restart();
            if (Input.GetKeyDown(KeyCode.N))
                Reset();
            if (Input.GetKeyDown(KeyCode.M))
                Complete();

            if (Input.GetKeyDown(KeyCode.G))
                gameObject.SetActiveWithAnim(false);
            if (Input.GetKeyDown(KeyCode.H))
                gameObject.SetActiveWithAnim(true);
            if (Input.GetKeyDown(KeyCode.J))
                this.DisableWithAnim();
            if (Input.GetKeyDown(KeyCode.D))
                gameObject.DestroyWithAnim();
            if (Input.GetKeyDown(KeyCode.F))
                gameObject.DestroyWithAnim(false);
        }
    }
}

public static class SimpleAnimationExtensions
{
    /// <summary>
    /// SimpleAnimator extension
    /// </summary>
    /// <param name="gameObject"></param>
    /// <param name="value"></param>
    /// <param name="withAnimation"></param>
    public static void SetActiveWithAnim(this GameObject gameObject, bool value)
    {
        var sta = gameObject.GetComponent<Common.SimpleAnimator>();
        if (!sta)
            gameObject.SetActive(value);
        else
        {
            if (value)
            {
                sta.Reset();
                gameObject.SetActive(true);
                sta.Play();
            }
            else
            {
                sta.Revert(() => {
                    gameObject.SetActive(false);
                });
            }
        }
    }

    public static void DestroyWithAnim(this GameObject gameObject, bool reverseAnim = true)
    {
        var sta = gameObject.GetComponent<Common.SimpleAnimator>();
        if (!sta)
            MonoBehaviour.Destroy(gameObject);
        else
        {
            if (reverseAnim)
                sta.Revert(() => { MonoBehaviour.Destroy(gameObject); });
            else
                sta.Play(() => { MonoBehaviour.Destroy(gameObject); });
        }
    }

    public static void DisableWithAnim(this MonoBehaviour bh, bool reverseAnim = true)
    {
        var sta = bh.GetComponent<Common.SimpleAnimator>();
        if (!sta)
            bh.enabled = false;
        else
        {
            if (reverseAnim)
                sta.Revert(() => { bh.enabled = false; });
            else
                sta.Play(() => { bh.enabled = false; });
        }
    }
    
}