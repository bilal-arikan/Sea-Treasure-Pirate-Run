﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

[ExecuteInEditMode]
public class ObjectBounds : MonoBehaviour
{
    public enum WaysToDetermineBounds { Collider, Collider2D, Renderer, Undefined }
    public WaysToDetermineBounds BoundsBasedOn;


    public Vector3 Center
    {
        get
        {
            return Bounds.center;
        }
    }
    [ReadOnly]
    public Vector3 TopRightBack;
    [ReadOnly]
    public Vector3 BottomLeftFront;
    [ReadOnly]
    public Bounds Bounds;
#if UNITY_EDITOR
    public bool DrawGizmos = true;
#endif
    private void OnEnable()
    {
        Recalculate();
    }

    public void Recalculate()
    {
        var Bounds = GetBounds();
        TopRightBack = Bounds.center + Bounds.size / 2;
        BottomLeftFront = Bounds.center - Bounds.size / 2;
    }

    /// <summary>
    /// Returns the bounds of the object, based on what has been defined
    /// </summary>
    public virtual Bounds GetBounds()
    {
        if (BoundsBasedOn == WaysToDetermineBounds.Renderer)
        {
            if (GetComponent<Renderer>() == null)
            {
                throw new Exception("The PoolableObject " + gameObject.name + " is set as having Renderer based bounds but no Renderer component can be found.");
            }
            return GetComponent<Renderer>().bounds;
        }

        if (BoundsBasedOn == WaysToDetermineBounds.Collider)
        {
            if (GetComponent<Collider>() == null)
            {
                throw new Exception("The PoolableObject " + gameObject.name + " is set as having Collider based bounds but no Collider component can be found.");
            }
            return GetComponent<Collider>().bounds;
        }

        if (BoundsBasedOn == WaysToDetermineBounds.Collider2D)
        {
            if (GetComponent<Collider2D>() == null)
            {
                throw new Exception("The PoolableObject " + gameObject.name + " is set as having Collider2D based bounds but no Collider2D component can be found.");
            }
            return GetComponent<Collider2D>().bounds;
        }

        return new Bounds(Vector3.zero, Vector3.zero);
    }

#if UNITY_EDITOR
    private void OnDrawGizmos()
    {
        if (!DrawGizmos &&  Bounds != null)
            return;
        Gizmos.color = CColor.blue;
        Gizmos.DrawLine(TopRightBack, TopRightBack.SetX(BottomLeftFront.x));
        Gizmos.DrawLine(TopRightBack, TopRightBack.SetY(BottomLeftFront.y));
        Gizmos.DrawLine(BottomLeftFront, BottomLeftFront.SetX(TopRightBack.x));
        Gizmos.DrawLine(BottomLeftFront, BottomLeftFront.SetY(TopRightBack.y));
    }
#endif
}
