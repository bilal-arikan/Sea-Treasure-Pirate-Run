﻿using Common.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Common.Components
{
    class RayTrace2DTrigger : MonoBehaviour
    {
        public Transform TargetTransformForAngle;
        public GameObject LastHitObject;
        public bool CallOnTriggerStay = false;
        RaycastHit2D newHit;
        RaycastHit2D oldHit;
        const string OnTriggerEnter2D = "OnTriggerEnter2D";
        const string OnTriggerStay2D = "OnTriggerStay2D";
        const string OnTriggerExit2D = "OnTriggerExit2D";


        private void Start()
        {
            if (TargetTransformForAngle == null)
                Debug.LogWarning("(RayTrace2DTrigger) Target Object Null !!!");

            oldHit = Physics2D.Raycast(transform.position, TargetTransformForAngle.position - transform.position);
        }


        private void FixedUpdate()
        {
            newHit = Physics2D.Raycast(transform.position, TargetTransformForAngle.position - transform.position);

            if (newHit.transform != null)
            {
                // Eski objeye değiyorsa
                if (LastHitObject == newHit.transform.gameObject)
                {
                    if(CallOnTriggerStay)
                        BroadcastMessage(OnTriggerStay2D, newHit.collider);
                }
                else
                {
                    // Obje yeni ama önceden başka objeye değmişse
                    if (LastHitObject != null)
                    {
                        BroadcastMessage(OnTriggerEnter2D, newHit.collider);
                        BroadcastMessage(OnTriggerExit2D, oldHit.collider);
                    }
                    // Obje yeni ve önceden başka objeye değmemişse
                    else
                    {
                        BroadcastMessage(OnTriggerEnter2D, newHit.collider);
                    }
                    LastHitObject = newHit.transform.gameObject;
                }
            }
            // Şimdilik değen yok ama önceden değen varsa
            else if (LastHitObject != null)
            {
                LastHitObject = null;

                BroadcastMessage(OnTriggerExit2D, oldHit.collider);
            }
            oldHit = newHit;
        }

        private void OnDrawGizmos()
        {
            if(TargetTransformForAngle != null)
                Gizmos.DrawLine(transform.position, TargetTransformForAngle.position);
        }
    }
}
