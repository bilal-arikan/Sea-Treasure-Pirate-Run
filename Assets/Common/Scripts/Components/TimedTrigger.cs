﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Events;

namespace Common
{
    public class TimedTrigger : MonoBehaviour
    {
        public float Seconds;
        public bool RealTime;
        public bool Loop;
        public UnityEvent ToCall;

        float currentTime;

        private void Update()
        {
            if (currentTime > Seconds)
            {
                ToCall.Invoke();

                if (Loop)
                    currentTime = currentTime - Seconds;
                else
                    enabled = false;
            }

            if (RealTime)
                currentTime += Time.unscaledDeltaTime;
            else
                currentTime += Time.deltaTime;
        }

    }
}
