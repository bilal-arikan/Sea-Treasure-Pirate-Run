﻿using Common.Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Common.Components
{
    public class PlayOnSoundManager : MonoBehaviour
    {
        public AudioClip Sound;

        public void Play()
        {
            SoundManager.Instance.PlaySound(Sound);
        }

        public void PlayPosition(Vector3 pos)
        {
            SoundManager.Instance.PlaySound(Sound,pos);
        }
    }
}
