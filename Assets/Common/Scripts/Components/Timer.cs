﻿//***********************************************************************//
// Copyright (C) 2017 Bilal Arıkan. All Rights Reserved.
// Author: Bilal Arıkan
// Time  : 04.11.2017   
//***********************************************************************//
using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Common.Tools
{
    public class Timer : MonoBehaviour
    {
        public Text ShowTimeText;

        [SerializeField]
        protected float currentTick = 0;
        [SerializeField]
        protected bool paused = true;
        public bool AutoStart = false;
        public bool UseRealTime = false;
        public static bool ShowHours = false;
        public static bool ShowMiliseconds = true;
        public float TargetTime;
        public Action<float> TimeChanged;
        public Action<float> TimerStarted;
        public Action<float> TimerFinished;
        public UnityEvent TimerStartedEvent;
        public UnityEvent TimerFinishedEvent;

        [Header("To Warn Player")]
        public AudioSource ShortTimeLeftAudio;
        public float ShortTimeLeftSeconds = 5;

        public float CurrentTime
        {
            get
            {
                return currentTick;
            }
        }
        public bool IsPaused
        {
            get
            {
                return paused;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="targetTime"></param>
        /// <param name="timerFinished"></param>
        /// <returns></returns>
        public static Timer StartTimer(float targetTime, Action<float> timerFinished, Action<float> timerChanged = null,bool useRealTime = false)
        {
            if(targetTime > 0)
            {
                var o = (new GameObject("OnceUsingTimer", typeof(Timer), typeof(TimedAutoDestroy)));
                o.GetComponent<TimedAutoDestroy>().TimeBeforeDestruction = targetTime + 3;
                Timer t = o.GetComponent<Timer>();
                t.TargetTime = targetTime;
                t.TimerFinished = timerFinished;
                t.TimeChanged = timerChanged;
                t.StartTimer();
                t.UseRealTime = useRealTime;
                return t;
            }
            else
                return null;
        }

        private void Start()
        {
            if (AutoStart)
                StartTimer();
        }

        /// <summary>
        /// </summary>
        public virtual void StartTimer()
        {
            if (TimeChanged != null)
                TimeChanged(currentTick);
            if (TimerStarted != null)
                TimerStarted(currentTick);
            if (TimerStartedEvent != null)
                TimerStartedEvent.Invoke();

            if (paused)
                StartCoroutine(UpdateTimer());
            /*else
                Debug.LogWarning("Timer Already Started !!!");*/

            /*if (TargetTime <= 0)
                Debug.Log("Timer Started on Endless MODE");
            else
                Debug.Log("Timer Started for " + TargetTime);*/

        }

        public virtual void FinishTimer()
        {
            currentTick = TargetTime;

            OnFinished();

            //Debug.Log("Timer Manually Finished");
        }

        protected IEnumerator UpdateTimer()
        {
            paused = false;
            while (!paused)
            {
                currentTick += UseRealTime ? Time.unscaledDeltaTime : Time.deltaTime;

                // Süre bittiyse
                if (TargetTime > 0 && currentTick > TargetTime)
                {
                    currentTick = TargetTime;
                    OnFinished();
                }
                else
                {
                    if (TimeChanged != null)
                        TimeChanged(currentTick);
                }

                yield return null;
            }
        }



        /// <summary>
        /// </summary>
        /// <returns>Current Tick</returns>
        public virtual float PauseTimer()
        {
            paused = true;
            return currentTick;
        }

        /// <summary>
        /// </summary>
        public virtual void ResetTimer()
        {
            paused = true;
            currentTick = 0;
        }


        protected virtual void OnFinished()
        {
            paused = true;

            if (TimeChanged != null)
                TimeChanged(currentTick);
            if (TimerFinished != null)
                TimerFinished(TargetTime);
            if(TimerFinishedEvent != null)
                TimerFinishedEvent.Invoke();

            if (ShowTimeText != null)
                ShowTimeText.text = TimeToString(currentTick);
        }

        /// <summary>
        /// </summary>
        /// <param name="t"></param>
        /// <returns></returns>
        public static string TimeToString(float t)
        {
            if (t < 0)
                return "-- : -- : --";

            int hours = (int)Mathf.Floor(t / 1440);
            int minutes = (int)Mathf.Floor(t / 60);
            int seconds = (int)(t % 60);
            int milliseconds = (int)((t % 1) * 100);

            
            return (ShowHours ? hours.ToString("00") + " : " : "") + 
                minutes.ToString("00") + " : " + seconds.ToString("00") + 
                (ShowMiliseconds ? " : " + milliseconds.ToString("00") : "");
        }

        /// <summary>
        /// </summary>
        /// <param name="t"></param>
        /// <returns></returns>
        public static float StringToTime(string t)
        {
            string[] s = t.Split(':');

            //int hours = int.Parse(s[0].Trim());
            int minutes = int.Parse(s[0].Trim());
            int seconds = int.Parse(s[1].Trim());
            int millisc = int.Parse(s[2].Trim());

            return minutes * 60 + seconds + millisc / 100f;
        }


    }
}
