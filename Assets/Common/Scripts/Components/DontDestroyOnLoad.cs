﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Common
{
    public class DontDestroyOnLoad : MonoBehaviour
    {
        void Awake()
        {
            DontDestroyOnLoad(gameObject);
        }
    }
}
