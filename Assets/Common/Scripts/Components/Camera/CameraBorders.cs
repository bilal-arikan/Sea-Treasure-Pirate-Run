﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Common
{
    public class CameraBorders : MonoBehaviour
    {
        public Camera Cam;
        public bool FitCamera;
        public Vector3 TopRight;
        public Vector3 TopLeft
        {
            get
            {
                return TopRight.SetX(BottomLeft.x);
            }
        }
        public Vector3 BottomLeft;
        public Vector3 BottomRight
        {
            get
            {
                return BottomLeft.SetX(TopRight.x);
            }
        }
        public bool IsCameraBiggerThanBordersHorizontally
        {
            get
            {
                var v4 = Cam.RightTopLeftBottom();
                if (v4.x > TopRight.x && v4.z < BottomLeft.x)
                    return true;
                return false;
            }
        }
        public bool IsCameraBiggerThanBordersVertically
        {
            get
            {
                var v4 = Cam.RightTopLeftBottom();
                if (v4.y > TopRight.y && v4.w < BottomLeft.y)
                    return true;
                return false;
            }
        }

        private void Start()
        {
            if(!Cam)
                Cam = GetComponent<Camera>();

            if (!Cam.orthographic)
                throw new UnityException("Camera Not Ortographic");

            var v4 = Cam.RightTopLeftBottom();
            if (v4.x > TopRight.x && v4.z < BottomLeft.x)
                throw new UnityException("Borders smaller than Cam bounds !!! (Horizontal)");
            if (v4.y > TopRight.y && v4.w < BottomLeft.y)
                throw new UnityException("Borders smaller than Cam bounds !!! (Vertical)");
        }

        private void Update()
        {
            if (Cam)
            {
                if (FitCamera)
                {
                    if (IsCameraBiggerThanBordersHorizontally)
                    {
                        Cam.transform.position = Cam.transform.position.SetX((TopRight.x + BottomLeft.x) / 2);
                        Cam.orthographicSize = (TopRight.x - BottomLeft.x) / 2;
                    }
                    if (IsCameraBiggerThanBordersVertically)
                    {
                        Cam.transform.position = Cam.transform.position.SetY((TopRight.y + BottomLeft.y) / 2);
                        Cam.orthographicSize = (TopRight.y - BottomLeft.y) / 2;
                    }
                }

                var v4 = Cam.RightTopLeftBottom();
                if (v4.x > TopRight.x)
                {
                    transform.position += new Vector3(TopRight.x - v4.x, 0, 0);
                    //transform.position += transform.position.WithX(TopRight.x - v4.x);
                    //Debug.Log("x fixed");
                }
                if (v4.y > TopRight.y)
                {
                    transform.position += new Vector3(0, TopRight.y - v4.y, 0);
                    //transform.position += transform.position.WithY(TopRight.y - v4.y);
                    //Debug.Log("y fixed");
                }
                if (v4.z < BottomLeft.x)
                {
                    transform.position += new Vector3(BottomLeft.x - v4.z,0,0);
                    //transform.position += transform.position.WithX(BottomLeft.x - v4.z);
                    //Debug.Log("z fixed");
                }
                if (v4.w < BottomLeft.y)
                {
                    transform.position += new Vector3(0, BottomLeft.y - v4.w, 0);
                    //transform.position += transform.position.WithY(BottomLeft.y - v4.w);
                    //Debug.Log("w fixed");
                }

            }
        }

        private void OnDrawGizmos()
        {
            Gizmos.color = Color.red;
            Gizmos.DrawLine(TopRight, TopLeft);
            Gizmos.DrawLine(TopRight, BottomRight);
            Gizmos.DrawLine(BottomLeft, BottomRight);
            Gizmos.DrawLine(BottomLeft, TopLeft);

            /*if (Cam)
            {
                var v4 = Cam.RightTopLeftBottom();
                Gizmos.color = Color.blue;
                Gizmos.DrawLine(new Vector3(v4.x, v4.y), new Vector3(v4.z, v4.w));
            }*/
        }
    }
}
