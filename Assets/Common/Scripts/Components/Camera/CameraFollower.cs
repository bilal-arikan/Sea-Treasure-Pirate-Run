﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class CameraFollower : MonoBehaviour
{
    public Rigidbody Followed;
    public Rigidbody2D Followed2D;

    public Vector3 Offset;

    public float CameraPositionLerp = 0.02f;
    public float VelocityMul = 1;
    public float VelocitySmoothnessLerp = 0.9f;
    public float MinAccountedSpeed = 10;
    public float CamBordersMul = 0.8f;
    public float InstantJumpDistance = 50;

    private Vector3 _smoothVelocity;
    private Camera _camera;

    private void OnEnable()
    {
        if(Followed !=null)
            Offset = transform.position - Followed.transform.position;
        else if(Followed2D != null)
            Offset = transform.position - Followed2D.gameObject.transform.position;

        if (_camera == null)
            SetCamera(Camera.main);
        else
            SetCamera(_camera);
    }


    private void LateUpdate()
    {
        if (Followed != null)
        {
            var camPos = _camera.transform.position;
            var followedPos = Followed.position + Offset;

            var vel = Followed.velocity.sqrMagnitude > MinAccountedSpeed * MinAccountedSpeed
                ? Followed.velocity
                : Vector3.zero;
            _smoothVelocity = Vector2.Lerp(vel, _smoothVelocity, VelocitySmoothnessLerp);

            var camTargetPos = followedPos + _smoothVelocity * VelocityMul;
            var camHalfWidth = _camera.orthographicSize * _camera.aspect * CamBordersMul;
            var camHalfHeight = _camera.orthographicSize * CamBordersMul;
            var followedDir = followedPos - camTargetPos;

            if (followedDir.x > camHalfWidth)
                camTargetPos.x = followedPos.x - camHalfWidth;
            if (followedDir.x < -camHalfWidth)
                camTargetPos.x = followedPos.x + camHalfWidth;
            if (followedDir.y > camHalfHeight)
                camTargetPos.y = followedPos.y - camHalfHeight;
            if (followedDir.y < -camHalfHeight)
                camTargetPos.y = followedPos.y + camHalfHeight;
            if (followedDir.z > camHalfHeight)
                camTargetPos.z = followedPos.z - camHalfHeight;
            if (followedDir.z < -camHalfHeight)
                camTargetPos.z = followedPos.z + camHalfHeight;

            var pos = (followedPos - camPos).sqrMagnitude < InstantJumpDistance * InstantJumpDistance
                    ? Vector3.Lerp(camPos, camTargetPos, CameraPositionLerp * Time.deltaTime)
                    : followedPos;

            _camera.transform.position = pos;
            camPos = pos;
        }
        else if (Followed2D != null)
        {
            var camPos = _camera.transform.position;
            var followedPos = Followed2D.gameObject.transform.position + Offset;

            transform.position = Vector3.Lerp(camPos, followedPos, VelocitySmoothnessLerp*Time.deltaTime);
            return;

            /*var vel = Followed2D.velocity.sqrMagnitude > MinAccountedSpeed * MinAccountedSpeed
                ? (Vector3)Followed2D.velocity
                : Vector3.zero;
            _smoothVelocity = Vector3.Lerp(vel, _smoothVelocity, VelocitySmoothnessLerp);

            var camTargetPos = followedPos + _smoothVelocity * VelocityMul;
            var camHalfWidth = _camera.orthographicSize * _camera.aspect * CamBordersMul;
            var camHalfHeight = _camera.orthographicSize * CamBordersMul;
            var followedDir = followedPos - camTargetPos;

            if (followedDir.x > camHalfWidth)
                camTargetPos.x = followedPos.x - camHalfWidth;
            if (followedDir.x < -camHalfWidth)
                camTargetPos.x = followedPos.x + camHalfWidth;
            if (followedDir.y > camHalfHeight)
                camTargetPos.y = followedPos.y - camHalfHeight;
            if (followedDir.y < -camHalfHeight)
                camTargetPos.y = followedPos.y + camHalfHeight;
            if (followedDir.z > camHalfHeight)
                camTargetPos.z = followedPos.z - camHalfHeight;
            if (followedDir.z < -camHalfHeight)
                camTargetPos.z = followedPos.z + camHalfHeight;

            var pos = (followedPos - camPos).sqrMagnitude < InstantJumpDistance * InstantJumpDistance
                    ? Vector3.Lerp(camPos, camTargetPos, CameraPositionLerp * Time.deltaTime)
                    : followedPos;

            _camera.transform.position = pos;
            camPos = pos;*/
        }
    }

    public void SetCamera(Camera cam)
    {
        _camera = cam;
        if (Followed != null)
            _camera.transform.position = Followed.position + Offset;
        else if (Followed2D != null)
            _camera.transform.position = Followed2D.gameObject.transform.position + Offset;
    }
}
