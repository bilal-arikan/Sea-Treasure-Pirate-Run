﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class ObjectFollowerSimple : MonoBehaviour
{
    public Transform Target;
    public float LerpAmount = 0.8f;
    public bool SetOffsetOnStart = true;
    public Vector3 Offset;

    private void Start()
    {
        if(SetOffsetOnStart && Target != null)
            Offset = transform.position - Target.position;
    }

    public void Follow(Transform target)
    {
        Target = target;
        Offset = transform.position - Target.position;
    }

    private void LateUpdate()
    {
        if(Target)
            transform.position = Vector3.Lerp(transform.position, Target.position + Offset, LerpAmount * Time.deltaTime);
    }
}
