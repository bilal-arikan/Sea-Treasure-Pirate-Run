﻿//***********************************************************************//
// Copyright (C) 2017 Bilal Arıkan. All Rights Reserved.
// Author: Bilal Arıkan
// Time  : 04.11.2017   
//***********************************************************************//
using System;
using System.Collections.Generic;
#if FirebaseAnalytics
using Firebase.Analytics;
using UnityEngine.Analytics;
#endif
using Common.Tools;
using UnityEngine;
using Common.Data;
using Arikan;

namespace Common.Managers
{


    public partial class AnalyticManager : Singleton<AnalyticManager>, IManager
    {
        [SerializeField]
        bool _Enabled = false;
        public bool Enabled
        {
            set
            {
#if FirebaseAnalytics
                FirebaseAnalytics.SetAnalyticsCollectionEnabled(value);
                UnityEngine.Analytics.Analytics.enabled = value;
                UnityEngine.Analytics.Analytics.deviceStatsEnabled = value;
#endif
                _Enabled = value;
            }
            get
            {
                return _Enabled;
            }
        }

        public StatisticData CurrentPlayStatistics;

        protected override void Awake()
        {
            base.Awake();
            Enabled = _Enabled;

#if FirebaseAnalytics
            FirebaseAnalytics.LogEvent(FirebaseAnalytics.EventAppOpen);
#endif

            EventManager.StartListeningObjectEvent(E.SocialLogIn, (platform) =>
            {
#if FirebaseAnalytics
                AnalyticsEvent.UserSignup((AuthorizationNetwork)((int)platform));
                FirebaseAnalytics.LogEvent(FirebaseAnalytics.EventLogin,"Platform",platform.ToString());
#endif
            });
            EventManager.StartListeningObjectEvent(E.SocialLogOut, (platform) =>
            {
                Custom("SocialLogOut", "Platform", platform.ToString());
            });
            EventManager.StartListeningObjectEvent(E.ItemPurchased, (id) =>
            {
                Custom("ItemPurchased", "ItemId", id);
            });
            EventManager.StartListeningObjectEvent(E.ItemPurchasingCanceled, (id) =>
            {
                Custom("ItemPurchasingCanceled", "ItemId", id);
            });
            EventManager.StartListeningObjectEvent(E.ItemPurchasingError, (id) =>
            {
                Custom("ItemPurchasingError", "ItemId", id);
            });
            EventManager.StartListeningObjectEvent(E.RewardVideoGift, (amount) =>
            {
                Custom("RewardWatched", "RewardAmount", amount);
            });

            EventManager.StartListening(E.GameStarted, () =>
            {
#if FirebaseAnalytics
                var res = AnalyticsEvent.GameStart();
                FirebaseAnalytics.LogEvent(FirebaseAnalytics.EventLevelStart);
#endif
            });
            EventManager.StartListening(E.GameFinished, () =>
            {
#if FirebaseAnalytics
                var res = AnalyticsEvent.GameOver();
                FirebaseAnalytics.LogEvent(FirebaseAnalytics.EventLevelEnd);
#endif
            });

            CurrentPlayStatistics = DatabaseManager.Local.Get<StatisticData>("TotalPlayStatistics");
            if (CurrentPlayStatistics == null)
                CurrentPlayStatistics = new StatisticData();

            // Statistics
            CConsole.ActionsNoArg.Add("statistics", () =>
            {
                CConsole.Log(ObjectSerializer.ToJson(CurrentPlayStatistics, true), CColor.aliceblue);
            });
        }

        public void Custom(string eventName, string key, object value)
        {
            Custom(eventName, new Dictionary<string, object>() { { key, value } });
        }
        public void Custom(string eventName, Dictionary<string, object> parameters = null)
        {
            try
            {
                if (parameters == null)
                    parameters = new Dictionary<string, object>();

#if FirebaseAnalytics
                AnalyticsEvent.Custom(eventName, parameters);

                Parameter[] ps = new Parameter[parameters.Count];
                int i = 0;
                foreach (var p in parameters)
                {
                    if (p.Value.GetType() == typeof(int))
                        ps[i] = new Parameter(p.Key, (int)(p.Value));
                    if (p.Value.GetType() == typeof(long))
                        ps[i] = new Parameter(p.Key, (long)(p.Value));
                    else if (p.Value.GetType() == typeof(float))
                        ps[i] = new Parameter(p.Key, (float)(p.Value));
                    else if ( p.Value.GetType() == typeof(double))
                        ps[i] = new Parameter(p.Key, (double)(p.Value));
                    else
                        ps[i] = new Parameter(p.Key, p.Value.ToString());
                    i++;
                }
                FirebaseAnalytics.LogEvent(eventName, ps);
#endif
            }
            catch (Exception e)
            {
                Debug.LogError(e.Message + " " + e.StackTrace);
            }
        }



        void OnApplicationQuit()
        {
#if FirebaseAnalytics
            Analytics.FlushEvents();
#endif
            CalculateStatistics();
            DatabaseManager.Local.Set("TotalPlayStatistics", CurrentPlayStatistics);
        }
        void OnApplicationPause(bool pauseStatus)
        {
#if FirebaseAnalytics
            Analytics.FlushEvents();
#endif
            CalculateStatistics();
            DatabaseManager.Local.Set("TotalPlayStatistics", CurrentPlayStatistics);
        }

        public void CalculateStatistics()
        {
            if (PlayerManager.CurrentPlayer == null)
                return;

            // Toplam oynanma
            PlayerManager.CurrentPlayer.TotalPlayTime += Time.time;

            // Kazanılan yıldızların bütün yıldızlara oranı
            /*int earnedStars = 0;
            foreach (var passedSubChValue in PlayerManager.CurrentPlayer.PassedSubChapterScores.Values)
                earnedStars += passedSubChValue.StarCount();
            CurrentPlayStatistics.ChaptersCompletition = Chapter.AllSubChCount > 0 ? earnedStars / (Chapter.AllSubChCount*3) : 0;*/

            // Toplam Achievement bitirme oranı
            /*float totalAch = 0;
            float currentAch = 0;
            foreach (var ach in PlayerManager.CurrentPlayer.Achievements)
            {
                totalAch += 100;
                currentAch += ach.Value;
            }
            CurrentPlayStatistics.AchievementsCompletition = totalAch > 0 ? currentAch / totalAch : 0;*/

            // Oyun Dili
            CurrentPlayStatistics.Language = LanguageManager.CurrentLanguage;

            // Oyunun açılışından bu yana ortalama Ses yüksekliği
            CurrentPlayStatistics.AverageSfxValue =
                (CurrentPlayStatistics.AverageSfxValue * PlayerManager.CurrentPlayer.GameOpenedDays.Count + SoundManager.Instance.SfxVolume)
                / (PlayerManager.CurrentPlayer.GameOpenedDays.Count + 1);

            // Oyunun açılışından bu yana ortalama Müzik yüksekliği
            CurrentPlayStatistics.AverageMusicValue =
                (CurrentPlayStatistics.AverageMusicValue * PlayerManager.CurrentPlayer.GameOpenedDays.Count + SoundManager.Instance.MusicVolume)
                / (PlayerManager.CurrentPlayer.GameOpenedDays.Count + 1);

            // Birkez olsun Facebookla giriş yaptı
            if (SocialSystem.Instance.IsSignedIn<SocialFacebook>() && !CurrentPlayStatistics.AnyLogInFacebook)
                CurrentPlayStatistics.AnyLogInFacebook = true;
            // Birkez olsun GooglePlay giriş yaptı
            if (SocialSystem.Instance.IsSignedIn<SocialCloudOnce>() && !CurrentPlayStatistics.AnyLogInGooglePlay)
                CurrentPlayStatistics.AnyLogInGooglePlay = true;
            // Birkez olsun Facebookla giriş yaptı
            if (SocialSystem.Instance.IsSignedIn<SocialGameJolt>() && !CurrentPlayStatistics.AnyLogInGameJolt)
                CurrentPlayStatistics.AnyLogInGameJolt = true;

            Debug.Log("Statistics Calculated");
        }
    }

    [Serializable]
    public class StatisticData
    {
        internal string Language;
        internal float AverageSfxValue;
        internal float AverageMusicValue;
        internal bool AnyLogInFacebook;
        internal bool AnyLogInGooglePlay;
        internal bool AnyLogInGameJolt;

    }
}
//*/
