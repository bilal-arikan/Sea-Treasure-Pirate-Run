﻿//***********************************************************************//
// Bu kodu başka yerden aldım :D 
//***********************************************************************//
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Common.Tools;
using Common.Data;
using System;

namespace Common.Managers
{

    /// <summary>
    /// The game manager is a persistent singleton that handles points and time
    /// </summary>
    public partial class GameManager : Singleton<GameManager>, IManager
    {
        /// the current time scale
        [ReadOnly]
		public float TimeScale = 1f;

        /// Last play time
        [SerializeField,ReadOnly]
        float _startTime;
        [SerializeField, ReadOnly]
        float _finishTime;
        [SerializeField, ReadOnly]
        float _pausedTime;
        [SerializeField, ReadOnly]
        float _unpausedTime;

        public float PlayTime
        {
            get
            {
                if (_status != GameStatus.GameInProgress)
                    return _finishTime - _startTime;
                else
                    return Time.time - _startTime;
            }
        }

        /// the various states the game can be in
        public enum GameStatus
        {
            MainMenu,
            BeforeGameStart,
            GameInProgress,
            Paused,
            GameOver
        };

        /// the current status of the game
        [SerializeField,ReadOnly]
        protected GameStatus _status;
        public GameStatus Status
        {
            get
            {
                return _status;
            }
            protected set
            {
                _status = value;
            }
        }



        // storage
        protected float _savedTimeScale = 1;
        protected GameStatus _statusBeforePause;

        [SerializeField,ReadOnly]
        protected GameMode currentMode;
        public GameMode CurrentMode
        {
            get
            {
                return currentMode;
            }
        }

        protected override void Awake()
        {
            base.Awake();
            EventManager.StartListening(E.PlayerWon, () =>SetStatus(GameStatus.GameOver));
            EventManager.StartListening(E.PlayerLost, () =>SetStatus(GameStatus.GameOver));
            EventManager.StartListeningObjectEvent(E.LevelLoadingCompleted, (index) =>
            {
                if (LevelLoadManager.CurrentIndex.x != 0)
                    SetStatus(GameStatus.GameInProgress);
                else
                    SetStatus(GameStatus.MainMenu);
            });
        }

        /// <summary>
        /// Sets the status. Status can be accessed by other classes to check if the game is paused, starting, etc
        /// </summary>
        /// <param name="newStatus">New status.</param>
        public virtual void SetStatus(GameStatus newStatus)
        {
            E eventToTriggered = E.Unknown;

            if (newStatus == GameStatus.MainMenu)
            {
                eventToTriggered = E.MainMenu;
            }
            else if (newStatus == GameStatus.BeforeGameStart )
            {
                eventToTriggered = E.GameOnPreparing;
            }
            else if (newStatus == GameStatus.GameInProgress && Status != GameStatus.Paused)
            {
                eventToTriggered = E.GameStarted;
                _startTime = Time.time;
                _pausedTime = Time.time;
                _unpausedTime = Time.time;
            }
            else if (newStatus == GameStatus.GameOver)
            {
                eventToTriggered = E.GameFinished;
                _finishTime = Time.time;
            }
            else if (newStatus == GameStatus.Paused)
            {
                eventToTriggered = E.GamePaused;
                _pausedTime = Time.time;
            }
            else if (newStatus == GameStatus.GameInProgress && Status == GameStatus.Paused)
            {
                eventToTriggered = E.GameUnPaused;
                _unpausedTime = Time.time;
                _startTime += (_unpausedTime - _pausedTime);
            }

            Status = newStatus;

            if(eventToTriggered != E.Unknown)
                EventManager.TriggerEvent(eventToTriggered);
            Debug.Log("New Status: " + newStatus);
        }

        public virtual void SetTimeScale(float newTimeScale)
        {
            _savedTimeScale = Time.timeScale;
            TimeScale = newTimeScale;
            Time.timeScale = newTimeScale;
        }

        public virtual void ResetTimeScale()
        {
            Time.timeScale = _savedTimeScale;
        }
        
        public virtual void Pause()
        {
            // if time is not already stopped		
            if (Status != GameStatus.Paused)
            {
                SetTimeScale(0.0f);
                _statusBeforePause = Status;
                SetStatus(GameStatus.Paused);
                EventManager.TriggerEvent(E.GamePaused);
            }
        }
        
        public virtual void UnPause()
        {
            ResetTimeScale();
            _status = _statusBeforePause;
            EventManager.TriggerEvent(E.GameUnPaused);
        }

        public virtual void ChangeGameMode(GameMode mode)
        {
            currentMode = mode;
            EventManager.TriggerObjectEvent(E.GameModeChanged, mode);
        }
    }
}