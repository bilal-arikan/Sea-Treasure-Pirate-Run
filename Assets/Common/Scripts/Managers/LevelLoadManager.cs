﻿//***********************************************************************//
// Copyright (C) 2017 Bilal Arıkan. All Rights Reserved.
// Author: Bilal Arıkan
// Time  : 04.11.2017   
//***********************************************************************//
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using Common.Tools;
using Common.Data;
using Common;
using UnityEngine.UI;

namespace Common.Managers
{
    public partial class LevelLoadManager : Singleton<LevelLoadManager>, IManager
    {
        public IntChapterDict AllChapters = new IntChapterDict();
        [ReadOnly]
        public bool IsGameStarted = false;
        public bool IsMapChanging = false;
        public float LoadCompleteDelay = 0;
        public float WaitToUnlockLevel= 20;

        public int ManagersSceneIndex = 1;
        public int MainMenuSceneIndex = 2;

        [SerializeField,ReadOnly]
        private bool IsUnlocking = false;

        /// <summary>
        /// Her bölüm yüklendiğinde bu Set edilir
        /// diğer değerlerde buna göre belirlenir
        /// </summary>
        [SerializeField,ReadOnly]
        private Point2 currentIndex = new Point2();
        public static Point2 CurrentIndex
        {
            get
            {
                return Instance.currentIndex;
            }
            protected set
            {
                Instance.currentIndex = value;
            }
        }

        public static Chapter CurrentCh
        {
            get
            {
                if (CurrentIndex.x > 0)
                    return Instance.AllChapters[CurrentIndex.x];
                else
                    return null;
            }
        }
        public static SubChapter CurrentSubCh
        {
            get
            {
                if (CurrentIndex.x > 0 && CurrentIndex.y > 0)
                    return Instance.AllChapters[CurrentIndex.x][CurrentIndex.y];
                else
                    return null;
            }
        }
        public static string ActiveSubChKey
        {
            get
            {
                return LevelLoadManager.CurrentIndex.x.ToString("00") + LevelLoadManager.CurrentIndex.y.ToString("00");
            }
        }
        public static string CurrentSubChName
        {
            get
            {
                return "Ch" + CurrentSubCh.Index.ToString();
            }
        }

        protected override void Awake()
        {
            base.Awake();

            CConsole.ActionsWithArg.Add("load", (s) => 
            {
                try
                {
                    string[] xy = { s.Trim().Substring(0, 2).Trim(), s.Trim().Substring(2, 2) } ;
                    int x = int.Parse(xy[0]);
                    int y = int.Parse(xy[1]);
                    LoadLevel(new Point2(x, y));
                    /*LoadLevel(new Point(x, y), (sc) =>
                    {
                        Debug.Log("Loading Completed");
                    });*/
                }
                catch
                {
                    CConsole.LogError("Wrong Format! example usage> \"load 0205\" (with 4 digit)");
                }

            });
            CConsole.ActionsNoArg.Add("scenes", () =>
            {
                string text = "";
                foreach(var s in AllChapters.Keys)
                    text += s + ", ";
                Debug.Log("Scenes: " + text);
            });
        }

        IEnumerator Start()
        {
            if (Core.IsOneSceneGame)
            {
                IsGameStarted = true;
                EventManager.TriggerObjectEvent(E.LevelLoadingCompleted, currentIndex);
                yield break;
            }

            // Başlangıçta yüklü bölümü tespit eder (Managers dan sonraki indexden başlar)
            for (int i = MainMenuSceneIndex; i < SceneManager.sceneCountInBuildSettings; i++)
                if (SceneManager.GetSceneByBuildIndex(i).isLoaded)
                {
                    Point2 index = Point2.FromString(SceneManager.GetSceneByBuildIndex(i).name.Substring(2, 4));
                    Debug.Log("Active: " + index);
                    currentIndex = index;

                    SceneManager.SetActiveScene(SceneManager.GetSceneByBuildIndex(i));

                    goto End;
                }
            Debug.LogWarning("Active Index Not Found !");
            // TestArena açıktır diye tahmin edip indexi 9999 yapıyoruz
            currentIndex = new Point2(99,99);
            End:

            yield return new WaitForEndOfFrame();
            if (currentIndex == new Point2(99, 99))
            {
                //StartCoroutine(LoadFirstSceneCo(new int[]{ MainMenuSceneIndex }));
                LoadLevel(new Point2());
            }
            else
            {
                Debug.Log(currentIndex + " Already Loadad");
                IsGameStarted = true;
                EventManager.TriggerObjectEvent(E.LevelLoadingCompleted, currentIndex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="toLoad">AllLevels Dictionary Key</param>
        /// <param name="loadFinished"></param>
        public void LoadLevel(Point2 toLoad)
        {
            // MainMenu ise
            if(toLoad.x == 0)
            {
                StartCoroutine(
                    AsynchronousLoad(
                        MainMenuSceneIndex,
                        toLoad));
            }
            else if (toLoad.x > 0 && toLoad.y > 0)// .Exists(k => toLoad.SceneBuildIndex == k.SceneBuildIndex))
            {
                Chapter toLoadCh = null;
                if(AllChapters.TryGetValue(toLoad.x,out toLoadCh))
                {
                    StartCoroutine(
                        AsynchronousLoad(
                            toLoadCh[toLoad.y].SceneBuildIndex,
                            toLoad));
                }
                else
                {
                    Debug.LogError("Level Bulunamadı!!! (" + toLoad + ")");
                }
                //Debug.Log(toLoad + " Loading...");
            }
            else
            {
                Debug.LogError("Level Index'i Geçerli değil: " + toLoad);
            }
        }


        IEnumerator AsynchronousLoad(int buildIndex, Point2 toLoad)
        {
            LoadingStarted(toLoad);

            //Application.backgroundLoadingPriority = ThreadPriority.High;

            // Önceden yüklü bölümleri Unload eder
#pragma warning disable 0618
            for (int i = MainMenuSceneIndex; i < SceneManager.sceneCountInBuildSettings; i++)
                if (SceneManager.GetSceneByBuildIndex(i).IsValid() && SceneManager.GetSceneByBuildIndex(i).isLoaded)
                    SceneManager.UnloadSceneAsync(i);
#pragma warning restore 0618

            AsyncOperation ao = SceneManager.LoadSceneAsync(buildIndex, LoadSceneMode.Additive);
            ao.allowSceneActivation = false;

            while (!ao.isDone)
            {
                // [0, 0.9] > [0, 1]
                float progress = Mathf.Clamp01(ao.progress / 0.9f);
                //Debug.Log("Loading progress: " + (progress * 100) + "%");
                EventManager.TriggerObjectEvent(E.LevelLoadingProgressed, progress);

                // Loading completed
                if (ao.progress >= 0.9f)
                {
                    ao.allowSceneActivation = true;
                }

                yield return null;
            }

            // Kod while dan çıkıp buraya ulaştığında Scene yüklenmiştir
            yield return new WaitForSeconds(LoadCompleteDelay);

            SceneManager.SetActiveScene(SceneManager.GetSceneByBuildIndex(buildIndex));

            LoadingCompleted(toLoad);
        }


        /// <summary>
        /// Sets up all visual elements, fades from black at the start
        /// </summary>
        protected virtual void LoadingStarted(Point2 scene)
        {
            Debug.Log("LoadingStarted " + scene);
            IsMapChanging = true;

            // daha oyun yeni açıldığı için bölüm değişme ekranını gödtermemedi için
            if (!IsGameStarted)
                return;

            EventManager.TriggerObjectEvent(E.LevelLoadingStarted, scene);
            EventManager.TriggerObjectEvent(E.LevelLoadingProgressed, 0f);
        }

        /// <summary>
        /// Triggered when the actual loading is done, replaces the progress bar with the complete animation
        /// </summary>
        protected virtual void LoadingCompleted(Point2 scene)
        {
            IsGameStarted = true;

            Debug.Log("LoadingCompleted " + scene);

            //Debug.Log("NewScene: " + scene.Name);
            currentIndex = scene;
            IsMapChanging = false;

            EventManager.TriggerObjectEvent(E.LevelLoadingProgressed, 100f);
            EventManager.TriggerObjectEvent(E.LevelLoadingCompleted, scene);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>returns true if success</returns>
        public void TryUnlockSubChapter(SubChapter sch,Action<bool> success, Action<float> AdFailedTimerChanged = null)
        {
            //AdsManager Disablsa (RemoveAds itemini almakla)
            if (!AdsManager.Instance.enabled)
            {
                EventManager.TriggerObjectEvent(E.SubChapterUnlocked, sch);
                success(true);
                return;
            }

            // Revard Göstermeyi Dene
#if UNITY_EDITOR
            bool s = false;
#else
            bool s = AdsManager.Instance.TryShowReward(
                (started) =>
                {
                    Debug.Log("Unlocking > " + sch.IndexPoint.x + ":" + sch.IndexPoint.x);

                }, (reward) =>
                {
                    EventManager.TriggerObjectEvent(E.SubChapterUnlocked, sch);
                    success(true);
                });

            // Revard Gösterilemezse Interstitial Göstermeyi dene
            if (!s)
                s = AdsManager.Instance.TryShowInterstitial(
                    (showed) =>
                    {
                        if (showed)
                        {
                            EventManager.TriggerObjectEvent(E.SubChapterUnlocked, sch);
                            success(true);
                        }
                    });
#endif


            // O da gösterilemezse 
            if (!s )
            {
                // Timerı takip edecek bir fonksiyon varsa ve Timer çalıştırılmadıysa Timer çalıştırılır
                if ( AdFailedTimerChanged != null && !IsUnlocking)
                {
                    IsUnlocking = true;
                    var t = Timer.StartTimer(WaitToUnlockLevel, (finishTime) => 
                    {
                        IsUnlocking = false;
                        EventManager.TriggerObjectEvent(E.SubChapterUnlocked, sch);
                        success(true);
                    },
                    (currentTime)=> 
                    {
                        // Kalan zamanı gösterir
                        AdFailedTimerChanged(WaitToUnlockLevel - currentTime);
                    },
                    // RealTime
                    true);
                    // Sonradan tespit edip silebilmek için
                    t.transform.parent = transform;
                }
                else
                {
                    success(false);
                }
            }

        }

        public void CancelUnlockSubChapter()
        {
            IsUnlocking = false;
            var t = GetComponentInChildren<Timer>();
            if(t != null)
            {
                Destroy(t.gameObject);
            }
        }
    }

}
