﻿using Common;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Common.Managers
{
    public class TutorialManager : Singleton<TutorialManager>, IManager
    {
        [ReadOnlyPlaying]
        public bool ShowTutorials = true;
        public bool ShowEvenWhatched = false;


        public Tutorial ActiveTutorial;

        public PointTutorialDict AllTutorials = new PointTutorialDict();

        private void Start()
        {
            if (ShowTutorials)
            {
                EventManager.StartListeningObjectEvent(E.LevelLoadingCompleted, (i) =>
                {
                    if (PlayerManager.CurrentPlayer != null)
                    {
                        // ShowEvenWhatched ise her halikarda göster
                        if (ShowEvenWhatched || !PlayerManager.CurrentPlayer.WhatchedTutorials.Contains(LevelLoadManager.CurrentIndex))
                            Show(LevelLoadManager.CurrentIndex);
                        else
                            Debug.Log("Tutorial" + LevelLoadManager.CurrentIndex + " watched Before.");
                    }
                });
            }
        }

#if UNITY_EDITOR
        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Alpha0))
                Show(new Point2());
            if (Input.GetKeyDown(KeyCode.Alpha1))
                Show(new Point2(1,1));
            if (Input.GetKeyDown(KeyCode.Alpha2))
                Show(new Point2(1,2));
        }
#endif

        public void Show(Point2 index)
        {
            if(ActiveTutorial != null)
            {
                Debug.LogWarning("Zaten Bir Tutorial Aktif: " + ActiveTutorial.name);
                return;
            }

            if (AllTutorials.ContainsKey(index))
                ActiveTutorial = Instantiate(AllTutorials[index].gameObject, transform).GetComponent<Tutorial>();

            if(ActiveTutorial != null)
            {
                // Tutorial gösterilirse oyun duraklatılır
                float def = Time.timeScale;
                GameManager.Instance.SetTimeScale(0);

                ActiveTutorial.Completed.AddListener(() =>
                {
                    PlayerManager.CurrentPlayer.WhatchedTutorials.Add(index);
                    Destroy(ActiveTutorial.gameObject);
                    // Tutorial bittiğinde oyun devam eder
                    GameManager.Instance.SetTimeScale(def);
                });

                Debug.Log("Tutorial Showing: " + ActiveTutorial.name);
            }

        }
    }
}
