﻿//***********************************************************************//
// Bu kodu başka yerden aldım :D  
//***********************************************************************//
using UnityEngine;
using UnityEngine.Events;
using System;
using System.Collections.Generic;
using Common.Tools;

namespace Common.Managers
{
	public class FloatEvent : UnityEvent<float> { }
	public class IntEvent : UnityEvent<int> { }
	public class StringEvent : UnityEvent<string> { }
    public class ObjectEvent : UnityEvent<object> { }

    /// <summary>
    /// Manages the various game events
    /// Trigger them to broadcast them to other classes that have registered to it
    /// </summary>

    [ExecuteInEditMode]
    [Serializable]
    public partial class EventManager : Singleton<EventManager>, IManager
    {
        public bool TriggeringDisabled = false;
        public Dictionary <E, UnityEvent> eventDictionary = new Dictionary <E, UnityEvent>();
        public Dictionary <E, FloatEvent> floatEventDictionary = new Dictionary <E, FloatEvent>();
        public Dictionary <E, IntEvent> intEventDictionary = new Dictionary <E, IntEvent>();
        public Dictionary <E, StringEvent> stringEventDictionary = new Dictionary <E, StringEvent>();
        public Dictionary <E, ObjectEvent> objectEventDictionary = new Dictionary <E, ObjectEvent>();

        public EventIntDict EventCounts = new EventIntDict();

        /// <summary>
        /// Resets All listeners
        /// </summary>
        public void Reset ()
		{
            eventDictionary = new Dictionary<E, UnityEvent>();
            floatEventDictionary = new Dictionary<E, FloatEvent>();
            intEventDictionary = new Dictionary<E, IntEvent>();
            stringEventDictionary = new Dictionary<E, StringEvent>();
            objectEventDictionary = new Dictionary<E, ObjectEvent>();
        }

        private void Start()
        {

        }

        void OnApplicationQuit()
        {
            eventDictionary.Clear();
            floatEventDictionary.Clear();
            intEventDictionary.Clear();
            stringEventDictionary.Clear();
            objectEventDictionary.Clear();
        }

        public static void StartListening (E eventName, UnityAction listener)
	    {
	    	
			UnityEvent thisEvent = null;
	        if (Instance.eventDictionary.TryGetValue (eventName, out thisEvent))
	        {
	            thisEvent.AddListener (listener);
            } 
	        else
	        {
	            thisEvent = new UnityEvent ();
	            thisEvent.AddListener (listener);
	            Instance.eventDictionary.Add (eventName, thisEvent);
            }

            // Adedi belirlemek için
            if (Instance.EventCounts.ContainsKey(eventName))
                Instance.EventCounts[eventName]++;
            else
                Instance.EventCounts.Add(eventName, 1);

        }

        /*public static void StartListeningFloatEvent (E eventName, UnityAction<float> listener)
		{
	    	
			FloatEvent thisFloatEvent = null;
			if (Instance.floatEventDictionary.TryGetValue (eventName, out thisFloatEvent))
	        {
				thisFloatEvent.AddListener (listener);
	        } 
	        else
	        {
				thisFloatEvent = new FloatEvent ();
				thisFloatEvent.AddListener (listener);
				Instance.floatEventDictionary.Add (eventName, thisFloatEvent);
	        }
	    }

		public static void StartListeningIntEvent (E eventName, UnityAction<int> listener)
		{
	    	
			IntEvent thisIntEvent = null;
			if (Instance.intEventDictionary.TryGetValue (eventName, out thisIntEvent))
	        {
				thisIntEvent.AddListener (listener);
	        } 
	        else
	        {
				thisIntEvent = new IntEvent ();
				thisIntEvent.AddListener (listener);
				Instance.intEventDictionary.Add (eventName, thisIntEvent);
	        }
	    }

		public static void StartListeningStringEvent (E eventName, UnityAction<string> listener)
		{
	    	
			StringEvent thisStringEvent = null;
			if (Instance.stringEventDictionary.TryGetValue (eventName, out thisStringEvent))
	        {
				thisStringEvent.AddListener (listener);
	        } 
	        else
	        {
				thisStringEvent = new StringEvent ();
				thisStringEvent.AddListener (listener);
				Instance.stringEventDictionary.Add (eventName, thisStringEvent);
	        }
		}*/

        public static void StartListeningObjectEvent(E eventName, UnityAction<object> listener)
        {
            
            ObjectEvent thisObjectEvent = null;
            if (Instance.objectEventDictionary.TryGetValue(eventName, out thisObjectEvent))
            {
                thisObjectEvent.AddListener(listener);
            }
            else
            {
                thisObjectEvent = new ObjectEvent();
                thisObjectEvent.AddListener(listener);
                Instance.objectEventDictionary.Add(eventName, thisObjectEvent);
            }

            // Adedi belirlemek için
            if (Instance.EventCounts.ContainsKey(eventName))
                Instance.EventCounts[eventName]++;
            else
                Instance.EventCounts.Add(eventName, 1);
        }



        public static void StopListening (E eventName, UnityAction listener)
	    {
			
			UnityEvent thisEvent = null;
	        if (Instance.eventDictionary.TryGetValue (eventName, out thisEvent))
	        {
	            thisEvent.RemoveListener (listener);
	        }

            // Adedi belirlemek için
            if (Instance.EventCounts.ContainsKey(eventName))
                Instance.EventCounts[eventName]--;
            else
                Instance.EventCounts.Remove(eventName);
        } 	

		/*public static void StopListeningFloatEvent (E eventName, UnityAction<float> listener)
	    {
			
			FloatEvent thisFloatEvent = null;
	        if (Instance.floatEventDictionary.TryGetValue (eventName, out thisFloatEvent))
	        {
	            thisFloatEvent.RemoveListener (listener);
	        }
		}

		public static void StopListeningIntEvent (E eventName, UnityAction<int> listener)
	    {
			
			IntEvent thisIntEvent = null;
	        if (Instance.intEventDictionary.TryGetValue (eventName, out thisIntEvent))
	        {
	            thisIntEvent.RemoveListener (listener);
	        }
		}

		public static void StopListeningStringEvent (E eventName, UnityAction<string> listener)
	    {
			
			StringEvent thisStringEvent = null;
	        if (Instance.stringEventDictionary.TryGetValue (eventName, out thisStringEvent))
	        {
	            thisStringEvent.RemoveListener (listener);
	        }
	   	}*/

        public static void StopListeningObjectEvent(E eventName, UnityAction<object> listener)
        {
            
            ObjectEvent thisObjectEvent = null;
            if (Instance.objectEventDictionary.TryGetValue(eventName, out thisObjectEvent))
            {
                thisObjectEvent.RemoveListener(listener);
            }

            // Adedi belirlemek için
            if (Instance.EventCounts.ContainsKey(eventName))
                Instance.EventCounts[eventName]--;
            else
                Instance.EventCounts.Remove(eventName);
        }



        public static void TriggerEvent (E eventName,bool onMainThread = false)
	    {
            if (Instance.TriggeringDisabled)
            {
                Debug.LogWarning("Event Didnt Triggered: " + eventName);
                return;
            }


	        UnityEvent thisEvent = null;
	        if (Instance.eventDictionary.TryGetValue (eventName, out thisEvent))
	        {
                if (onMainThread)
                    Common.Core.Invoke(thisEvent.Invoke);
                else
                    thisEvent.Invoke();
	        }
		}

	    /*public static void TriggerFloatEvent (E eventName, float value)
	    {
			

	        FloatEvent thisEvent = null;
	        if (Instance.floatEventDictionary.TryGetValue (eventName, out thisEvent))
	        {
				thisEvent.Invoke (value);
	        }
		}

	    public static void TriggerIntEvent (E eventName, int value)
	    {
			

	        IntEvent thisEvent = null;
	        if (Instance.intEventDictionary.TryGetValue (eventName, out thisEvent))
	        {
				thisEvent.Invoke (value);
	        }
		}

	    public static void TriggerStringEvent (E eventName, string value)
	    {
			

	        StringEvent thisEvent = null;
	        if (Instance.stringEventDictionary.TryGetValue (eventName, out thisEvent))
	        {
				thisEvent.Invoke (value);
	        }
	    }*/

        /// <summary>
        /// Special: Triggers all other Overloaded Events
        /// </summary>
        /// <param name="eventName"></param>
        /// <param name="value"></param>
        public static void TriggerObjectEvent(E eventName, object value, bool onMainThread = false)
        {
            if (Instance.TriggeringDisabled)
            {
                Debug.LogWarning("Event Didnt Triggered: " + eventName);
                return;
            }

            ObjectEvent thisEvent = null;
            if (Instance.objectEventDictionary.TryGetValue(eventName, out thisEvent))
            {
                /*if (onMainThread)
                    Common.Core.Invoke(()=> { thisEvent.Invoke(value); });
                else*/
                    thisEvent.Invoke(value);
            }
            IntEvent intEvent;
            if (Instance.intEventDictionary.TryGetValue(eventName, out intEvent))
            {
                thisEvent.Invoke(value);
            }
            FloatEvent floatEvent;
            if (Instance.floatEventDictionary.TryGetValue(eventName, out floatEvent))
            {
                thisEvent.Invoke(value);
            }
            StringEvent stringEvent;
            if (Instance.stringEventDictionary.TryGetValue(eventName, out stringEvent))
            {
                thisEvent.Invoke(value);
            }
        }


        public static void ClearListeners(E e)
        {
            Instance.eventDictionary.Remove(e);
            Instance.objectEventDictionary.Remove(e);
        }
    }
}