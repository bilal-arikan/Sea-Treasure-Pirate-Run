﻿//***********************************************************************//
// Copyright (C) 2017 Bilal Arıkan. All Rights Reserved.
// Author: Bilal Arıkan
// Time  : 04.11.2017   
//***********************************************************************//
using System;
using System.Collections.Generic;
#if Admob
using GoogleMobileAds;
using GoogleMobileAds.Api;
#endif
using UnityEngine;
using Common.Tools;
using System.Collections;
using UnityEngine.Events;

namespace Common.Managers
{
    [ScriptOrder(100)]
    public partial class AdsManager : Singleton<AdsManager>,IManager
    {
        public bool LogAllEvents = false;
        public bool InTesting = false;
        public string AdmobAppId = "";//"ca-app-pub-5742130512939823~8349211623";
        public Action<AdType, EventType> FullDebugAction;
#if Admob
        public AdPosition BannerShowPosition = AdPosition.Bottom;
#endif
        protected bool IsInited
        {
            get
            {
#if Admob
                return (bannerView != null && interstitialView != null && rewardView != null );
#else
                return false;
#endif
            }
        }
        protected string DeviceID
        {
            get
            {
                return SystemInfo.deviceUniqueIdentifier;
            }
        }

        [SerializeField]
        protected string UnitIDBanner = "";//"ca-app-pub-5742130512939823/6896451898";
        [SerializeField]
        protected string UnitIDInterstitial = "";//"ca-app-pub-5742130512939823/7005182970";
        [SerializeField]
        protected string UnitIDRewardVideo = "";//"ca-app-pub-5742130512939823/7144574347";

        public enum AdType
        {
            Banner,
            Interstitial,
            Reward
        }
        public enum EventType
        {
            Loaded,
            FailedToLoad, // Called when an ad request failed to load.
            Opening, // Called when an ad is clicked.
            Closed, // Called when the user returned from the app after an ad click.
            LeavingApp, // Called when the ad click caused the user to leave the application.
            Started,
            Rewarded
        }

#if Admob
        protected BannerView bannerView;
        protected InterstitialAd interstitialView;
        protected RewardBasedVideoAd rewardView;

        protected AdFailedToLoadEventArgs lastErrorBanner;
        protected AdFailedToLoadEventArgs lastErrorInters;
        protected AdFailedToLoadEventArgs lastErrorReward;
        protected AdRequest request;
        protected AdRequest.Builder builder;

        protected EventHandler<EventArgs> _started;
        protected EventHandler<Reward> _rewarded;

        public bool NewRewardLoaded = false;
        public bool NewIntersLoaded = false;
        public bool NewBannerLoaded = false;

        public UnityEvent NewBannerLoadedEvent;
        public UnityEvent NewIntersLoadedEvent;
        public UnityEvent NewRewardLoadedEvent;

        public UnityEvent ClickBanner;
        public UnityEvent ClickInters;
        public UnityEvent ClickReward;

        protected override void Awake()
        {
            base.Awake();

            MobileAds.Initialize(AdmobAppId);

            if (InTesting)
            {
                builder = new AdRequest.Builder()
                    .AddTestDevice(AdRequest.TestDeviceSimulator)
                    .AddTestDevice(DeviceID);
            }
            else
            {
                builder = new AdRequest.Builder();
            }

            CConsole.ActionsNoArg.Add("ads-log", () =>
            {
                LogAllEvents = !LogAllEvents;
                Debug.Log("Ads Log Enabled: " + LogAllEvents);
            });
            CConsole.ActionsWithArg.Add("banner", (c) =>
            {
                /*Top = 0,
                Bottom = 1,
                TopLeft = 2,
                TopRight = 3,
                BottomLeft = 4,
                BottomRight = 5,
                Center = 6*/
                var cs = c.ToString().Split(' ');
                int x = int.Parse(cs[0]);
                int y = int.Parse(cs[1]);
                if (x == 0)
                    InitBanner(AdSize.Banner, (AdPosition)y);
                else if (x == 1)
                    InitBanner(AdSize.MediumRectangle, (AdPosition)y);
                else if (x == 2)
                    InitBanner(AdSize.IABBanner, (AdPosition)y);
                else if (x == 3)
                    InitBanner(AdSize.MediumRectangle, (AdPosition)y);
                else if (x == 4)
                    InitBanner(AdSize.SmartBanner, (AdPosition)y);

                Debug.Log("Banner: " + bannerView.GetWidthInPixels() + "," + bannerView.GetHeightInPixels() + " " + ((AdPosition)y).ToString());
            });

            InitBanner(AdSize.SmartBanner, BannerShowPosition);
            InitInterstitial();
            InitReward();
        }

        void Start()
        {
            Core.Invoke(CheckAdsDisabled, 1);
        }

#endif

        public void DisableAds()
        {
            enabled = false;
            HideBanner();
            Debug.Log("Ads Disabled");

            if (PlayerManager.CurrentPlayer != null)
            {
                PlayerManager.CurrentPlayer.DisabledAds = true;
                DatabaseManager.Local.Set("NoAds", true);
            }
            else
                Debug.LogWarning("DisableAds Couldn't Save CurrentPlayer Null !!!");

            EventManager.TriggerEvent(E.AdsRemoved);
        }

        public void CheckAdsDisabled()
        {
            if (!PlayerManager.CurrentPlayer.DisabledAds)
                PlayerManager.CurrentPlayer.DisabledAds = DatabaseManager.Local.Get<bool>("NoAds", false);
            if (PlayerManager.CurrentPlayer.DisabledAds)
                DisableAds();
        }

#if Admob
        public virtual void InitBanner(AdSize size, AdPosition pos)
        {
            //----------------------------------------------------------------------------------------------------------
            // Create a 320x50 banner at the top of the screen.
            if (bannerView != null)
                bannerView.Destroy();
            bannerView = new BannerView(UnitIDBanner, size, pos);
            bannerView.OnAdLoaded += delegate {
                OnAdEvent(AdType.Banner, EventType.Loaded);
            };
            bannerView.OnAdClosed += delegate {
                OnAdEvent(AdType.Banner, EventType.Closed);
            };
            /*bannerView.OnAdFailedToLoad += delegate (object o, AdFailedToLoadEventArgs e)
            {
                lastErrorBanner = e;
                OnAdEvent(AdType.Banner, EventType.FailedToLoad);
            };*/
            bannerView.OnAdOpening += delegate { OnAdEvent(AdType.Banner, EventType.Opening); };
            bannerView.OnAdLeavingApplication += delegate { OnAdEvent(AdType.Banner, EventType.LeavingApp); };

            bannerView.Hide();
            NewBannerLoaded = false;
            request = builder.Build();
            bannerView.LoadAd(request);
        }

        public virtual void InitInterstitial()
        {
            //----------------------------------------------------------------------------------------------------------
            if (interstitialView != null)
                interstitialView.Destroy();
            interstitialView = new InterstitialAd(UnitIDInterstitial);
            interstitialView.OnAdLoaded += delegate {
                OnAdEvent(AdType.Interstitial, EventType.Loaded);
            };
            interstitialView.OnAdClosed += delegate {
                OnAdEvent(AdType.Interstitial, EventType.Closed);
            };
            interstitialView.OnAdFailedToLoad += delegate (object o, AdFailedToLoadEventArgs e)
            {
                lastErrorInters = e;
                OnAdEvent(AdType.Interstitial, EventType.FailedToLoad);
            };
            interstitialView.OnAdOpening += delegate { OnAdEvent(AdType.Interstitial, EventType.Opening); };
            interstitialView.OnAdLeavingApplication += delegate { OnAdEvent(AdType.Interstitial, EventType.LeavingApp); };

            NewIntersLoaded = false;
            request = builder.Build();
            interstitialView.LoadAd(request);
        }

        public virtual void InitReward()
        {
            //----------------------------------------------------------------------------------------------------------
            if (rewardView != RewardBasedVideoAd.Instance)
            {
                rewardView = RewardBasedVideoAd.Instance;
                rewardView.OnAdLoaded += delegate {
                    OnAdEvent(AdType.Reward, EventType.Loaded);
                };
                rewardView.OnAdClosed += delegate {
                    OnAdEvent(AdType.Reward, EventType.Closed);
                };
                rewardView.OnAdFailedToLoad += delegate (object o, AdFailedToLoadEventArgs e)
                {
                    lastErrorReward = e;
                    OnAdEvent(AdType.Reward, EventType.FailedToLoad);
                };
                rewardView.OnAdOpening += delegate { OnAdEvent(AdType.Reward, EventType.Opening); };
                rewardView.OnAdLeavingApplication += delegate { OnAdEvent(AdType.Reward, EventType.LeavingApp); };
                rewardView.OnAdRewarded += delegate { OnAdEvent(AdType.Reward, EventType.Rewarded); };
                rewardView.OnAdStarted += delegate { OnAdEvent(AdType.Reward, EventType.Started); };

                request = builder.Build();
                rewardView.LoadAd(request, UnitIDRewardVideo);
            }
            else
            {
                Debug.LogError("Reward Couldnt Initialized !!!");
            }

        }
#endif


        public virtual void TryShowBanner()
        {
            if (LogAllEvents)
                CConsole.Log("Ad> Banner Show", CColor.olivedrab);

#if Admob
            if (!enabled)
            {
                Debug.Log("BannerShow: AdsManager Disabled");
                return;
            }

            if (bannerView != null)
            {
                bannerView.Show();
            }
            else
            {
#if UNITY_EDITOR
                Debug.LogWarning("BannerShow NULL");
#else
                Debug.LogError("BannerShow NULL");
#endif
            }
#endif
        }

        public virtual bool TryShowInterstitial(Action<bool> success = null)
        {
            if (LogAllEvents)
                CConsole.Log("Ad> Inters Show", CColor.olivedrab);

            if (!enabled)
            {
                Debug.Log("InterstitialShow: AdsManager Disabled");
                if (success != null)
                    success(false);
                return false;
            }
#if Admob
            if (interstitialView != null)
            {
                if (interstitialView.IsLoaded())
                {
                    interstitialView.Show();

                    if (success != null)
                        success(true);
                    return true;
                }
                else
                {
                    //interstitialView.LoadAd(request);
                    Debug.LogWarning("Intestitial Not Loaded, Please Use InitInterstitial()");
                    interstitialView.LoadAd(request);
                }
            }
            else
            {
#if UNITY_EDITOR
                Debug.LogWarning("InterstitialShow NULL");
#else
                Debug.LogError("InterstitialShow NULL");
#endif
            }
#endif
            if (success != null)
                success(false);
            return false;
        }

        public virtual bool TryShowReward(Action<EventArgs> started, Action<int> rewarded)
        {
            if (LogAllEvents)
                CConsole.Log("Ad> Reward Show", CColor.olivedrab);

#if UNITY_EDITOR && Admob
            started.Invoke(new EventArgs() { });
            rewarded.Invoke(10);
            return true;
#elif Admob
            if (rewardView != null)
            {
                if (_started != null)
                    rewardView.OnAdStarted -= _started;
                if (_rewarded != null)
                    rewardView.OnAdRewarded -= _rewarded;

                _started = delegate (object o, EventArgs e) { 
                    started.Invoke(e); 
                };
                _rewarded = delegate (object o, Reward e) {
                    rewarded.Invoke((int)e.Amount);
                }; ;

                if (_started != null)
                    rewardView.OnAdStarted += _started;
                if (_rewarded != null)
                    rewardView.OnAdRewarded += _rewarded;

                if (rewardView.IsLoaded())
                {
                    rewardView.Show();
                    return true;
                }
                else
                {
                    Debug.LogWarning("Reward Not Loaded, Please Use InitReward()");
                    request = builder.Build();
                    rewardView.LoadAd(request, UnitIDRewardVideo);
                    return false;
                }
            }
            else
            {
                Debug.LogError("RewardShow NULL");
                InitReward();
                return false;
            }
#else
            return false;
#endif
        }


        public virtual void HideBanner()
        {
#if Admob
            if (bannerView != null)
            {
                bannerView.Hide();
            }
            else
            {
                Debug.LogWarning("BannerHide NULL");
            }
#endif
        }

#if Admob
        /*IEnumerator LoadCo(AdType type)
        {
            yield return new WaitForSeconds(3);

            if(type == AdType.Banner)
            {
                bannerView.LoadAd(request);
            }
            else if(type == AdType.Interstitial)
            {
                interstitialView.LoadAd(request);
            }
            else if(type == AdType.Reward)
            {
                rewardView.LoadAd(request, UnitIDRewardVideo);
            }
        }*/

        /// <summary>
        /// Every action Calls this method
        /// </summary>
        /// <param name="adType"></param>
        /// <param name="eventType"></param>
        protected virtual void OnAdEvent(AdType adType, EventType eventType)
        {
            if (LogAllEvents)
                CConsole.Log("Ad> " + adType + ": " + eventType,CColor.olivedrab);
            if (FullDebugAction != null)
                FullDebugAction.Invoke(adType, eventType);

            if (adType == AdType.Banner)
            {
                if (eventType == EventType.FailedToLoad)
                {
                    //Debug.LogError("BannerError: "+ lastErrorBanner != null ? lastErrorBanner.Message : "");
                }
                else if(eventType == EventType.Loaded)
                {
                    NewBannerLoaded = true;
                    NewBannerLoadedEvent.Invoke();
                }
                else if (eventType == EventType.Closed)
                {
                    request = builder.Build();
                    NewBannerLoaded = false;
                    bannerView.LoadAd(request);
                }
                else if (eventType == EventType.Opening)
                {
                    ClickBanner.Invoke();
                }
            }
            else if (adType == AdType.Interstitial)
            {
                if (eventType == EventType.FailedToLoad)
                {
                    //Debug.LogError("InterstitialError: " + lastErrorInters != null ? lastErrorInters.Message : "");
                }
                else if (eventType == EventType.Loaded)
                {
                    NewIntersLoaded = true;
                    NewIntersLoadedEvent.Invoke();
                }
                else if (eventType == EventType.Closed)
                {
                    request = builder.Build();
                    NewIntersLoaded = false;
                    interstitialView.LoadAd(request);
                }
                else if (eventType == EventType.Opening)
                {
                    ClickInters.Invoke();
                }
            }
            else if (adType == AdType.Reward)
            {
                if (eventType == EventType.FailedToLoad)
                {
                    //Debug.LogError("RewardError: " + (lastErrorReward != null ? lastErrorReward.Message : ""));
                    Core.Invoke(() =>
                    {
                        request = builder.Build();
                        if (!rewardView.IsLoaded())
                            rewardView.LoadAd(request, UnitIDRewardVideo);
                        else
                            CConsole.Log("Reward Timed Load: Already Loaded",CColor.olivedrab);
                    }, 2);
                }
                else if (eventType == EventType.Loaded)
                {
                    NewRewardLoaded = true;
                    NewRewardLoadedEvent.Invoke();
                }
                else if (eventType == EventType.Closed)
                {
                    request = builder.Build();
                    NewRewardLoaded = false;
                    rewardView.LoadAd(request, UnitIDRewardVideo);
                }
                else if (eventType == EventType.Opening)
                {
                    ClickReward.Invoke();
                }
            }
        }
#endif

    }
}
//*/
