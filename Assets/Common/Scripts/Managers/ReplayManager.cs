﻿//***********************************************************************//
// Copyright (C) 2017 Bilal Arıkan. All Rights Reserved.
// Author: Bilal Arıkan
// Time  : 04.11.2017   
//***********************************************************************//
using System;
using System.Collections;
using System.Linq;
using System.Text;
using UnityEngine;
using Common.Tools;

namespace Common.Managers
{
    public partial class ReplayManager : Singleton<ReplayManager>, IManager
    {
        public ReplayTransform Object;

        protected override void Awake()
        {
            base.Awake();
        }

        ReplayData data;
        void Update()
        {
            if (Input.GetKeyDown(KeyCode.A))
                data = Object.StartRecording();
            if (Input.GetKeyUp(KeyCode.A))
                Object.StopRecording(data,(d) => { Debug.Log("TotalFrames:" + d.Frames.Count); });

        }

    }
}
