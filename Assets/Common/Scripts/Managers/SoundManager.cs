﻿//***********************************************************************//
// Bu kodu başka yerden aldım
//***********************************************************************//
using UnityEngine;
using System.Collections;
using Common.Tools;
using Common.Data;

namespace Common.Managers
{	
	/// <summary>
	/// This persistent singleton handles sound playing
	/// </summary>
	public partial class SoundManager : Singleton<SoundManager>, IManager
    {	
		/// true if the music is enabled	
		public bool MusicOn
        {
            get
            {
                if (backgroundMusic != null)
                    return backgroundMusic.volume > 0;
                else
                    return false;
            }
            set
            {
                if(backgroundMusic != null)
                {
                    if (value)
                        backgroundMusic.volume = MusicVolume;
                    else
                        backgroundMusic.volume = 0;
                }
            }
        }
		/// true if the sound fx are enabled
		public bool SfxOn
        {
            get
            {
                return AudioListener.volume > 0;
            }
            set
            {
                if (value)
                    AudioListener.volume = SfxVolume;
                else
                    AudioListener.volume = 0;
            }
        }

        /// the music volume
        [SerializeField,ReadOnlyPlaying]
        [Range(0,1)]
		float musicVolume=0.3f;
        public float MusicVolume
        {
            get
            {
                return musicVolume;
            }
            set
            {
                musicVolume = value;
                if (backgroundMusic != null)
                    backgroundMusic.volume = value;
            }
        }

        /// the sound fx volume
        [SerializeField,ReadOnlyPlaying]
        [Range(0,1)]
		float sfxVolume=1f;
        public float SfxVolume
        {
            get
            {
                return sfxVolume;
            }
            set
            {
                sfxVolume = value;
                AudioListener.volume = value;
            }
        }

        [SerializeField]
        protected AudioSource backgroundMusic;

        public MusicData[] BackgorundMenuMusics;
        public MusicData[] BackgorundGamePlayMusics;

        public AudioClip WinAudio;
        public AudioClip LostAudio;

        protected override void Awake()
        {
            base.Awake();
            AudioListener.volume = SfxVolume;
            if (backgroundMusic == null)
                backgroundMusic = gameObject.GetOrAddComponent<AudioSource>();
            backgroundMusic.volume = MusicVolume;

            SfxOn = DatabaseManager.Local.Get("SfxOn", true);
            MusicOn = DatabaseManager.Local.Get("MusicOn", true);

            // Level geçişleri
            EventManager.StartListening(E.GameStarted, () =>
            {
                var md = BackgorundGamePlayMusics.GetRandom();
                backgroundMusic.clip = md.MusicClip;
                backgroundMusic.Play();

                if(MusicOn)
                    EventManager.TriggerObjectEvent(E.BackgroundMusicChanged, md);
            });
            // Level geçişleri
            EventManager.StartListening(E.MainMenu, () =>
            {
                var md = BackgorundMenuMusics.GetRandom();
                backgroundMusic.clip = md.MusicClip;
                backgroundMusic.Play();

                if (MusicOn)
                    EventManager.TriggerObjectEvent(E.BackgroundMusicChanged, md);
            });
            // Bölüm Kazanılırsa
            EventManager.StartListening(E.PlayerWon, () =>
            {
                SoundManager.Instance.PlaySound(WinAudio);
            });
            // Bölüm Kaybedilirse
            EventManager.StartListening(E.PlayerLost, () =>
            {
                SoundManager.Instance.PlaySound(LostAudio);
            });
        }

        void OnApplicationQuit()
        {
            DatabaseManager.Local.Set("SfxOn", SfxOn);
            DatabaseManager.Local.Set("MusicOn", MusicOn);
        }

        /// <summary>
        /// Plays a background music.
        /// Only one background music can be active at a time.
        /// </summary>
        /// <param name="Clip">Your audio clip.</param>
        public virtual void PlayBackgroundMusic(AudioSource Music)
		{
			// if the music's been turned off, we do nothing and exit
			if (!MusicOn)
				return;
			// if we already had a background music playing, we stop it
			if (backgroundMusic!=null)
				backgroundMusic.Stop();
			// we set the background music clip
			backgroundMusic=Music;
			// we set the music's volume
			backgroundMusic.volume=MusicVolume;
			// we set the loop setting to true, the music will loop forever
			backgroundMusic.loop=true;
			// we start playing the background music
			backgroundMusic.Play();		
		}	

		/// <summary>
		/// Plays a sound
		/// </summary>
		public virtual void PlaySound(AudioClip Sfx, Vector3 Location)
		{
			if (!SfxOn || Sfx == null)
				return;
            AudioSource.PlayClipAtPoint(Sfx, Location);
		}
        /// <summary>
        /// Plays a sound on Camera position
        /// </summary>
        public virtual void PlaySound(AudioClip Sfx)
        {
            if (!SfxOn || Sfx == null)
                return;
            AudioSource.PlayClipAtPoint(Sfx, Camera.main.transform.position);
        }

    }
}