﻿//***********************************************************************//
// Unitynin sayfasından aldım
//***********************************************************************//
using System;
using System.Collections.Generic;
using UnityEngine;
using System.Collections;
#if IAP
using UnityEngine.Purchasing;
#endif
using Common.Tools;

namespace Common.Managers
{


    [ExecuteInEditMode]
    public partial class IAPManager : Singleton<IAPManager>, IManager
#if IAP
       , IStoreListener
#endif
    {

        [Serializable]
        public class ProductInfo
        {
#if IAP

            internal ProductInfo(Product p)
            {
                Id = p.transactionID;
                Available = p.availableToPurchase;
                DefinitionId = p.definition.id;
                IsoCurrencyCode = p.metadata.isoCurrencyCode;
                LocalizedDescription = p.metadata.localizedDescription;
                LocalizedPrice = p.metadata.localizedPrice;
                LocalizedTitle = p.metadata.localizedTitle;
                LocalizedPriceString = p.metadata.localizedPriceString;
            }

            public string Id;
            public bool Available;
            public string DefinitionId;
            public string IsoCurrencyCode;
            public string LocalizedTitle;
            public string LocalizedDescription;
            public decimal LocalizedPrice;
            public string LocalizedPriceString;
#endif
        }

        // Product identifiers for all products capable of being purchased: 
        // "convenience" general identifiers for use with Purchasing, and their store-specific identifier 
        // counterparts for use with and outside of Unity Purchasing. Define store-specific identifiers 
        // also on each platform's publisher dashboard (iTunes Connect, Google Play Developer Console, etc.)

        // General product identifiers for the consumable, non-consumable, and subscription products.
        // Use these handles in the code to reference which product to purchase. Also use these values 
        // when defining the Product Identifiers on the store. Except, for illustration purposes, the 
        // kProductIDSubscription - it has custom Apple and Google identifiers. We declare their store-
        // specific mapping to Unity Purchasing's AddProduct, below.

        public bool ShowToastOnFailed = true;
        public string PurchaseFailedText ="Purchasing Failed";


#if UNITY_EDITOR
        [SerializeField,ReadOnly]
#endif
        List<string> Consumables = new List<string>();

#if UNITY_EDITOR
        [SerializeField,ReadOnly]
#endif
        List<string> NonConsumables = new List<string>();

        /// <summary>
        /// All Available Products
        /// </summary>
        public List<ProductInfo> Products = new List<ProductInfo>();

#if IAP
        private static IStoreController m_StoreController;          // The Unity Purchasing system.
        private static IExtensionProvider m_StoreExtensionProvider; // The store-specific Purchasing subsystems.

        IEnumerator Start()
        {
            yield return new WaitForEndOfFrame();

            Consumables.Clear();
            foreach (var cons in typeof(IAP.Cons).GetFields())
                Consumables.Add((string)cons.GetValue(null));
            NonConsumables.Clear();
            foreach (var cons in typeof(IAP.NonCons).GetFields())
                NonConsumables.Add((string)cons.GetValue(null));



            if (!Application.isPlaying)
                yield break;

#if UNITY_IOS
            EventManager.StartListening(E.IAPInitialized, () =>
             {
                 RestorePurchases();
             });
#endif

            yield return new WaitForSecondsRealtime(0.5f);

            // If we haven't set up the Unity Purchasing reference
            if (m_StoreController == null)
            {
                // Begin to configure our connection to Purchasing
                Init();

                CConsole.ActionsNoArg.Add("products", () =>
                {
                    CConsole.Log(ObjectSerializer.ToJson(Products, true));
                });
            }
        }

        public void Init()
        {
            // If we have already connected to Purchasing ...
            if (IsInitialized())
            {
                CConsole.Log("IAP initialized",CColor.greenyellow);
                return;
            }

            // Create a builder, first passing in a suite of Unity provided stores.
            var builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());

            // Add a product to sell / restore by way of its identifier, associating the general identifier
            // with its store-specific identifiers.
            foreach (var s in Consumables)
                builder.AddProduct(s, ProductType.Consumable);
            foreach (var s in NonConsumables)
                builder.AddProduct(s, ProductType.NonConsumable);

            // Kick off the remainder of the set-up with an asynchrounous call, passing the configuration 
            // and this class' instance. Expect a response either in OnInitialized or OnInitializeFailed.
            UnityPurchasing.Initialize(this, builder);
    }


    private bool IsInitialized()
        {
            // Only say we are initialized if both the Purchasing references are set.
            return m_StoreController != null && m_StoreExtensionProvider != null;
        }
#endif



            public void BuyProductID(string productId)
        {
#if IAP
            //Sistemi online şekilde yazarsak altif ederiz
            /*if(AuthenticationManager.UserIsValid)
            {
                UIManager.Instance.ShowWarning("Please Login with \na Valid User !!!");
                return;
            }*/

            EventManager.TriggerObjectEvent(E.ItemPurchasingStarted, productId);

            // If Purchasing has been initialized ...
            if (IsInitialized())
            {
                Products.Clear();
                foreach (var p in m_StoreController.products.all)
                    Products.Add(new ProductInfo(p));

                // ... look up the Product reference with the general product identifier and the Purchasing 
                // system's products collection.
                Product product = m_StoreController.products.WithID(productId);

                // If the look up found a product for this device's store and that product is ready to be sold ... 
                if (product != null && product.availableToPurchase)
                {

                    CConsole.Log(string.Format("Purchasing product: '{0}'", product.definition.id),CColor.greenyellow);
                    // ... buy the product. Expect a response either through ProcessPurchase or OnPurchaseFailed 
                    // asynchronously.
                    m_StoreController.InitiatePurchase(product);
                }
                // Otherwise ...
                else
                {
                    EventManager.TriggerObjectEvent(E.ItemPurchasingError, productId);

                    if (ShowToastOnFailed)
                        UIManager.Instance.ShowToast(PurchaseFailedText);
                    // ... report the product look-up failure situation  
                    Debug.LogError("BuyProductID: FAIL. Not purchasing product, either is not found or is not available for purchase");
                }
            }
            // Otherwise ...
            else
            {
                EventManager.TriggerObjectEvent(E.ItemPurchasingError, productId);

                if (ShowToastOnFailed)
                    UIManager.Instance.ShowToast(PurchaseFailedText);
                // ... report the fact Purchasing has not succeeded initializing yet. Consider waiting longer or 
                // retrying initiailization.
                Debug.LogError("BuyProductID FAIL. Not initialized.");
            }
#endif
        }


        // Restore purchases previously made by this customer. Some platforms automatically restore purchases, like Google. 
        // Apple currently requires explicit purchase restoration for IAP, conditionally displaying a password prompt.
        public void RestorePurchases()
        {
#if IAP
            // If Purchasing has not yet been set up ...
            if (!IsInitialized())
            {
                // ... report the situation and stop restoring. Consider either waiting longer, or retrying initialization.
                Debug.LogError("RestorePurchases FAIL. Not initialized.");
                return;
            }

            // If we are running on an Apple device ... 
            if (Application.platform == RuntimePlatform.IPhonePlayer ||
                Application.platform == RuntimePlatform.OSXPlayer)
            {
                // ... begin restoring purchases
                CConsole.Log("RestorePurchases started ...",CColor.greenyellow);


                // Fetch the Apple store-specific subsystem.
                var apple = m_StoreExtensionProvider.GetExtension<IAppleExtensions>();
                // Begin the asynchronous process of restoring purchases. Expect a confirmation response in 
                // the Action<bool> below, and ProcessPurchase if there are previously purchased products to restore.
                apple.RestoreTransactions((result) =>
                {
                    // The first phase of restoration. If no more responses are received on ProcessPurchase then 
                    // no purchases are available to be restored.
                    CConsole.Log("RestorePurchases continuing: " + result + ". If no further messages, no purchases available to restore.",CColor.greenyellow);
                });

            }
            // Otherwise ...
            else
            {
                // We are not running on an Apple device. No work is necessary to restore purchases.
                Debug.LogError("RestorePurchases FAIL. Not supported on this platform. Current = " + Application.platform);
            }
#endif
        }


        //  
        // --- IStoreListener
        //
#if IAP
        public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
        {
            // Purchasing has succeeded initializing. Collect our Purchasing references.
            //Debug.Log("OnInitialized: PASS");

            // Overall Purchasing system, configured with products for this application.
            m_StoreController = controller;
            // Store specific subsystem, for accessing device-specific store features.
            m_StoreExtensionProvider = extensions;
             
            EventManager.TriggerEvent(E.IAPInitialized);
        }


        public void OnInitializeFailed(InitializationFailureReason error)
        {
            // Purchasing set-up has not succeeded. Check error for reason. Consider sharing this reason with the user.
            Debug.LogError("IAP OnInitializeFailed:" + error);
        }


        public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs args)
        {
            foreach (var s in Consumables)
            {
                // A consumable product has been purchased by this user.
                if (String.Equals(args.purchasedProduct.definition.id, s, StringComparison.Ordinal))
                {
                    CConsole.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id),CColor.greenyellow);
                    // The consumable item has been successfully purchased, add 100 coins to the player's in-game score.
                    EventManager.TriggerObjectEvent(E.ItemPurchased, args.purchasedProduct.definition.id);
                    return PurchaseProcessingResult.Complete;
                }
            }
            foreach (var s in NonConsumables)
            {
                // A consumable product has been purchased by this user.
                if (String.Equals(args.purchasedProduct.definition.id, s, StringComparison.Ordinal))
                {
                    CConsole.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id),CColor.greenyellow);
                    // The consumable item has been successfully purchased, add 100 coins to the player's in-game score.
                    EventManager.TriggerObjectEvent(E.ItemPurchased, args.purchasedProduct.definition.id);
                    return PurchaseProcessingResult.Complete;
                }
            }

            // Return a flag indicating whether this product has completely been received, or if the application needs 
            // to be reminded of this purchase at next app launch. Use PurchaseProcessingResult.Pending when still 
            // saving purchased products to the cloud, and when that save is delayed. 
            return PurchaseProcessingResult.Complete;
        }


        public void OnPurchaseFailed(Product product, PurchaseFailureReason failureReason)
        {
            // A product purchase attempt did not succeed. Check failureReason for more detail. Consider sharing 
            // this reason with the user to guide their troubleshooting actions.
            Debug.LogError(string.Format("IAP OnPurchaseFailed: FAIL. Product: '{0}', PurchaseFailureReason: {1}", product.definition.storeSpecificId, failureReason));

            switch (failureReason)
            {
                case PurchaseFailureReason.DuplicateTransaction:
                    break;
                case PurchaseFailureReason.ExistingPurchasePending:
                    break;
                case PurchaseFailureReason.PaymentDeclined:
                    break;
                case PurchaseFailureReason.ProductUnavailable:
                    break;
                case PurchaseFailureReason.PurchasingUnavailable:
                    break;
                case PurchaseFailureReason.SignatureInvalid:
                    break;
                case PurchaseFailureReason.Unknown:
                    break;
                case PurchaseFailureReason.UserCancelled:
                    break;
            }

            if (failureReason == PurchaseFailureReason.UserCancelled)
                EventManager.TriggerObjectEvent(E.ItemPurchasingCanceled, product.definition.storeSpecificId);
            else
                EventManager.TriggerObjectEvent(E.ItemPurchasingError, product.definition.storeSpecificId);
        }
#endif
    }
}
//*/
