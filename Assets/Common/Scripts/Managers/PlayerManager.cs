﻿//***********************************************************************//
// Copyright (C) 2017 Bilal Arıkan. All Rights Reserved.
// Author: Bilal Arıkan
// Time  : 04.11.2017   
//***********************************************************************//
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Common.Tools;
using Common.Data;

namespace Common.Managers
{
    public partial class PlayerManager: Singleton<PlayerManager>, IManager
    {

        [SerializeField]
        Data.Player currentPlayer;
        public static Player CurrentPlayer
        {
            get { return Instance.currentPlayer; }
        }

        [SerializeField]
        Profile currentProfile;
        public static Profile CurrentProfile
        {
            get
            {
                return Instance.currentProfile;
            }
            internal set
            {
                Instance.currentProfile = value;
            }
        }

        [SerializeField,ReadOnly]
        private bool isFirstPlaying;
        public static bool IsFirstPlaying
        {
            get
            {
                return Instance.isFirstPlaying;
            }
        }

        [ReadOnly]
        public bool PlayerLoaded = false;
        [ReadOnly]
        public bool ProfileLoaded = false;
        
        public int[] LevelSteps = { 0, 15, 32, 65, 150, 285, 400 };



        protected override void Awake()
        {
            base.Awake();
            EventManager.StartListeningObjectEvent(E.PlayerProfileChanged, (p) => {
                if (CurrentProfile.Photo == null && !string.IsNullOrEmpty(CurrentProfile.PhotoUrl))
                    Core.DownloadImage(CurrentProfile.PhotoUrl, (s) => { CurrentProfile.Photo = s; });
            });
            EventManager.StartListening(E.DatabaseReset, () =>
            {
                currentPlayer = new Player();
                currentProfile = new Profile();
            });
            LoadPlayerData();
            LoadPlayerProfile();
        }

        void OnApplicationQuit()
        {
            SavePlayerData();
#if UNITY_EDITOR
            Debug.Log("Player Saved while Closing:\n"+ObjectSerializer.ToJson(CurrentPlayer,true));
#endif
        }
        void OnApplicationPause()
        {
            SavePlayerData();
#if UNITY_EDITOR
            Debug.Log("Player Saved while Pausing:\n" + ObjectSerializer.ToJson(CurrentPlayer, true));
#endif
        }

        /// <summary>
        /// Günlük girişten kazanılan hediye varmı kontrol eder
        /// </summary>
        protected void CheckDailyGifts()
        {
            if (ConfigManager.CurrentDayNumber == 0)
                Debug.LogWarning("Tarih bilgisi Sunucudan Alınamadı !!!");

            if( !CurrentPlayer.GameOpenedDays.Contains(ConfigManager.CurrentDayNumber) )
            {
                for(int i = 0; i < 7; i++)
                {
                    // kaydedilen günlerden öncesine bakılamaz
                    if((CurrentPlayer.GameOpenedDays.Count - i - 1 >= 0) &&
                    // Son kaydedilen gün, bugünün 1 öncesi mi?
                        (CurrentPlayer.GameOpenedDays.ElementAt(CurrentPlayer.GameOpenedDays.Count - i - 1) == ConfigManager.CurrentDayNumber - i - 1))
                    {
                    }
                    else
                    {
                        //(i+1). Günün hediyesi
                        EventManager.TriggerObjectEvent(E.DailyGift, i+1);
                        break;
                    }
                }

                // Hediyeler alındıktan sonra şu an ki gün kaydedilir
                CurrentPlayer.GameOpenedDays.Add(ConfigManager.CurrentDayNumber);
            }
            else
            {
                Debug.Log("Zaten Bugünün Hediyesini Aldın");
            }
        }

        public void LoadPlayerProfile()
        {
            currentProfile = new Profile();
            currentProfile.PhotoUrl = C.invite_image.StringValue;
            ProfileLoaded = true;
            EventManager.TriggerObjectEvent(E.PlayerProfileChanged, currentProfile);
        }

        public void LoadPlayerData()
        {
            CConsole.Log("Player Data Loading...", CColor.cyan);

            // Localden Data yı alır
            var player = DatabaseManager.Local.Get<Data.Player>((string)("Players/" + PlayerManager.CurrentPlayer.ID));
            if (player != null)
            {
                currentPlayer = player;
                isFirstPlaying = false;
                CConsole.Log("Player Loaded: From Local", CColor.cyan);
            }
            // Ordanda alınamazsa yeni Player oluştur
            else
            {
                currentPlayer = new Data.Player();
                isFirstPlaying = true;
                SavePlayerData();
                CConsole.Log("No Data (New Player Created)", CColor.cyan);
            }
            PlayerLoaded = true;

            // Reklam kaldırma ürünü satın alındıysa
            AdsManager.Instance.enabled = !currentPlayer.DisabledAds;

            EventManager.TriggerObjectEvent(E.PlayerDataChanged, CurrentPlayer);
        }

        public void SavePlayerData()
        {
            // Her halukarda Locale kaydet
            DatabaseManager.Local.Set("Players/" + PlayerManager.CurrentPlayer.ID, CurrentPlayer);
            CConsole.Log("Player Saved to Local\n" + ObjectSerializer.ToJson(PlayerManager.CurrentPlayer,true),CColor.cyan);
            EventManager.TriggerObjectEvent(E.PlayerDataSaved, CurrentPlayer);
        }

        public void SavePlayerProfile()
        {
            // Her halukarda Locale kaydet
            DatabaseManager.Local.Set("Profiles/" + PlayerManager.CurrentPlayer.ID, CurrentProfile);
            CConsole.Log("Profile Saved to Local", CColor.cyan);
            EventManager.TriggerObjectEvent(E.PlayerProfileSaved, CurrentProfile);
        }

        public void SaveScore(PassedSubCh psc)
        {
            CurrentPlayer.PassedSubChapterScores.AddOrChange(LevelLoadManager.CurrentIndex, psc);

            //ve Sunucuya kaydetme kodları olmalı
            //Ama şimdilik devre dışı
        }
    }
}
