﻿//***********************************************************************//
// Copyright (C) 2017 Bilal Arıkan. All Rights Reserved.
// Author: Bilal Arıkan
// Time  : 04.11.2017   
//***********************************************************************//
using System;
using System.Collections.Generic;
using UnityEngine;
using MarkLight;
using Common.Tools;
using Common.Managers;
using MarkLight.Views.UI;

namespace Common.Managers
{
    public partial class UIManager : Singleton<UIManager>, IManager
    {
        [ReadOnly]
        public SceneUI Scene;
        [ReadOnly]
        public View LastMainView;
        [ReadOnly]
        public View MainMenuView;

        public ScreenOrientation Orientation = ScreenOrientation.Landscape;

        public Sprite DefaultToastSprite;
        public List<Sprite> LoadingViewSprites = new List<Sprite>();

        [SerializeField, ReadOnly]
        private Vector2 DefaultRefResolution;
        protected Vector2 CurrentReferefnceResolution
        {
            get
            {
                return Scene.UserInterface.CanvasScaler.referenceResolution;
            }
        }

        public ElementSize AskToUSerDefaultWidth;
        public ElementSize AskToUSerDefaultHeight;
        public ElementSize WarningDefaultWidth;
        public ElementSize WarningDefaultHeight;


        protected override void Awake()
        {
            Screen.orientation = Orientation;
            base.Awake();
            Init();
        }

        public virtual void Init()
        {
            EventManager.StartListeningObjectEvent(E.LevelLoadingStarted, LevelLoadingStarted);
            EventManager.StartListeningObjectEvent(E.LevelLoadingProgressed, LevelLoadingProgressed);
            EventManager.StartListeningObjectEvent(E.LevelLoadingCompleted, LevelLoadingCompleted);

            EventManager.StartListening(E.AppStarted, () =>
            {
                DefaultRefResolution = Scene.UserInterface.CanvasScaler.referenceResolution;

                AskToUSerDefaultWidth = Scene.AreYouSure.MainRegion.Width.Value;
                AskToUSerDefaultHeight = Scene.AreYouSure.MainRegion.Height.Value;
                WarningDefaultWidth = Scene.Warning.MainRegion.Width.Value;
                WarningDefaultHeight = Scene.Warning.MainRegion.Height.Value;
            });
#if FirebaseMessaging
            // Firebaseden Mesaj Alındığında
            EventManager.StartListeningObjectEvent(E.ReceivedMessage, (m) =>
            {
                Firebase.Messaging.FirebaseMessage msg = m as Firebase.Messaging.FirebaseMessage;
                string data;
                if(msg.Data.TryGetValue("ShowWarning",out data))
                {
                    ShowWarning(data);
                }
                if (msg.Data.TryGetValue("ShowToast", out data))
                {
                    ShowToast(data);
                }
                if (msg.Data.TryGetValue("ShowAskToUser", out data))
                {
                    string url = msg.Data.GetValueOrDefault("Url");

                    ShowAskToUser(data, (s) =>
                    {
                        if (s)
                            Application.OpenURL(url);
                    });
                }
            });
#endif
            // Spinner Show/Hide
            CConsole.ActionsNoArg.Add("spinner", () =>
            {
                UIManager.Instance.SetWaitingtState(!Scene.LoadingSpinner.IsActive.Value);
            });
            // Spinner Show/Hide
            CConsole.ActionsWithArg.Add("hide-ui", (s) =>
            {
                try
                {
                    float seconds = int.Parse(s.ToString());
                    var defM = MainMenuView;
                    ChangeSubView<Region>();
                    ChangeMainView<Region>();
                    Core.Invoke(() => ChangeMainView(defM.GetType()), seconds);
                }
                catch
                {
                    CConsole.LogError("Parameter must be float !");
                }
            });
            CConsole.ActionsWithArg.Add("notif", (s) =>
            {
                var ps = s.Split(' ');
                //UnityEngine.iOS.LocalNotification.SendNotification(UnityEngine.Random.Range(1, 1000), int.Parse(ps[0]), ps[1], ps[2], Color.green);
            });
            CConsole.ActionsWithArg.Add("notif2", (s) =>
            {
                var ps = s.Split(' ');
                //UnityEngine.iOS.LocalNotification.SendRepeatingNotification (UnityEngine.Random.Range(1, 1000), int.Parse(ps[0]), int.Parse(ps[1]), ps[2], ps[3], Color.green);
            });
        }

        /// <summary>
        /// UserInterface.CanvasScaler.referenceResolution
        /// </summary>
        /// <param name="res"></param>
        public void SetUIScale(Vector2 res)
        {
            Scene.UserInterface.CanvasScaler.referenceResolution = res;
        }
        public void SetUIScale(float multiplication)
        {
            Scene.UserInterface.CanvasScaler.referenceResolution = DefaultRefResolution * multiplication;
        }
        public void ResetUIScale()
        {
            Scene.UserInterface.CanvasScaler.referenceResolution = DefaultRefResolution;
        }

        /// <summary>
        /// Change Main Screen
        /// </summary>
        /// <param name="t"></param>
        public void ChangeMainView<T>() where T : UIView
        {
            ChangeMainView(typeof(T));
        }
        public void ChangeMainView(Type t)
        {
            Scene.SubViewSwitcher.SwitchTo(0, false);

            LastMainView = Scene.MainViewSwitcher.ActiveView;
            if (t == null || t == typeof(Region))
            {
                Scene.MainViewSwitcher.SwitchTo(0, false);
            }
            else
            {
                Scene.MainViewSwitcher.SwitchTo(UIManager.Instance.Scene.MainViews[t], false);
            }
        }

        /// <summary>
        /// Change Main Screen
        /// </summary>
        /// <param name="t"></param>
        public void ChangeSubView<T>(bool showDarkBackground = true) where T : UIView
        {
            ChangeSubView(typeof(T), showDarkBackground);
        }
        public void ChangeSubView(Type t, bool showDarkBackground = true)
        {
            if (t == null || t == typeof(Region))
            {
                Scene.SubViewSwitcher.SwitchTo(UIManager.Instance.Scene.SubViews[typeof(Region)]);
                Scene.DarkBackRegion.IsActive.Value = false;
            }
            else
            {
                if (showDarkBackground)
                    Scene.DarkBackRegion.IsActive.Value = true;
                Scene.SubViewSwitcher.SwitchTo(UIManager.Instance.Scene.SubViews[t]);
            }
        }

        /// <summary>
        /// </summary>
        /// <param name="text"></param>
        /// <param name="accept">Answer Result</param>
        public void ShowAskToUser(string text, Action<bool> accept, float width = -1, float height = -1)
        {
            if (width > 0)
                Scene.AreYouSure.MainRegion.Width.Value = new ElementSize(width, ElementSizeUnit.Percents);
            else
                Scene.AreYouSure.MainRegion.Width.Value = AskToUSerDefaultWidth;

            if (height > 0)
                Scene.AreYouSure.MainRegion.Height.Value = new ElementSize(height, ElementSizeUnit.Percents);
            else
                Scene.AreYouSure.MainRegion.Height.Value = AskToUSerDefaultHeight;

            Scene.AreYouSure.Show(text, accept);
        }

        public void ShowWarning(string text, Action ok = null, float width = -1, float height = -1)
        {
            if (width > 0)
                Scene.Warning.MainRegion.Width.Value = new ElementSize(width, ElementSizeUnit.Percents);
            else
                Scene.Warning.MainRegion.Width.Value = WarningDefaultWidth;

            if (height > 0)
                Scene.Warning.MainRegion.Height.Value = new ElementSize(height, ElementSizeUnit.Percents);
            else
                Scene.Warning.MainRegion.Height.Value = WarningDefaultHeight;

            Scene.Warning.Show(text, ok);
        }

        public void ShowToast(string text, Sprite sprite = null, float time = 3, int width = -1, int height = -1)
        {
            Debug.Log(text);
            if (Scene != null)
            {
                Common.Core.Invoke(() =>
                {
                    Scene.Toast.Show(sprite ?? DefaultToastSprite, text);
                });
            }
        }

#if GameJolt
        public void ShowToastGameJolt(string text, Sprite s = null)
        {
            if (s == null)
                GameJolt.UI.Manager.Instance.QueueNotification(text);
            else
                GameJolt.UI.Manager.Instance.QueueNotification(text, s);
        }
#endif

        public void SetWaitingtState(bool show)
        {
            if (show)
            {
                Scene.LoadingSpinner.Show();
                Debug.Log("Spinner On");
            }
            else
            {
                Scene.LoadingSpinner.Hide();
                Debug.Log("Spinner Off");
            }
        }

        /*public void ShowColorSelector(Action<Color> colorSelected, Action<bool> answer)
        {
            Scene.ColorS.Show(colorSelected, answer);
        }*/

        void LevelLoadingStarted(object index)
        {
            LoadingView.Instance.MainSprite.Value = new SpriteAsset(string.Empty, LoadingViewSprites.GetRandom());

            ChangeSubView<Region>();
            ChangeMainView<LoadingView>();
        }

        void LevelLoadingProgressed(object percent)
        {
            LoadingView.Instance.Percent.Value = "% " + (int)((float)percent * 100);
        }

        void LevelLoadingCompleted(object index)
        {
            //ChangeSubView<Region>();
            if (LevelLoadManager.CurrentIndex.x == 0)
                ChangeMainView<MenuView>();
            else
                ChangeMainView<GameView>();
        }
    }
}