﻿//***********************************************************************//
// Copyright (C) 2017 Bilal Arıkan. All Rights Reserved.
// Author: Bilal Arıkan
// Time  : 04.11.2017   
//***********************************************************************//
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Common.Tools;
using MarkLight;

namespace Common.Managers
{
    /// <summary>
    /// ISO 639-1
    /// </summary>
    public partial class LanguageManager : Singleton<LanguageManager>, IManager
    {
        [SerializeField,ReadOnlyPlaying]
        private string langID = "en";
        public static string CurrentLanguage
        {
            get
            {
                return Instance.langID;
            }
        }

        protected override void Awake()
        {
            base.Awake();
#if !UNITY_EDITOR
            Debug.Log("Available Languages: en,tr,ja,zh,ko,de,h,ru,es,ar");
#endif
            EventManager.StartListeningObjectEvent(E.LanguageChanged, (l) =>
            {
                string lang = l.ToString();

                if (lang == "ar")
                {
                    var texts = GameObject.FindObjectsOfType<UnityEngine.UI.Text>();
                    foreach (var t in texts)
                        t.text = ArabicSupport.ArabicFixer.Fix(t.text, true, true);
                    Debug.Log("Arabic Words Fixed " + texts.Length);
                }
            });
            EventManager.StartListening(E.ConfigsFetched, () =>
            {
                Debug.Log("RemoteConfig Lang:" + C.def_language.StringValue);
            });

            Common.CConsole.ActionsWithArg.Add("setlanguage", (s) => { LanguageManager.ChangeLanguage(s); });
        }

        /// <summary>
        /// </summary>
        /// <param name="id"></param>
        public static void ChangeLanguage(string id)
        {
            if (Instance.langID == id)
                return;

            Instance.langID = id;
            ResourceDictionary.SetConfiguration(id);
            ResourceDictionary.NotifyObservers(); // update bindings
                                                  //DatabaseManager.Language = id;
            EventManager.TriggerObjectEvent(E.LanguageChanged, id);

        }
    }
}


public static partial class L
{
    const string ResName = "Loc";

    public static string GetText(string textID)
    {
        bool hasValue;
        string text = ResourceDictionary.GetValue(ResName, textID, out hasValue);

        if (string.IsNullOrEmpty(text))
            return "";
        else
            return text;
    }
}