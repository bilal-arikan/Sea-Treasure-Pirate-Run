﻿//***********************************************************************//
// Copyright (C) 2017 Bilal Arıkan. All Rights Reserved.
// Author: Bilal Arıkan
// Time  : 04.11.2017   
//***********************************************************************//
using System;
using System.Collections.Generic;
using UnityEngine;
using Common.Tools;
using System.Collections.Specialized;
using System.Collections;
using Common.Data;
using System.Linq;
#if FirebaseConfig
using Firebase.RemoteConfig;
#endif

namespace Common.Managers
{
    /// <summary>
    /// 
    /// </summary>
    public partial class ConfigManager : Singleton<ConfigManager>, IManager
    {
#if FirebaseConfig
        [ReadOnlyPlaying]
        public Firebase.LogLevel FirebaseLogLevel = Firebase.LogLevel.Info;
#endif
        protected bool _syncronized = false;
        public bool IsSyncronized
        {
            get
            {
                return _syncronized;
            }
        }



        [SerializeField,ReadOnly]
        long currentDayNumber;
        public static long CurrentDayNumber
        {
            get
            {
                if (Instance.currentDayNumber == 0)
                    return (long)((DateTime.Now - DateTime.MinValue).TotalDays);
                else
                    return Instance.currentDayNumber;
            }
            
        }

        [SerializeField,ReadOnly]
        bool availableNewVersion = false;
        public bool AvailableNewVersion
        {
            get
            {
                return availableNewVersion;
            }
        }

        [SerializeField,ReadOnly]
        private int currentVersion = 1;
        public static int CurrentVersion
        {
            get
            {
                return Instance.currentVersion;
            }
        }

        [SerializeField,ReadOnly]
        private int playstoreVersion = 1;
        public static int PlaystoreVersion
        {
            get
            {
                return Instance.playstoreVersion;
            }
        }

        protected override void Awake()
        {
            currentVersion = GetVersionNumber();
            base.Awake();

            EventManager.StartListening(E.ConfigsFetched, () =>
            {
                // Check new Version
                CheckNewVersion();

                if (AvailableNewVersion)
                    UIManager.Instance.ShowToast("Available New Version!");

                if (C.console_show_exceptions.IsAvailable)
                    CConsole.Instance.ShowOnException = C.console_show_exceptions.BooleanValue;
                if (C.console_show_errors.IsAvailable)
                    CConsole.Instance.ShowOnError = C.console_show_errors.BooleanValue;
            });
            CConsole.ActionsWithArg.Add("quality", (arg) =>
            {
                SetGraphicQuality(int.Parse(arg));
            });
        }

        void Start()
        {
            currentDayNumber = (long)(Core.GetNetworkTime() - DateTime.MinValue).TotalDays;
            FetchConfigs();

            //Debug.Log("CurrentVersion: " + Application.version);

            if (Application.internetReachability == NetworkReachability.NotReachable)
                Debug.LogWarning("No Internet Connection");

#if FirebaseConfig
            CConsole.ActionsWithArg.Add("firebaseloglevel", (arg) =>
            {
                int result;
                if (int.TryParse(arg, out result))
                {
                    Firebase.FirebaseApp.LogLevel = (Firebase.LogLevel)result;
                    Debug.Log("Firebase LogLevel: " + Firebase.FirebaseApp.LogLevel);
                }
                else
                    CConsole.Log("Parameter Error ! exm:\"firebaseloglevel 2\"");
            });
#endif
        }


        /// <summary>
        /// Fetch configs from Firebase server
        /// if fails, use defaults values
        /// </summary>
        public virtual void FetchConfigs()
        {
#if FirebaseConfig
            Firebase.FirebaseApp.LogLevel = FirebaseLogLevel;
#if UNITY_EDITOR
            FirebaseRemoteConfig.Settings = new ConfigSettings() { IsDeveloperMode = true };
#endif
            Dictionary<string, object> Defs = new Dictionary<string, object>();
            foreach (var kv in DefaultConfigs.DictInstance)
                Defs.Add(kv.Key, kv.Value);

            Debug.Log("Configs Fetching: started");
            Firebase.RemoteConfig.FirebaseRemoteConfig.SetDefaults(Defs);
            Firebase.FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task =>
            {
                Firebase.DependencyStatus dependencyStatus = task.Result;
                if (dependencyStatus == Firebase.DependencyStatus.Available)
                {
                    Firebase.RemoteConfig.FirebaseRemoteConfig.FetchAsync().ContinueWith(OnFetchingConfigsFinished);
                }
                else
                {
                    Debug.LogError("Could not resolve all Firebase dependencies: " + dependencyStatus);
                }
            });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="task"></param>
        protected virtual void OnFetchingConfigsFinished(System.Threading.Tasks.Task task)
        {
            ConfigInfo info = FirebaseRemoteConfig.Info;
            switch (info.LastFetchStatus)
            {
                case LastFetchStatus.Success:
                    //////////////////////////////////////////////////////////////////////////
                    FirebaseRemoteConfig.ActivateFetched();
                    _syncronized = true;
                    Debug.Log(String.Format("Remote Configs loaded (last fetch time {0}) LastVersion:{1}.", info.FetchTime,C.googlestore_version));

#if UNITY_EDITOR
                    FetchedConfigs.Clear();
                    foreach (string s in FirebaseRemoteConfig.Keys)
                        FetchedConfigs.Add(s, GetConfigValue(s).ToString());
#endif
                    //////////////////////////////////////////////////////////////////////////
                    break;
                case LastFetchStatus.Failure:
                    switch (info.LastFetchFailureReason)
                    {
                        case FetchFailureReason.Error:
                            Debug.LogError("Fetch failed for Invalid Reason: " + task.Exception.Message);
                            break;
                        case FetchFailureReason.Throttled:
                            Debug.Log("Fetch throttled until " + info.ThrottledEndTime);
                            break;
                    }
                    break;
                case LastFetchStatus.Pending:
                    Debug.LogError("Latest Fetch call still pending.");
                    break;
            }


            EventManager.TriggerEvent(E.ConfigsFetched);
#endif
        }

        public void ServerTime(Action<DateTime> result)
        {
#if GameJolt
            GameJolt.API.Misc.GetTime(result);
#endif
        }

        /// <summary>
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public CConfigValue GetConfigValue(string key)
        {
            try
            {
#if FirebaseConfig
                ConfigValue cc;
                    cc = FirebaseRemoteConfig.GetValue(key);
                //Debug.Log("CC>" + key + ">" + cc.StringValue + ":" + cc.ByteArrayValue.Count());
                return new CConfigValue(cc.StringValue, cc.ByteArrayValue.Count() > 0);
#else
                //Debug.Log("CC>" + key + ">" + cc.StringValue + ":" + cc.ByteArrayValue.Count());
                return new CConfigValue(DefaultConfigs[key], false);
#endif
            }
            catch
            {
                return new CConfigValue();
            }
        }

        public void SetGraphicQuality(int quality)
        {
            Debug.Log("Quality Set: " + quality);
            QualitySettings.SetQualityLevel(quality, true);
        }
        

        public int GetVersionNumber()
        {
            int number = 1;
            var parts = Application.version.Trim().Split('.');
            if (parts.Length == 3)
            {
                try
                {
                    number = 0;
                    number += int.Parse(parts[0]) * 1000000;
                    number += int.Parse(parts[1]) * 1000;
                    number += int.Parse(parts[2]);
                }
                catch (Exception e)
                {
                    Debug.LogError("CheckNewVersion " + e.Message);
                    number = 1;
                }
            }
            return number;
        }

        public bool CheckNewVersion()
        {
            playstoreVersion = (int)C.googlestore_version.LongValue;

            //Debug.Log("Version: " + playstoreVersion + googlestore_version + currentVersion);

            availableNewVersion = currentVersion < playstoreVersion;
            if (availableNewVersion)
                EventManager.TriggerObjectEvent(E.AvailableNewVersion, playstoreVersion);

            return availableNewVersion;
        }

        public void GetNewsAndReleaseNotes(Action<List<New>> newsResult, Action<List<string>> releaseResult)
        {
            List<New> news = ObjectSerializer.FromJson<List<New>>(C.news_json.StringValue);
            List<string> releases = ObjectSerializer.FromJson<List<string>>(C.releases_json.StringValue);

            newsResult(news);
            releaseResult(releases);
        }



        [Serializable]
        public class New
        {
            public string DeepUrl;
            public string ImageUrl;

            [Newtonsoft.Json.JsonIgnore]
            public Sprite Image;
            [Newtonsoft.Json.JsonIgnore]
            public MarkLight.SpriteAsset NewSpriteAsset
            {
                get
                {
                    return new MarkLight.SpriteAsset(DeepUrl, Image);
                }
                set
                {
                    Image = value.Sprite;
                }
            }
        }

        public class CConfigValue
        {
            readonly bool available = false;
            readonly string Value;

            public CConfigValue()
            {

            }

            public CConfigValue(string data,bool available)
            {
                this.available = available;
                Value = data;
            }

            public bool IsAvailable
            {
                get
                {
                    return available;
                }
            }
            public bool BooleanValue
            {
                get
                {
                    return bool.Parse(Value);
                }
            }
            public IEnumerable<byte> ByteArrayValue
            {
                get
                {
                    return Value.ToCharArray().ConvertAll(c => (byte)c);
                }
            }
            public double DoubleValue
            {
                get
                {
                    return double.Parse(Value);
                }
            }
            public float FloatValue
            {
                get
                {
                    return float.Parse(Value);
                }
            }
            public long LongValue
            {
                get
                {
                    return long.Parse(Value);
                }
            }
            public int IntValue
            {
                get
                {
                    return int.Parse(Value);
                }
            }
            public string StringValue
            {
                get
                {
                    return Value;
                }
            }

            public override string ToString()
            {
                return Value;
            }
        }
    }
}
