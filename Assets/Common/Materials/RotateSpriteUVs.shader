﻿Shader "Custom/RotateUVs" {
	Properties{
		_Color("Main Color", Color) = (1,1,1,1)
		_MainTex("Color (RGB) Alpha (A)", 2D) = "white"
		_RotationSpeed("Rotation Speed", Float) = 200.0
	}
		SubShader{
		Tags{ "RenderType" = "Transparent" "Queue" = "Transparent" }
		Blend SrcAlpha OneMinusSrcAlpha
		LOD 200

		CGPROGRAM
#pragma surface surf Lambert vertex:vert alpha


		sampler2D _MainTex;
	fixed4 _Color;
	float _RotationSpeed;

	struct Input {
		float2 uv_MainTex;
	};

	void vert(inout appdata_full v) {
		v.texcoord.xy -= 0.5;
		float s = sin(_RotationSpeed * _Time);
		float c = cos(_RotationSpeed * _Time);
		float2x2 rotationMatrix = float2x2(c, -s, s, c);
		rotationMatrix *= 0.5;
		rotationMatrix += 0.5;
		rotationMatrix = rotationMatrix * 2 - 1;
		v.texcoord.xy = mul(v.texcoord.xy, rotationMatrix);
		v.texcoord.xy += 0.5;
	}


	void surf(Input IN, inout SurfaceOutput o) {
		fixed4 c = tex2D(_MainTex, IN.uv_MainTex);
		o.Albedo = c.rgb;

		o.Alpha = c.a;
	}

	ENDCG
	}
		Fallback "Unlit/Transparent Cutout"
}