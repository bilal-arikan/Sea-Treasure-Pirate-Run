﻿using Common.Data;
using Common.Managers;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using MarkLight;
using Arikan;

public class CaptainPlayer : Singleton<CaptainPlayer> {
    public Vector3 TargetPosition
    {
        get
        {
            return Agent.destination;
        }
    }
    [ReadOnly]
    public NavMeshAgent Agent;
    [ReadOnly]
    public PortController DockedPort;

    public SpriteRenderer HealthBarRenderer;

    [ReadOnlyPlaying]
    public int MaxHealth = 10;
    [ReadOnlyPlaying]
    public int Health= 3;

    public Dictionary<Good, int> Inventory = new Dictionary<Good, int>();
    public int OwnedChestCount = 0;
    public int StartingGold = 30;

    // Use this for initialization
    protected override void Awake ()
    {
        base.Awake();
        Agent = GetComponent<NavMeshAgent>();
        GameManager.Instance.Player = this;

        if(C.player_health.IsAvailable)
            Health = C.player_health.IntValue;
        if(C.player_maxhealth.IsAvailable)
            MaxHealth = C.player_maxhealth.IntValue;

        HealthBarRenderer.size = new Vector2(Health, 1);

        EventManager.StartListening(E.InventoryChanged, () =>
        {
            if(DockedPort != null)
            {
                GameView.Instance.ActivePrices.ForEach(p => { if (p.gameObject.activeInHierarchy) p.OnShow(); });
            }

            if (PlayerManager.CurrentPlayer.Xp >= 100000)
                SocialSystem.Instance.SetAchievement("GoldenShip");
        });
    }

    IEnumerator Start()
    {
        GameView.Instance.Inventory.Clear();
        foreach (var g in PreLoadeds.Instance.AllGoods)
        {
            Inventory.Add(g, 0);
            GameView.Instance.Inventory.Add(new ListItemGood(g, 0));
        }

        if (C.starting_chest.IsAvailable)
            OwnedChestCount = C.starting_chest.IntValue;
        GameView.Instance.ChestCount.Value = OwnedChestCount;
        GameView.Instance.ButtonOpenChest.IsActive.Value = OwnedChestCount > 0;

        if (C.starting_gold.IsAvailable)
            StartingGold = C.starting_gold.IntValue;
        Inventory[PreLoadeds.Instance.Gold] = StartingGold;

        yield return new WaitForEndOfFrame();
        CalculateFortune();
        EventManager.TriggerEvent(E.InventoryChanged);
        EventManager.TriggerObjectEvent(E.PlayerLevelUp,1);
    }

    void CalculateFortune()
    {
        int sum = 0;
        foreach (var kv in Inventory)
            sum += kv.Value * kv.Key.FortuneMultiplication;

        PlayerManager.CurrentPlayer.Xp = sum;
    }

    /// <summary>
    /// Return true if still alive
    /// </summary>
    /// <param name="damage"></param>
    /// <returns></returns>
    public bool AddDamage(int damage)
    {
        Health = Mathf.Clamp(Health - damage , 0, MaxHealth);
        HealthBarRenderer.size = new Vector2(Health, 1);

        if (Health == 0)
        {
            GetComponent<Collider>().enabled = false;
            EventManager.TriggerEvent(E.PlayerLost);
            Destroy(gameObject, 1);
        }

        if (Health == 10)
            SocialSystem.Instance.SetAchievement("MostPowerfull");

        return Health > 0;
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(TargetPosition, 1);
    }


    public bool TryBuy(Good ToBuy, int amount, Good ToGive, int price,bool asMuchAs = false)
    {
        int ownedToBuyCount = Inventory.GetValueOrDefault(ToBuy);
        int ownedToGiveCount = Inventory.GetValueOrDefault(ToGive);

        bool inventoryChanged = false;

        if (asMuchAs && ownedToGiveCount < amount * price)
        {
            // Alabildiğimiz kadar alıyoruz
            int canBuy = ownedToGiveCount / price;
            Inventory[ToGive] -= canBuy * price;
            Inventory[ToBuy] = ownedToBuyCount + canBuy;
            inventoryChanged = true;
        }
        else if(ownedToGiveCount >= amount * price)
        {
            Inventory[ToGive] -= amount * price;
            Inventory[ToBuy] = ownedToBuyCount + amount;
            inventoryChanged = true;
        }

        if (inventoryChanged)
        {
            CalculateFortune();
            EventManager.TriggerEvent(E.InventoryChanged);
        }

        return inventoryChanged;
    }

    public bool TrySell(Good ToSell, int amount, Good ToTake, int sellPrice, bool asMuchAs = false)
    {
        int ownedToSellCount = Inventory.GetValueOrDefault(ToSell);
        int ownedToTakeCount = Inventory.GetValueOrDefault(ToTake);

        bool inventoryChanged = false;

        if (asMuchAs && ownedToSellCount < amount)
        {
            // Elimizdekilerin hepsini satıyoruz
            Inventory[ToSell] = 0;
            Inventory[ToTake] = ownedToTakeCount + ownedToSellCount * sellPrice;
            inventoryChanged = true;
        }
        else if (ownedToSellCount >= amount)
        {
            Inventory[ToSell] -= amount;
            Inventory[ToTake] = ownedToTakeCount + amount * sellPrice;
            inventoryChanged = true;
        }

        if (inventoryChanged)
        {
            CalculateFortune();
            EventManager.TriggerEvent(E.InventoryChanged);
        }

        return inventoryChanged;
    }


    public void AddToInventory(Good g, int amount)
    {
        if (Inventory.ContainsKey(g))
            Inventory[g] += amount;
        else
            Inventory.Add(g, amount);

        CalculateFortune();
        EventManager.TriggerEvent(E.InventoryChanged);
    }
}
