﻿using Common.Managers;
using Common.Tools;
using UnityEngine;

public class PlayerFollowerAudioListener : Singleton<PlayerFollowerAudioListener> {
	void LateUpdate () {
        if (GameManager.Instance != null && GameManager.Instance.Player != null)
            transform.position = GameManager.Instance.Player.transform.position;
    }
}
