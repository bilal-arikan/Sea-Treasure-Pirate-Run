﻿using Common.Data;
using Common.Managers;
using Common.Tools;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class LootableSpawner : Singleton<LootableSpawner>
{
    public int MaxLootableCount = 10;
    public float LootableSpawnSeconds = 5;

    public Transform LootablesParent;
    [ReadOnly]
    public Transform[] SpawnPoints;
    int LastSpawnPointIndex = 0;


    protected override void Awake()
    {
        base.Awake();
        SpawnPoints = GameObject.FindGameObjectsWithTag("Respawn").ConvertAll(o => o.transform);
        SpawnPoints.Shuffle();

        if(C.max_lootable_count.IsAvailable)
            MaxLootableCount = C.max_lootable_count.IntValue;
        if (C.lootable_spawn_time.IsAvailable)
            LootableSpawnSeconds = C.lootable_spawn_time.FloatValue;
    }

    private void Start()
    {
        StartCoroutine(SpawnCo());
    }

    IEnumerator SpawnCo()
    {
        while (true)
        {
            SpawnLoot(LastSpawnPointIndex);
            LastSpawnPointIndex++;
            if (LastSpawnPointIndex >= SpawnPoints.Length)
            {
                LastSpawnPointIndex = 0;
                SpawnPoints.Shuffle();
            }

            yield return new WaitForSeconds(LootableSpawnSeconds);
        }
    }

    public Lootable SpawnLoot(int index = -1)
    {
        //LastSpawnPoint = GetRandomLocation();

        if (PreLoadeds.Instance.ToSpawnLootablePrefabs.Count > 0 && index >= 0 && index < PreLoadeds.Instance.ToSpawnLootablePrefabs.Count)
        {
            var t = SpawnPoints.GetRandom();
            var l = Instantiate(PreLoadeds.Instance.ToSpawnLootablePrefabs[index], t.position, Quaternion.identity, LootablesParent);
            l.SpawnedTransform = t;
            return l;
        }
        else if (MaxLootableCount > GameManager.Instance.ActiveLootables.Count)
        {
            var t = SpawnPoints.GetRandom();
            var l = Instantiate(PreLoadeds.Instance.ToSpawnLootablePrefabs.GetRandom(), t.position, Quaternion.identity, LootablesParent);
            l.SpawnedTransform = t;
            return l;
        }
        else
            return null;
    }

    Vector3 GetRandomLocation()
    {
        NavMeshTriangulation navMeshData = NavMesh.CalculateTriangulation();

        // Pick the first indice of a random triangle in the nav mesh
        int t = UnityEngine.Random.Range(0, navMeshData.indices.Length - 3);

        // Select a random point on it
        Vector3 point = Vector3.Lerp(navMeshData.vertices[navMeshData.indices[t]], navMeshData.vertices[navMeshData.indices[t + 1]], UnityEngine.Random.value);
        Vector3.Lerp(point, navMeshData.vertices[navMeshData.indices[t + 2]], UnityEngine.Random.value);

        //Debug.Log("RP:" + point);

        return point;
    }

    /*private void OnDrawGizmos()
    {
        Gizmos.DrawCube(LastSpawnPoint, Vector3.one * 5);
    }*/
}
