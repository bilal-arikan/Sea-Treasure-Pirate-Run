﻿using Common.Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class Chest : MonoBehaviour
{
    public AudioClip LootAudio;
    public AudioClip OpenAudio;
    public Sprite ChestIcon;

    private void OnTriggerEnter(Collider other)
    {
        var player = other.GetComponent<CaptainPlayer>();
        if (player != null)
        {
            if (LootAudio != null)
                SoundManager.Instance.PlaySound(LootAudio, transform.position);

            EventManager.TriggerEvent(E.ChestLooted);

            Destroy(gameObject);
        }
    }
}
