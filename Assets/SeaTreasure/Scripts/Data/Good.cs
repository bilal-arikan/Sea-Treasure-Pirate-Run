﻿using MarkLight;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

[Serializable]
[CreateAssetMenu(fileName = "Good-", menuName = "Common/Good")]
public class Good : ScriptableObject
{
    public string Name;
    public Sprite Image;
    public LootType Type;
    public int FortuneMultiplication = 1;
}
