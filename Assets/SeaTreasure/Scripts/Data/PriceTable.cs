﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

[Serializable]
//[CreateAssetMenu(fileName = "Price-", menuName = "Common/PriceTable")]
public class PriceTable //: ScriptableObject
{
    public Good GoodToBuy;
    public Good MoneyToGive;

    public List<Point2> LowPricesGold = new List<Point2>();
    public List<Point2> MiddlePricesGold = new List<Point2>();
    public List<Point2> HighPricesGold = new List<Point2>();
}

