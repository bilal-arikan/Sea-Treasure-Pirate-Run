﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using Common.Tools;
using System.Collections;

namespace Common.Data
{
    public partial class PreLoadeds : Singleton<PreLoadeds>
    {
        public Chest ChestPrefab;
        public RepairTool RepairToolPrefab;
        public Lootable GoldPrefab;
        public Lootable DiamondPrefab;
        public List<Lootable> ToSpawnLootablePrefabs = new List<Lootable>();

        public Good Gold;
        public Good Diamond;
        public List<Good> AllGoods = new List<Good>();

        public TextAsset AllPricesText;


        public List<PriceTable> PriceTables = new List<PriceTable>();
        public List<Price> LowPrices = new List<Price>();
        public List<Price> MediumPrices = new List<Price>();
        public List<Price> HighPrices = new List<Price>();


        public List<CannonBall> CannonBallPrefabs = new List<CannonBall>();
        public List<Pirate> PiratePrefabs = new List<Pirate>();
        public List<Pirate> PirateBossPrefabs = new List<Pirate>();
        public List<ParticleSystem> ExplosionPrefabs = new List<ParticleSystem>();

        public Sprite MoneyProduct;
        public Sprite SpecMoneyProduct;
        public Sprite IAPProduct;

        private void Start()
        {

            if (AllPricesText)
            {
                var s = AllPricesText.text;
                var lines = s.Split('\n');

                PriceTable table = null;
                foreach (var line in lines)
                {
                    if (line[0] == 'P')
                    {
                        var pros = line.Split(',');
                        table = new PriceTable();
                        PriceTables.Add(table);
                        table.GoodToBuy = AllGoods.Find(g => g.Name == pros[1]);
                        table.MoneyToGive = AllGoods.Find(g => g.Name == pros[2]);
                    }
                    else if (line[0] == 'L')
                    {
                        var pric = line.Split(',');
                        table.LowPricesGold.Add(new Point2(int.Parse(pric[1]), int.Parse(pric[2])));
                    }
                    else if (line[0] == 'M')
                    {
                        var pric = line.Split(',');
                        table.MiddlePricesGold.Add(new Point2(int.Parse(pric[1]), int.Parse(pric[2])));
                    }
                    else if (line[0] == 'H')
                    {
                        var pric = line.Split(',');
                        table.HighPricesGold.Add(new Point2(int.Parse(pric[1]), int.Parse(pric[2])));
                    }
                }

                LowPrices.Clear();
                MediumPrices.Clear();
                HighPrices.Clear();
                foreach (var t in PriceTables)
                {
                    foreach (var p in t.LowPricesGold)
                        LowPrices.Add(new Price(t.GoodToBuy, t.MoneyToGive, p));
                    foreach (var p in t.MiddlePricesGold)
                        MediumPrices.Add(new Price(t.GoodToBuy, t.MoneyToGive, p));
                    foreach (var p in t.HighPricesGold)
                        HighPrices.Add(new Price(t.GoodToBuy, t.MoneyToGive, p));
                }

                Debug.Log("Prices Parsed From Text File: " + PriceTables.Count);
            }

        }
    }
}
