﻿using Common.Managers;
using System;
using System.Collections.Generic;
using UnityEngine;


namespace Common.Data
{
    public partial class Player
    {
        public int BestScore = 0;
        public int DestroyedPirateAmount = 0;
    }
}
