﻿using Arikan;
using Common;
using Common.Data;
using Common.Managers;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityStandardAssets.Utility;

public enum PirateType
{
    Normal = 0,
    Boss = 9
}

public class Pirate : MonoBehaviour {

    [HideInInspector]
    public NavMeshAgent Agent;
    public bool IsPlayerVisible
    {
        get
        {
            return GameManager.Instance.Player != null &&
                GameManager.Instance.Player.enabled &&
                (transform.position - GameManager.Instance.Player.transform.position).magnitude < VisionLength;
        }
    }

    public int Health = 1;
    public PirateType Type;

    [Space(20)]
    public float VisionLength = 20;
    public int DropCount = 1;
    public float DropAreaRadius = 1;
    [Range(0, 1)]
    public float ChestDropChange = 0.3f;
    [Range(0, 1)]
    public float RepairToolChange = 0.6f;
    public float LookCoSeconds = 1.5f;

    public int CannonBallCount = 1;
    public float SerialFireSeconds = 1;

    public float CannonBallSpeed;
    public float NextFireStepSeconds = 5;

    [ReadOnly]
    public float NextFireTime;

    private Collider Coll;
    public SpriteRenderer HealthBarRenderer;

    void Awake()
    {
        Coll = GetComponent<Collider>();
        Agent = GetComponent<NavMeshAgent>();
        GameManager.Instance.ActivePirates.Add(this);

        if (Health > 1)
            HealthBarRenderer.size = new Vector2(Health, 1);
        else
            HealthBarRenderer.enabled = false;
    }

    // Use this for initialization
    void Start () {
        NextFireTime = Time.timeSinceLevelLoad + NextFireStepSeconds;
        StartCoroutine(LookCo());

        if(LevelLoadManager.CurrentIndex.x != 0)
            GameView.Instance.PirateCount.Value = GameManager.Instance.ActivePirates.Count;
    }

    private void OnDestroy()
    {
        GameManager.Instance.ActivePirates.Remove(this);

        if (!LevelLoadManager.Instance.IsMapChanging && !Core.IsApplicationQuitting)
        {
            GameView.Instance.PirateCount.Value = GameManager.Instance.ActivePirates.Count;
            GameManager.Instance.DestroyedPirateCount++;

            if(GameManager.Instance.Player != null && GameManager.Instance.Player.Health == 1)
                GameManager.Instance.AchRevenge++;

            if (GameManager.Instance.DestroyedPirateCount >= 100)
                SocialSystem.Instance.SetAchievement("PirateHunter");
            if (GameManager.Instance.AchRevenge >= 20)
                SocialSystem.Instance.SetAchievement("Revenge");

            // Patlama efekti
            Instantiate(PreLoadeds.Instance.ExplosionPrefabs[1], transform.position, Quaternion.identity);

            // Drop Count adet Drop oluştururu
            for(int i = 0;i <DropCount; i++)
            {
                var dropOffset = (Random.insideUnitCircle * DropAreaRadius);
                Vector3 dropPoint = transform.position + new Vector3(dropOffset.x, 0, dropOffset.y);

                if (UnityEngine.Random.value < ChestDropChange)
                    Instantiate(PreLoadeds.Instance.ChestPrefab, dropPoint, Quaternion.Euler(-90, 0, 0).WithRandomY());
                else if (UnityEngine.Random.value < RepairToolChange)
                    Instantiate(PreLoadeds.Instance.RepairToolPrefab, dropPoint, Quaternion.Euler(-90, 0, 0).WithRandomY());
            }

        }

    }

    IEnumerator LookCo()
    {
        while (true)
        {
            yield return new WaitForSeconds(LookCoSeconds);

            if (Time.timeSinceLevelLoad > NextFireTime && IsPlayerVisible)
            {
                for(int i = 0; i < CannonBallCount; i++)
                {
                    if (!IsPlayerVisible)
                        break;

                    FireCannonToTarget();
                    yield return new WaitForSeconds(SerialFireSeconds);
                }
                NextFireTime = Time.timeSinceLevelLoad + NextFireStepSeconds;
            }

            if (IsPlayerVisible)
            {
                Agent.SetDestination(GameManager.Instance.Player.transform.position);
            }
            else if(Agent.velocity.magnitude == 0 && PirateSpawner.Instance != null)
            {
                var random = PirateSpawner.Instance.SpawnPoints.GetRandom();
                if(random)
                    Agent.SetDestination(random.position);
            }

        }
    }

    /// <summary>
    /// Return true if still alive
    /// </summary>
    /// <param name="damage"></param>
    /// <returns></returns>
    public bool AddDamage(int damage)
    {
        Health = Mathf.Clamp(Health - damage, 0, 1000);
        HealthBarRenderer.size = new Vector2(Health, 1);


        if (Health == 0)
            Destroy(gameObject);

        return Health > 0;
    }


    public void FireCannonToTarget()
    {
        //Debug.Log("Cannon Fired by " + name);
        var o = Instantiate(PreLoadeds.Instance.CannonBallPrefabs.GetRandom(), transform.position, Quaternion.identity);
        o.FiredShip = gameObject;
        var autoSpeed = o.GetComponent<AutoMoveAndRotate>();
        autoSpeed.moveUnitsPerSecond = new AutoMoveAndRotate.Vector3andSpace()
        {
            space = Space.World,
            value = (GameManager.Instance.Player.transform.position - transform.position).normalized * CannonBallSpeed
        };
            
    }


    private void OnDrawGizmos()
    {
        if(IsPlayerVisible && GameManager.Instance.Player != null)
        {
            Gizmos.DrawLine(transform.position, GameManager.Instance.Player.transform.position);
        }
        Gizmos.DrawWireSphere(transform.position, VisionLength);
    }
}
