﻿using Common.Data;
using Common.Managers;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CannonBall : MonoBehaviour {
    public int DamageAmount = 1;
    public GameObject FiredShip;
    public List<AudioClip> FireSounds = new List<AudioClip>();
    public List<AudioClip> WaterSplashSounds = new List<AudioClip>();
    public List<AudioClip> ExplodedSounds = new List<AudioClip>();

    AudioSource audi;
    bool TouchedToShip = false;

    private void Start()
    {
        audi = GetComponent<AudioSource>();
        if (audi != null && FireSounds.Count > 0)
        {
            SoundManager.Instance.PlaySound(FireSounds.GetRandom(), transform.position);

        }

    }

    private void OnDestroy()
    {
        // Patlama sesi
        if (TouchedToShip)
        {
            if (audi != null && ExplodedSounds.Count > 0)
            {
                SoundManager.Instance.PlaySound(ExplodedSounds.GetRandom(), transform.position);
                Instantiate(PreLoadeds.Instance.ExplosionPrefabs[0], transform.position, Quaternion.identity);
            }
        }
        // Suya düşme sesi
        else
        {
            if (audi != null && WaterSplashSounds.Count > 0)
            {
                SoundManager.Instance.PlaySound(WaterSplashSounds.GetRandom(), transform.position);
            }
        }
    }


    void OnTriggerEnter(Collider other)
    {
        var player = other.GetComponent<CaptainPlayer>();
        if (player != null)
        {
            Debug.Log("Exploded on Player ");
            TouchedToShip = true;
            player.AddDamage(DamageAmount);
            Destroy(gameObject);
            return;
        }
        var pirate = other.GetComponent<Pirate>();
        if (pirate != null && FiredShip != null && pirate.gameObject != FiredShip)
        {
            //Debug.Log("Exploded on Pirate " + FiredShip.name + ":" +  pirate.name);
            TouchedToShip = true;
            pirate.AddDamage(DamageAmount);
            Destroy(gameObject);
            return;
        }
    }
}
