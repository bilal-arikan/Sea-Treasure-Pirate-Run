﻿using System;
using System.Collections.Generic;
using MarkLight;
using MarkLight.Views.UI;
using Common.Managers;

public class IAPView : UIView
{
    const string OnlyForCurrentgame = 
        "The Product that purchased only\n" +
        "applies for <color=#ff0000ff>This Game Playing</color>  !!!\n" +
        "Purchased items <color=#ff0000ff>Will be Lost</color> on\n" +
        "next game playing !!!\n" +
        "<color=#ffa500ff>(Removing ads Exception)</color>";

    const string AreYouSure = "Are You Sure ?";

    public Button IAP_noads;
    public Button IAP_diamond150;
    public Button IAP_heart3;
    public Button IAP_destroyhalf;
    public Button IAP_chest3;
    public Button IAP_gold1500;
    public Button IAP_highpriceseason;
    public Button IAP_speedx3;
    public Button IAP_chest3_D;
    public Button IAP_gold1500_D;
    public Button IAP_highpriceseason_D;
    public Button IAP_speedx3_D;

    private void OnEnable()
    {
        IAP_noads.IsDisabled.Value = !AdsManager.Instance.enabled;
        //IAP2
        IAP_heart3.IsDisabled.Value = (GameManager.Instance.Player.MaxHealth - GameManager.Instance.Player.Health) < 3;
        //IAP4
        //IAP5
        IAP_destroyhalf.IsDisabled.Value = GameManager.Instance.ActivePirates.Count <= 1;
        IAP_highpriceseason.IsDisabled.Value = GameManager.Instance.HighPriceNextSeason;
        IAP_speedx3.IsDisabled.Value = GameManager.Instance.Player.Agent.speed > 25;

        IAP_highpriceseason_D.IsDisabled.Value = GameManager.Instance.HighPriceNextSeason;
        IAP_speedx3_D.IsDisabled.Value = GameManager.Instance.Player.Agent.speed > 25;
    }

    public void ClickIAP1()
    {
        UIManager.Instance.ShowAskToUser("<size=45>Ads will be removed\nFOREVER !</size>", (s) => {
            if(s)
                IAPManager.Instance.BuyProductID(IAP.NonCons.NoAds);
            OnEnable();
        });
    }

    public void ClickIAP2()
    {
        UIManager.Instance.ShowAskToUser(OnlyForCurrentgame, (s) => {
            if(s)
                IAPManager.Instance.BuyProductID(IAP.Cons.Chest3);
            OnEnable();
        }, -1, 0.5f);
        //IAPManager.Instance.BuyProductID(IAP.Cons.Chest3);
    }

    public void ClickIAP3()
    {
        UIManager.Instance.ShowAskToUser(OnlyForCurrentgame, (s) =>
        {
            if (s)
                IAPManager.Instance.BuyProductID(IAP.Cons.Hearth3);
            OnEnable();
        }, -1, 0.5f);
        //IAPManager.Instance.BuyProductID(IAP.Cons.Hearth3);
    }

    public void ClickIAP4()
    {
        UIManager.Instance.ShowAskToUser(OnlyForCurrentgame, (s) =>
        {
            if (s)
                IAPManager.Instance.BuyProductID(IAP.Cons.Gold1500);
            OnEnable();
        }, -1, 0.5f);
        //IAPManager.Instance.BuyProductID(IAP.Cons.Gold1500);
    }

    public void ClickIAP5()
    {
        UIManager.Instance.ShowAskToUser(OnlyForCurrentgame, (s) =>
        {
            if (s)
                IAPManager.Instance.BuyProductID(IAP.Cons.Diamond150);
            OnEnable();
        }, -1, 0.5f);
        //IAPManager.Instance.BuyProductID(IAP.Cons.Diamond150);
    }

    public void ClickIAP6()
    {
        UIManager.Instance.ShowAskToUser(OnlyForCurrentgame, (s) =>
        {
            if (s)
                IAPManager.Instance.BuyProductID(IAP.Cons.DestroyHalf);
            OnEnable();
        }, -1, 0.5f);
        //IAPManager.Instance.BuyProductID(IAP.Cons.DestroyHalf);
    }

    public void ClickIAP7()
    {
        UIManager.Instance.ShowAskToUser(OnlyForCurrentgame, (s) =>
        {
            if (s)
                IAPManager.Instance.BuyProductID(IAP.Cons.HighestPrices);
            OnEnable();
        }, -1, 0.5f);
        //IAPManager.Instance.BuyProductID(IAP.Cons.HighestPrices);
    }

    public void ClickIAP8()
    {
        UIManager.Instance.ShowAskToUser(OnlyForCurrentgame, (s) =>
        {
            if (s)
                IAPManager.Instance.BuyProductID(IAP.Cons.SpeedX3);
            OnEnable();
        }, -1, 0.5f);
        //IAPManager.Instance.BuyProductID(IAP.Cons.SpeedX3);
    }


    public void ClickIAP9()
    {
        UIManager.Instance.ShowAskToUser(AreYouSure, (s) =>
        {
            if (s)
            {
                bool success = GameManager.Instance.TryPurchase(IAP.Cons.Chest3);
                if (!success)
                    UIManager.Instance.ShowToast("Not Enough Resources !");
            }
            OnEnable();
        });
    }

    public void ClickIAP10()
    {
        UIManager.Instance.ShowAskToUser(AreYouSure, (s) =>
        {
            if (s)
            {
                bool success = GameManager.Instance.TryPurchase(IAP.Cons.Gold1500);
                if (!success)
                    UIManager.Instance.ShowToast("Not Enough Resources !");
            }
            OnEnable();
        });
    }

    public void ClickIAP11()
    {
        UIManager.Instance.ShowAskToUser(AreYouSure, (s) =>
        {
            if (s)
            {
                bool success = GameManager.Instance.TryPurchase(IAP.Cons.HighestPrices);
                if (!success)
                    UIManager.Instance.ShowToast("Not Enough Resources !");
            }
            OnEnable();
        });
    }

    public void ClickIAP12()
    {
        UIManager.Instance.ShowAskToUser(AreYouSure, (s) =>
        {
            if (s)
            {
                bool success = GameManager.Instance.TryPurchase(IAP.Cons.SpeedX3);
                if (!success)
                    UIManager.Instance.ShowToast("Not Enough Resources !");
            }
            OnEnable();
        });
    }

    public void BackToGame()
    {
        UIManager.Instance.ChangeSubView(null);
        GameManager.Instance.UnPause();
    }
}
