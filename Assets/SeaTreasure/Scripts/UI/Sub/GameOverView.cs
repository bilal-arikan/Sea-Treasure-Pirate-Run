﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Common.Managers;
using Common.Tools;
using MarkLight;
using MarkLight.Views.UI;
using Common.Data;
using Arikan;

public class GameOverView : SingletonView<GameOverView>
{
    public _int YourScore;
    public _int OldScore;
    public ObservableList<ListItemGood> OwnedGoods = new ObservableList<ListItemGood>();
    public _int Fortune;
    public _int DestroyedPirates;

    public Button ScrGooglePlayButton;
    public Button ScrGameJoltButton;


    private void OnEnable()
    {
        // Set ScoreTable Buttons
        ScrGooglePlayButton.IsDisabled.Value = !SocialSystem.Instance.IsSignedIn<SocialCloudOnce>();
        ScrGameJoltButton.IsDisabled.Value = !SocialSystem.Instance.IsSignedIn<SocialGameJolt>();

        OwnedGoods.Clear();
        foreach (var kv in GameManager.Instance.Player.Inventory)
        {
            if (kv.Value > 0)
                OwnedGoods.Add(new ListItemGood(kv.Key, kv.Value));
        }

        YourScore.Value = PlayerManager.CurrentPlayer.Xp;
        OldScore.Value = PlayerManager.CurrentPlayer.BestScore;
        DestroyedPirates.Value = GameManager.Instance.DestroyedPirateCount;
    }
    
    
    public void ReplayCh()
    {
        LevelLoadManager.Instance.LoadLevel(LevelLoadManager.CurrentIndex);
    }

    public void ReturnToMainMenu()
    {
        LevelLoadManager.Instance.LoadLevel(new Point2());
        AdsManager.Instance.TryShowInterstitial();
    }

    public void ScrGooglePlayButtonClick()
    {
        SocialSystem.Instance.ShowLeaderboard<SocialCloudOnce>();
    }
    public void ScrGameJoltButtonClick()
    {
        SocialSystem.Instance.ShowLeaderboard<SocialGameJolt>();
    }
}
