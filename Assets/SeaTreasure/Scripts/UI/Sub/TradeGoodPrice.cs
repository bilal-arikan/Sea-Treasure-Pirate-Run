﻿using Common.Managers;
using MarkLight;
using MarkLight.Views.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TradeGoodPrice : UIView {
    public Button BuyButton;
    public Button SellButton;
    public _SpriteAsset BuyImage;
    public _SpriteAsset SellImage;

    public Good GoodToBuy;
    public Good GoodToSell;
    public _int BuyPrice;
    public _int SellPrice;

    public void SetPrice(Price p)
    {
        GoodToBuy = p.GoodToBuy;
        GoodToSell = p.GoodToSell;
        BuyPrice.Value = p.BuyPrice;
        SellPrice.Value = p.SellPrice;
        BuyImage.Value = p.GoodToBuy.Image;
        SellImage.Value = p.GoodToSell.Image;
    }

    public void OnShow()
    {
        // 0 sa butonu gizle 
        int ownedToGive = GameManager.Instance.Player.Inventory.GetValueOrDefault(GoodToSell, 0);
        BuyButton.IsDisabled.Value = ownedToGive < BuyPrice;
        BuyButton.IsActive.Value = BuyPrice != 0;

        // 0 sa butonu gizle
        int ownedGoods = GameManager.Instance.Player.Inventory.GetValueOrDefault(GoodToBuy, 0);
        SellButton.IsDisabled.Value = ownedGoods <= 0;
        SellButton.IsActive.Value = SellPrice != 0;
    }

    public void ClickBuy()
    {
        GameManager.Instance.Player.TryBuy(GoodToBuy, 1, GoodToSell, BuyPrice);
        //Debug.Log("ClickBuy");
    }

    public void ClickSell()
    {
        GameManager.Instance.Player.TrySell(GoodToBuy, 1, GoodToSell, SellPrice);
        //Debug.Log("ClickSell");
    }

    public void LongClickBuy()
    {
        var b = GameManager.Instance.Player.TryBuy(GoodToBuy, 100000, GoodToSell, BuyPrice, true);
        Debug.Log("BuyLongPress " + GoodToBuy.Name + ":" + GoodToSell.Name + ":" + b);
    }

    public void LongClickSell()
    {
        var b = GameManager.Instance.Player.TrySell(GoodToBuy, 100000, GoodToSell, SellPrice, true);
        Debug.Log("SellLongPress " + GoodToSell.Name + ":" + GoodToBuy.Name + ":" + b);
    }

}
