﻿using System;
using System.Collections.Generic;
using MarkLight;
using MarkLight.Views;
using MarkLight.Views.UI;
using Common.Managers;
using UnityEngine;
using Common.Data;
using System.Collections;

public class CreditsView : SingletonView<CreditsView>
{
    public _string Version;
    public Button ConsoleButton;

    public void BackToGame()
    {
        UIManager.Instance.ChangeSubView(null);
    }


    public void ShowConsole()
    {
        AdsManager.Instance.HideBanner();
        Common.CConsole.Show();
    }

    void Start()
    {
        Version.Value = "v " + Application.version;
    }

    private void OnEnable()
    {
        AdsManager.Instance.TryShowBanner();
    }

    private void OnDisable()
    {
        AdsManager.Instance.HideBanner();
    }

    public void ClickWebSite()
    {
        Application.OpenURL(C.website_url.StringValue);
    }

    public void ClickFacebook()
    {
        Application.OpenURL(C.facebook_page.StringValue);
    }

    public void QualityLow()
    {
        ConfigManager.Instance.SetGraphicQuality(0);
    }
    public void QualityMedium()
    {
        ConfigManager.Instance.SetGraphicQuality(1);
    }
    public void QualityHigh()
    {
        ConfigManager.Instance.SetGraphicQuality(2);
    }
}
