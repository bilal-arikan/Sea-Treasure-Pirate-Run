﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using MarkLight;
using MarkLight.Views.UI;
using Common.Managers;
using Common.Tools;
using Common.Data;
using MarkLight.Views;

public class ListItemGood
{
    public Good G;
    public int Amount = 0;
    public Sprite ImageAsset
    {
        get
        {
            return G.Image;
        }
        set
        {
            G.Image = value;
        }
    }

    public ListItemGood(Good g, int amount = 0)
    {
        this.G = g;
        Amount = amount;
    }
}

public class GameView : SingletonView<GameView>
{
    public _int BottomFortune;
    public _int CurrentFortune;
    public _int TopFortune;
    public _SpriteAsset FortuneImage;
    public _int NextSeasonSeconds;

    public ObservableList<ListItemGood> Inventory = new ObservableList<ListItemGood>();

    public Button MarketButton;
    public Button ButtonOpenChest;
    public Button ButtonWatchReward;
    public Label WatchRewardLabel;
    public _int ChestCount;
    public _int PirateCount;
    public _int RewardLeft;

    public Region SettingsRegion;

    public Region PortView;
    public Group Prices;
    public _string DockedPortName;
    public List<TradeGoodPrice> ActivePrices = new List<TradeGoodPrice>();

    public ViewAnimation ShowPricesAnimation;
    public ViewAnimation HidePricesAnimation;

    protected override void Awake()
    {
#if UNITY_WEBGL
        MarketButton.IsActive.Value = false;
        ButtonWatchReward.IsActive.Value = false;
#endif
        base.Awake();

        ActivePrices.AddRange(Prices.GetChildren<TradeGoodPrice>(false));

        EventManager.StartListening(E.InventoryChanged, () =>
        {
            Inventory.Capacity = PreLoadeds.Instance.AllGoods.Count;
            for (int i = 0; i < GameManager.Instance.Player.Inventory.Count; i++)
                Inventory[i] = new ListItemGood( 
                    GameManager.Instance.Player.Inventory.ElementAt(i).Key, 
                    GameManager.Instance.Player.Inventory.ElementAt(i).Value);

            if(UIManager.Instance.FortuneSprites.Count > PlayerManager.CurrentPlayer.Level)
                FortuneImage.Value = new SpriteAsset(string.Empty, UIManager.Instance.FortuneSprites[PlayerManager.CurrentPlayer.Level]);
            else
                FortuneImage.Value = new SpriteAsset(string.Empty, UIManager.Instance.FortuneSprites[UIManager.Instance.FortuneSprites.Count-1]);

            BottomFortune.Value = PlayerManager.Instance.LevelSteps[PlayerManager.CurrentPlayer.Level - 1];

            if (PlayerManager.Instance.LevelSteps.Length > PlayerManager.CurrentPlayer.Level)
                TopFortune.Value = PlayerManager.Instance.LevelSteps[PlayerManager.CurrentPlayer.Level];
            else
                TopFortune.Value = PlayerManager.Instance.LevelSteps[PlayerManager.Instance.LevelSteps.Length - 1];

            CurrentFortune.Value = PlayerManager.CurrentPlayer.Xp;
        });
    }

    private void OpenChest()
    {
        EventManager.TriggerEvent(E.ChestUsed);
    }

    private void OpenMarket()
    {
        UIManager.Instance.ChangeSubView(typeof(IAPView));
        GameManager.Instance.Pause();
    }

    private void WatchReward()
    {
        AdsManager.Instance.TryShowReward(
        (e) =>
        {

        }, (r) =>
        {
            EventManager.TriggerObjectEvent(E.RewardVideoGift, r);
        });
    }
    
    public void ClickTutorial()
    {
        TutorialManager.Instance.Show(LevelLoadManager.CurrentIndex);
    }
    public void ClickTouchCtrl()
    {
        GameManager.Instance.Joystick.SetActive(false);
        GameManager.Instance.UseJoystick = false;
    }
    public void ClickJoystickCtrl()
    {
        GameManager.Instance.Joystick.SetActive(true);
        GameManager.Instance.UseJoystick = true;
    }
    public void ClickSound()
    {
        SoundManager.Instance.SfxOn = !SoundManager.Instance.SfxOn;
    }
    public void ClickMusic()
    {
        SoundManager.Instance.MusicOn = !SoundManager.Instance.MusicOn;
    }

    public void ClickSettings()
    {
        SettingsRegion.IsActive.Value = !SettingsRegion.IsActive.Value;
    }



    public void BackToGame()
    {
        LevelLoadManager.Instance.LoadLevel(new Point2());
        AdsManager.Instance.TryShowInterstitial();
    }

    public bool SetIsActive(int amount) { return amount > 0; }
}

