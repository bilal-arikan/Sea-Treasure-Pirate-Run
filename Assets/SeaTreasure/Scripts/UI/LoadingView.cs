﻿using MarkLight.Views.UI;
using System.Collections.Generic;
using UnityEngine;
using Common.Managers;
using MarkLight;

public class LoadingView : SingletonView<LoadingView>
{
    public _string MainText;
    public _string Percent;

    public _SpriteAsset MainSprite;

}
