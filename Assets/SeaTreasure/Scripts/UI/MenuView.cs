﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MarkLight.Views;
using MarkLight.Views.UI;
using Common.Managers;
using UnityEngine;
using Arikan;
using Common;

public class MenuView : SingletonView<MenuView>
{
    public Button UpdateButton;
    public Button GooglePlayButton;
    public Button GameJoltButton;

    public ViewAnimation UpdateButtonAnimation;

    private void Start()
    {
#if !CloudOnce
        GooglePlayButton.IsActive.Value = false;
#endif
#if !GameJolt
        GameJoltButton.IsActive.Value = false;
#endif
    }

#if UNITY_EDITOR
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Q))
        {
            UpdateButton.IsActive.Value = ConfigManager.Instance.AvailableNewVersion;

            if (ConfigManager.Instance.AvailableNewVersion)
            {
                UpdateButtonAnimation.StartAnimation();
            }
        }
        if (Input.GetKeyDown(KeyCode.W))
        {
            UpdateButton.IsActive.Value = true;
            UpdateButtonAnimation.StartAnimation();
        }
        if (Input.GetKeyDown(KeyCode.E))
        {
            UIManager.Instance.ChangeSubView(typeof(CreditsView));
        }
    }
#endif

    public void PlayCampaign()
    {
        GameManager.Instance.ChangeGameMode(GameMode.Default);
        LevelLoadManager.Instance.LoadLevel(new Point2(1, 1));

        SocialSystem.Instance.SetAchievement("FirstPlaying");
    }
    public void PlaySurvival()
    {
        GameManager.Instance.ChangeGameMode(GameMode.Survival);
        LevelLoadManager.Instance.LoadLevel(new Point2(1, 1));

        SocialSystem.Instance.SetAchievement("FirstPlaying");
    }
    public void ClickAbout()
    {
        UIManager.Instance.ChangeSubView(typeof(CreditsView));
    }


    public void OpenGooglePlay()
    {
        if (SocialSystem.Instance.IsSignedIn<SocialCloudOnce>())
            SocialSystem.Instance.ShowAchievements<SocialCloudOnce>();
        else
            SocialSystem.Instance.SignIn<SocialCloudOnce>().Catch((e)=>
            {
                Debug.LogException(e);
                UIManager.Instance.ShowToast("Play Games Failed to connect !!!");
            });
    }
    public void OpenGameJolt()
    {
        if (SocialSystem.Instance.IsSignedIn<SocialGameJolt>())
            SocialSystem.Instance.ShowAchievements<SocialGameJolt>();
        else
            SocialSystem.Instance.SignIn<SocialGameJolt>().Catch((e)=>
            {
                Debug.LogException(e);
                UIManager.Instance.ShowToast("GameJolt Failed to connect !!!");
            });
    }
    public void ClickSound()
    {
        SoundManager.Instance.SfxOn = !SoundManager.Instance.SfxOn;
    }
    public void ClickMusic()
    {
        SoundManager.Instance.MusicOn = !SoundManager.Instance.MusicOn;
    }

    public void ClickShare()
    {
        SocialSystem.Instance.ShareText(C.invite_title.StringValue, C.playstore_url.StringValue);
        SocialSystem.Instance.SetAchievement("Share");
    }
    public void ClickLike()
    {
#if UNITY_IOS
        Core.OpenURL(C.applestore_url.StringValue);
        // SocialManager.Instance.OpenPage(WebPage.AppStore);
#elif UNITY_ANDROID
        Core.OpenURL(C.market_url.StringValue);
        // SocialManager.Instance.OpenPage(WebPage.GoogleMarket);
#elif UNITY_WEBGL
        Core.OpenURL(C.googleplus_page.StringValue);
        // SocialManager.Instance.OpenPage(WebPage.GooglePlus);
#endif
    }
}
