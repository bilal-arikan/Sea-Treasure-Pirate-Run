﻿using Common.Data;
using Common.Managers;
using Common.Tools;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class PirateSpawner : Singleton<PirateSpawner>
{
    public int SpawnPirateForEveryLevel = 4;
    public float SpawnPirateForEverySeconds = 5;
    [Space(15)]
    public int SpawnBossEveryLevel = 10;
    public int SpawnBossMinimumPirateCount = 20;

    public Transform PiratesParent;
    [ReadOnly]
    public Transform[] SpawnPoints;

    public Vector3 LastSpawnPoint ;

    public int TotalSpawnedPirateCount = 0;

    protected override void Awake()
    {
        base.Awake();
        SpawnPoints = GameObject.FindGameObjectsWithTag("Respawn").ConvertAll(o => o.transform);

        if (C.pirate_spawn_amount.IsAvailable)
            SpawnPirateForEveryLevel = C.pirate_spawn_amount.IntValue;
    }

    private void Start()
    {
        EventManager.StartListeningObjectEvent(E.PlayerLevelUp, SpawnByLevel);

        StartCoroutine(SpawnInTime());
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="level"></param>
    void SpawnByLevel(object level)
    {
        int l = (int)level;

        if (GameManager.Instance.ActivePirates.Count < (l + 1) * SpawnPirateForEveryLevel)
        {
            Vector3 playerPose = GameManager.Instance.Player.transform.position;
            // Playera En uzak noktayı bul
            var points = SpawnPoints.OrderByDescending(p => (p.position - playerPose).magnitude);


            if (GameManager.Instance.ActivePirates.Count >= SpawnBossMinimumPirateCount
                && SpawnBossEveryLevel != 0 &&
                l % SpawnBossEveryLevel == 0)
            {
                // Spawn a BOSS
                SpawnBossPirate(points.ElementAt(0).position);
            }
            else
            {
                //PirateCountForEveryLevel kadar Spawn Et
                for (int i = 0; i < SpawnPirateForEveryLevel; i++)
                    SpawnPirate(points.ElementAt(i).position);
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    IEnumerator SpawnInTime()
    {
        while (true)
        {
            if (SpawnPirateForEverySeconds <= 0)
            {
                yield return new WaitForSeconds(5);
                continue;
            }

            yield return new WaitForSeconds(SpawnPirateForEverySeconds);

            if (GameManager.Instance.Player == null)
                yield break;

            Vector3 playerPose = GameManager.Instance.Player.transform.position;
            // Playera En uzak noktayı bul
            var points = SpawnPoints.OrderByDescending(p => (p.position - playerPose).magnitude);

            if (TotalSpawnedPirateCount >= SpawnBossMinimumPirateCount &&
                TotalSpawnedPirateCount % SpawnBossEveryLevel * 3 == 0)
            {
                // Spawn a BOSS
                SpawnBossPirate(points.ElementAt(0).position);
            }
            else
            {
                SpawnPirate(points.ElementAt(0).position);
            }
        }
    }


    public Pirate SpawnPirate(Vector3 worlPpose,int index = -1)
    {
        if (PreLoadeds.Instance.PiratePrefabs.Count > 0 && index >= 0 && index < PreLoadeds.Instance.PiratePrefabs.Count)
        {

        }
        else
        {
            index = UnityEngine.Random.Range(0, PreLoadeds.Instance.PiratePrefabs.Count);
        }

        var pirate = Instantiate(PreLoadeds.Instance.PiratePrefabs[index], worlPpose, UnityEngine.Random.rotationUniform, PiratesParent);

        TotalSpawnedPirateCount++;

        return pirate;
    }

    public Pirate SpawnBossPirate(Vector3 worlPpose, int index = -1)
    {
        if (PreLoadeds.Instance.PirateBossPrefabs.Count > 0 && index >= 0 && index < PreLoadeds.Instance.PirateBossPrefabs.Count)
        {

        }
        else
        {
            index = UnityEngine.Random.Range(0, PreLoadeds.Instance.PirateBossPrefabs.Count);
        }

        var pirate = Instantiate(PreLoadeds.Instance.PirateBossPrefabs[index], worlPpose, UnityEngine.Random.rotationUniform, PiratesParent);

        TotalSpawnedPirateCount++;

        return pirate;
    }
}
