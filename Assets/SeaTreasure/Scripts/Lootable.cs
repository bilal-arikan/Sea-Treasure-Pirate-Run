﻿using Common.Managers;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum LootType
{
    Gold,
    Diamond,
    Good1,
    Good2,
    Good3,
    Good4,
    Good5,
}

public class Lootable : MonoBehaviour {

    public Good Good;
    public int Amount = 1;
    public AudioClip LootAudio;
    public Transform SpawnedTransform;

    void Awake()
    {
        GameManager.Instance.ActiveLootables.Add(this);
    }

    private void OnDestroy()
    {
        GameManager.Instance.ActiveLootables.Remove(this);
    }

    private void OnTriggerEnter(Collider other)
    {
        var player = other.GetComponent<CaptainPlayer>();
        if (player != null)
        {
            if (LootAudio != null)
                SoundManager.Instance.PlaySound(LootAudio, transform.position);

            player.AddToInventory(Good, Amount);

            
            Destroy(gameObject);
        }
    }

    public static LootType RandomType()
    {
        var values = System.Enum.GetValues(typeof(LootType));
        return (LootType)values.GetValue(UnityEngine.Random.Range(0,values.Length));
    }
}
