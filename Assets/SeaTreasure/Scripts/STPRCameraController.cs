﻿using Common;
using Common.Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class STPRCameraController : MonoBehaviour
{
    [ReadOnly]
    public float DistanceToShip = 50.0f;
    public float RotateSpeed = 1;
    public float LerpAmount = 0.5f;

    public float TargetAngle;
    public float CurrentAngle
    {
        get
        {
            return transform.rotation.eulerAngles.y;
        }
        set
        {
            transform.rotation = Quaternion.Euler(transform.rotation.eulerAngles.SetY(value));
        }
    }
    public Transform FollowingObject;
    public Quaternion DefaultRotation;

    private void Start()
    {
        FollowingObject = GameManager.Instance.Player.transform;
        DistanceToShip = (new Vector2(transform.position.x, transform.position.z) - new Vector2(FollowingObject.position.x, FollowingObject.position.z)).magnitude;
        TargetAngle = transform.rotation.eulerAngles.y;
        DefaultRotation = transform.rotation;

        SimpleGesture.While1FingerPanning(OnPanning);
    }

    void OnPanning(GestureInfoPan i)
    {
        if(!InputManager.IsClickedUI && i.touchPhase == TouchPhase.Moved)
        {
            TargetAngle = (CurrentAngle + i.deltaDirection.x * RotateSpeed) % 360;
            Debug.Log("OnPanning:" + TargetAngle);
        }
    }

    private void Update()
    {
        if(FollowingObject != null && TargetAngle != CurrentAngle)
        {
            CurrentAngle = Mathf.LerpAngle(CurrentAngle, TargetAngle, LerpAmount*Time.unscaledDeltaTime);

            Quaternion rotation = Quaternion.Euler(DefaultRotation.eulerAngles.SetY(CurrentAngle));

            Vector3 negDistance = new Vector3(0.0f, 0.0f, -DistanceToShip);
            Vector3 localPosition = rotation * negDistance + FollowingObject.position;

            transform.rotation = rotation;
            transform.localPosition = localPosition;
        }
    }

    private void OnDestroy()
    {
        if (!Core.IsApplicationQuitting)
            SimpleGesture.Stop1FingerPanning(OnPanning);
    }

}
