﻿using Common.Managers;
using MarkLight;
using MarkLight.Views.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

[Serializable]
public class Price
{
    public Good GoodToBuy;
    public Good GoodToSell;
    public int BuyPrice;
    public int SellPrice;

    public Price(Good b, Good s, Point2 price)
    {
        GoodToBuy = b;
        GoodToSell = s;
        BuyPrice = price.x;
        SellPrice = price.y;
    }
}
