﻿//***********************************************************************//
// Copyright (C) 2017 Bilal Arıkan. All Rights Reserved.
// Author: Bilal Arıkan
// Time  : 04.11.2017   
//***********************************************************************//
using System;
using System.Collections.Generic;
using UnityEngine;
using Common.Tools;
using System.Collections;

namespace Common.Managers
{
    public partial class PlayerManager: Singleton<PlayerManager>
    {

        IEnumerator Start()
        {
            LoadPlayerData();
            LoadPlayerProfile();
            EventManager.StartListening(E.PlayerLost, () =>
            {
                SavePlayerData();
            });

            yield return new WaitForEndOfFrame();
            CheckDailyGifts();
        }
    }
}
