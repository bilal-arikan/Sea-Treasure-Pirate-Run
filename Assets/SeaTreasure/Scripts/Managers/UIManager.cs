﻿//***********************************************************************//
// Copyright (C) 2017 Bilal Arıkan. All Rights Reserved.
// Author: Bilal Arıkan
// Time  : 04.11.2017   
//***********************************************************************//
using System;
using System.Collections.Generic;
using UnityEngine;
using MarkLight;
using Common.Tools;
using Common.Managers;
using MarkLight.Views.UI;
using System.Collections;

namespace Common.Managers
{
    public partial class UIManager : Singleton<UIManager>
    {
        public Timer EnableRewardTimer;

        public float RewardOnStartDelay = 60;
        public float WaitForNextReward = 120;

        public List<Sprite> FortuneSprites = new List<Sprite>();

        void Start()
        {
            var o = new GameObject("RewardTimer", typeof(Timer));
            o.transform.parent = transform;
            EnableRewardTimer = o.GetComponent<Timer>();
            EnableRewardTimer.UseRealTime = true;
            EnableRewardTimer.TargetTime = RewardOnStartDelay;
            EnableRewardTimer.TimerFinished = EnableWatchReward;
            EnableRewardTimer.TimeChanged = (t) => {
                GameView.Instance.RewardLeft.Value = (int)(WaitForNextReward - t);
            };

            EventManager.StartListeningObjectEvent(E.RewardVideoGift, (r) =>
            {
                EnableRewardTimer.TargetTime = WaitForNextReward;
                GameView.Instance.ButtonWatchReward.IsDisabled.Value = true;
                GameView.Instance.WatchRewardLabel.IsVisible.Value = true;
                EnableRewardTimer.StartTimer();
            });
            EventManager.StartListening(E.ConfigsFetched, () =>
            {
                //Configs Fetched
                if (C.reward_show_seconds.IsAvailable)
                    WaitForNextReward = C.reward_show_seconds.FloatValue;
            });
            EventManager.StartListeningObjectEvent(E.AvailableNewVersion, (v) =>
            {
                MenuView.Instance.UpdateButton.IsActive.Value = ConfigManager.Instance.AvailableNewVersion;

                if (ConfigManager.Instance.AvailableNewVersion)
                {
                    MenuView.Instance.UpdateButtonAnimation.StartAnimation();
                }
            });

            // Başlangıç
            GameView.Instance.ButtonWatchReward.IsDisabled.Value = (RewardOnStartDelay > 0);
            GameView.Instance.WatchRewardLabel.IsVisible.Value = (RewardOnStartDelay > 0);
            if (RewardOnStartDelay > 0)
                EnableRewardTimer.StartTimer();
        }


        void EnableWatchReward(float time)
        {
            Debug.Log("EnableWatchReward()");
            GameView.Instance.ButtonWatchReward.IsDisabled.Value = false;
            GameView.Instance.WatchRewardLabel.IsVisible.Value = false;
            EnableRewardTimer.ResetTimer();
        }
    }
}