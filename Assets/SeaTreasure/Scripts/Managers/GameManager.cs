﻿//***********************************************************************//
// Bu kodu başka yerden aldım :D 
//***********************************************************************//
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Common.Tools;
using Common.Data;
using System;
using UnityStandardAssets.CrossPlatformInput;
using MarkLight;
using MarkLight.Views;
using MarkLight.Views.UI;
using Arikan;

namespace Common.Managers
{

    /// <summary>
    /// The game manager is a persistent singleton that handles points and time
    /// </summary>
    public partial class GameManager : Singleton<GameManager>
    {
        public CaptainPlayer Player;
        public LayerMask ClickLayerMask;

        public bool UseJoystick = false;
        public GameObject Joystick;

        public float NewDestinationDistance = 3;
        Vector3 NewTargetPosition;
        bool setNewTarget = true;

        private float NextNewSeasonTime;
        public bool HighPriceNextSeason = false;
        public int PassedSeasonCount = 0;
        public int PortGoodCount = 3;

        [ReadOnly]
        public int DestroyedPirateCount = 0;
        [ReadOnly]
        public int AchRevenge = 0;

        [ReadOnly]
        public List<Pirate> ActivePirates = new List<Pirate>();
        [ReadOnly]
        public List<Lootable> ActiveLootables = new List<Lootable>();

        public List<PortController> AllPorts = new List<PortController>();

        [Header("Game Settings")]
        public float NewSeasonTimeStep = 20;
        public int RewardGoldAmount = 2000;
        [Space(15)]
        public int SpawnPirateForEveryLevel = 4;
        public float SpawnPirateForEverySeconds = 10;
        public int SpawnBossEveryLevel = 5;
        public int SpawnBossMinimumPirateCount = 20;

        private void Start()
        {
            EventManager.StartListening(E.ConfigsFetched, () =>
            {
                if(C.new_season_seconds.IsAvailable)
                    NewSeasonTimeStep = C.new_season_seconds.FloatValue;
                if(C.ports_good_count.IsAvailable)
                    PortGoodCount = C.ports_good_count.IntValue;

                if(C.visible_console_button.IsAvailable)
                    CreditsView.Instance.ConsoleButton.IsActive.Value = C.visible_console_button.BooleanValue;

                if (C.pirate_spawn_amount.IsAvailable)
                    SpawnPirateForEveryLevel = C.pirate_spawn_amount.IntValue;
            });
            EventManager.StartListening(E.NewSeason, () =>
            {
                if (HighPriceNextSeason)
                    UIManager.Instance.ShowToast("High Price Season NOW");
                if(PassedSeasonCount >= 10)
                    SocialSystem.Instance.SetAchievement("PassedSeasons");
            });
            EventManager.StartListening(E.PlayerLost, () =>
            {
                UIManager.Instance.ChangeSubView(typeof(GameOverView));
                if (PlayerManager.CurrentPlayer.BestScore < PlayerManager.CurrentPlayer.Xp)
                    PlayerManager.CurrentPlayer.BestScore = PlayerManager.CurrentPlayer.Xp;
                if (PlayerManager.CurrentPlayer.DestroyedPirateAmount < DestroyedPirateCount)
                    PlayerManager.CurrentPlayer.DestroyedPirateAmount = DestroyedPirateCount;

                SocialSystem.Instance.SetScore("Richest", PlayerManager.CurrentPlayer.Xp);
                SocialSystem.Instance.SetScore("PirateHunter", DestroyedPirateCount);
                if(CurrentMode == GameMode.Survival)
                    SocialSystem.Instance.SetScore("Survival", PlayerManager.CurrentPlayer.Xp);
            });
            EventManager.StartListening(E.ChestLooted, () =>
            {
                Player.OwnedChestCount++;
                GameView.Instance.ChestCount.Value++;
                GameView.Instance.ButtonOpenChest.IsActive.Value = Player.OwnedChestCount > 0;

                if (Player.OwnedChestCount == 20)
                    SocialSystem.Instance.SetAchievement("TreasureHunter");
            });
            EventManager.StartListening(E.ChestUsed, () =>
            {
                // Chest Open Sound
                SoundManager.Instance.PlaySound(PreLoadeds.Instance.ChestPrefab.OpenAudio,Player.transform.position);

                Player.OwnedChestCount--;
                GameView.Instance.ChestCount.Value--;
                GameView.Instance.ButtonOpenChest.IsActive.Value = Player.OwnedChestCount > 0;

                int randomAmount = UnityEngine.Random.Range(10, 50);
                LootType randomType = Lootable.RandomType();

                var good = PreLoadeds.Instance.AllGoods.Find(g => g.Type == randomType);

                if (randomType == LootType.Gold)
                    randomAmount = randomAmount * 10;
                else if(randomType == LootType.Diamond)
                    randomAmount = randomAmount / 5;

                Player.AddToInventory(good, randomAmount);

                UIManager.Instance.ShowToast(good.Name + " +" + randomAmount, good.Image);
            });
            EventManager.StartListeningObjectEvent(E.RewardVideoGift, (r) =>
            {
                if (C.reward_gold_amount.IsAvailable)
                    RewardGoldAmount = C.reward_gold_amount.IntValue;
                Player.AddToInventory(PreLoadeds.Instance.Gold, RewardGoldAmount);
                UIManager.Instance.ShowToast("+" + RewardGoldAmount, PreLoadeds.Instance.Gold.Image);
            });
            EventManager.StartListeningObjectEvent(E.DailyGift, (d) =>
            {
                int day = (int)d;
                if (day >= 3)
                    SocialSystem.Instance.SetAchievement("Play3Days");
                if (day >= 7)
                    SocialSystem.Instance.SetAchievement("Play7Days");

                if(day != 1)
                    UIManager.Instance.ShowToast(day + ". Day");
            });
            EventManager.StartListeningObjectEvent(E.ItemPurchased, (i) =>
            {
                string id = i as string;
                if(id == IAP.NonCons.NoAds)
                {
                    AdsManager.Instance.DisableAds();
                    UIManager.Instance.ShowToast("All Ads Removed");
                }
                else if(id == IAP.Cons.Chest3)
                {
                    EventManager.TriggerEvent(E.ChestLooted);
                    EventManager.TriggerEvent(E.ChestLooted);
                    EventManager.TriggerEvent(E.ChestLooted);
                    EventManager.TriggerEvent(E.ChestLooted);
                    EventManager.TriggerEvent(E.ChestLooted);
                    UIManager.Instance.ShowToast("Added 5 Chest",PreLoadeds.Instance.ChestPrefab.ChestIcon);
                }
                else if (id == IAP.Cons.Hearth3)
                {
                    Player.AddDamage(-3);
                    UIManager.Instance.ShowToast("+3 Repair");
                }
                else if (id == IAP.Cons.Gold1500)
                {
                    Player.AddToInventory(PreLoadeds.Instance.Gold, 5000);
                    UIManager.Instance.ShowToast(5000 +"Gold Purchased", PreLoadeds.Instance.Gold.Image);
                }
                else if (id == IAP.Cons.Diamond150)
                {
                    Player.AddToInventory(PreLoadeds.Instance.Diamond, 500);
                    UIManager.Instance.ShowToast(500 + " Diamond Purchased", PreLoadeds.Instance.Diamond.Image);
                }
                else if (id == IAP.Cons.DestroyHalf)
                {
                    int count = ActivePirates.Count / 2;
                    var toDestroy = ActivePirates.GetRandom(count);
                    foreach (var p in toDestroy)
                        Destroy(p.gameObject);

                    UIManager.Instance.ShowToast(count + " Pirates Destroyed");
                }
                else if (id == IAP.Cons.HighestPrices)
                {
                    HighPriceNextSeason = true;
                    UIManager.Instance.ShowToast("Next Season\nPrices will be High");
                }
                else if (id == IAP.Cons.SpeedX3)
                {
                    Player.Agent.speed = Player.Agent.speed * 3;
                    Player.Agent.angularSpeed = Player.Agent.angularSpeed * 3;
                    Player.Agent.acceleration = Player.Agent.acceleration * 3;
                    UIManager.Instance.ShowToast("3x Speed Purchased");
                }
            });
            EventManager.StartListeningObjectEvent(E.LevelLoadingCompleted, OnChapterLoaded);

        }

        internal void OnChapterLoaded(object scene)
        {
            Point2 sc = (Point2)scene;

            Debug.Log("OnChapterLoaded " + sc);

            if (sc.x == 0)
            {
                SetStatus(GameStatus.MainMenu);
                return;
            }

            NextNewSeasonTime = Time.timeSinceLevelLoad + NewSeasonTimeStep;

            NewTargetPosition = Vector3.zero;

            DestroyedPirateCount = 0;
            AchRevenge = 0;
            PassedSeasonCount = 0;

            Joystick = GameObject.FindGameObjectWithTag("GameController");
            Joystick.SetActive(UseJoystick);

            if(CurrentMode == GameMode.Default)
            {
                PirateSpawner.Instance.SpawnPirateForEverySeconds = 0;
                PirateSpawner.Instance.SpawnPirateForEveryLevel = SpawnPirateForEveryLevel;
                PirateSpawner.Instance.SpawnBossEveryLevel = SpawnBossEveryLevel;
                PirateSpawner.Instance.SpawnBossMinimumPirateCount = SpawnBossMinimumPirateCount;
            }
            else if(CurrentMode == GameMode.Survival)
            {
                PirateSpawner.Instance.SpawnPirateForEverySeconds = SpawnPirateForEverySeconds;
                PirateSpawner.Instance.SpawnPirateForEveryLevel = 0;
                PirateSpawner.Instance.SpawnBossEveryLevel = SpawnBossEveryLevel;
                PirateSpawner.Instance.SpawnBossMinimumPirateCount = SpawnBossMinimumPirateCount;
            }

            GameView.Instance.PortView.Offset.Value = new ElementMargin(300, 0, 0, 0);
            EventManager.TriggerEvent(E.GameStarted);
            GameView.Instance.PirateCount.Value = GameManager.Instance.ActivePirates.Count;
            EventManager.TriggerEvent(E.InventoryChanged);

            StartCoroutine(NewSeasonCo());
            StartCoroutine(ShowSeasonTimeCo());
            StartCoroutine(SetNewTargetLocationCo());
        }

        // Update is called once per frame
        void Update()
        {
            if (!UseJoystick && !InputManager.IsClickedUI && Player != null && Input.GetKey(KeyCode.Mouse0))
            {
                var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                RaycastHit hit;
                if (Physics.Raycast(ray, out hit, 1000, ClickLayerMask.value))
                {
                    NewTargetPosition = hit.point;
                }
            }
            if (UseJoystick && Player != null)
            {
                float h = CrossPlatformInputManager.GetAxis("Horizontal");
                float v = CrossPlatformInputManager.GetAxis("Vertical");

                Vector3 temp = new Vector3(h, 0, v) * NewDestinationDistance * 3;

                temp = Quaternion.Euler(0, Camera.main.transform.rotation.eulerAngles.y, 0) * temp;

                NewTargetPosition = temp + Player.transform.position;
            }
        }

        IEnumerator NewSeasonCo()
        {
            Point2 StartedCh = LevelLoadManager.CurrentIndex;

            yield return new WaitForSeconds(1);

            while (LevelLoadManager.CurrentIndex == StartedCh)
            {
                while (Player.DockedPort != null)
                    yield return null;

                ResetPortsGoods();
                PassedSeasonCount++;
                Debug.Log("NewSeason");

                NextNewSeasonTime = Time.timeSinceLevelLoad + NewSeasonTimeStep;
                EventManager.TriggerEvent(E.NewSeason);
                HighPriceNextSeason = false;

                yield return new WaitForSeconds(NewSeasonTimeStep);
            }
        }

        /// <summary>
        /// Sadece kalan süreyi gösterir
        /// </summary>
        /// <returns></returns>
        IEnumerator ShowSeasonTimeCo()
        {
            Point2 StartedCh = LevelLoadManager.CurrentIndex;
            while (LevelLoadManager.CurrentIndex == StartedCh)
            {
                GameView.Instance.NextSeasonSeconds.Value = Mathf.Clamp((int)(NextNewSeasonTime - Time.timeSinceLevelLoad),0,1000);

                yield return new WaitForSeconds(0.9f);
            }
        }

        IEnumerator SetNewTargetLocationCo()
        {
            Point2 StartedCh = LevelLoadManager.CurrentIndex;
            while (LevelLoadManager.CurrentIndex == StartedCh)
            {
                if(Player != null && Vector3.Distance(NewTargetPosition, Player.Agent.destination) > NewDestinationDistance)
                {
                    Player.Agent.SetDestination(NewTargetPosition);
                }
                //Debug.Log("NTL" + NewTargetPosition + " " + Vector3.Distance(NewTargetPosition, Player.Agent.destination));

                yield return new WaitForSeconds(0.1f);
            }
        }

        public void ShowTradePanel(PortController port)
        {
            GameView.Instance.DockedPortName.Value = port.PortNameText.text;

            for(int i = 0; i < GameView.Instance.ActivePrices.Count; i++)
            {
                var tgp = GameView.Instance.ActivePrices[i];
                if (i < port.Prices.Count)
                {
                    tgp.gameObject.SetActive(true);
                    tgp.SetPrice(port.Prices[i]);
                    tgp.OnShow();
                }
                else
                {
                    tgp.gameObject.SetActive(false);
                }

            }
            GameView.Instance.ShowPricesAnimation.StartAnimation();
        }

        public void HideTradePanel()
        {
            GameView.Instance.ShowPricesAnimation.ReverseAnimation();
        }


        public void ResetPortsGoods()
        {
            if (AllPorts.Count == 0)
                return;

            foreach (var p in AllPorts)
            {
                p.Prices.Clear();

                // farklı ürünler seçiyoruz
                /*PriceTable[] tables = PreLoadeds.Instance.PriceTables.GetRandomElements(PortGoodCount);
                // Ürünlerin sırasını karrıştırıyoruz
                tables.Shuffle();*/

                if (HighPriceNextSeason)
                {
                    for (int i = 0; i < PortGoodCount; i++)
                    {
                        // Hepsi High
                        /*if (tables[i].HighPricesGold.Count > 0)
                        {
                            p.Prices.Add(new Price(tables[i].GoodToBuy, tables[i].MoneyToGive, tables[i].HighPricesGold.GetRandom()));
                        }*/
                        var price = PreLoadeds.Instance.HighPrices.GetRandom();
                        // Aynı ürün den varsa tekrar dene
                        if (p.Prices.Exists(pSame => pSame.GoodToBuy == price.GoodToBuy && pSame.GoodToSell == price.GoodToSell))
                        {
                            i--;
                            continue;
                        }
                        p.Prices.Add(price);
                    }
                }
                else
                {
                    for (int i = 0; i < PortGoodCount; i++)
                    {
                        /*if (i % 3 == 0)
                            p.Prices.Add(new Price(tables[i].GoodToBuy, tables[i].MoneyToGive, tables[i].LowPricesGold.GetRandom()));
                        else if(i % 3 == 1)
                            p.Prices.Add(new Price(tables[i].GoodToBuy, tables[i].MoneyToGive, tables[i].HighPricesGold.GetRandom()));
                        else
                            p.Prices.Add(new Price(tables[i].GoodToBuy, tables[i].MoneyToGive, tables[i].MiddlePricesGold.GetRandom()));*/
                        Price price = null;
                        if (i % 3 == 0)
                            price = (PreLoadeds.Instance.LowPrices.GetRandom());
                        else if (i % 3 == 1)
                            price = (PreLoadeds.Instance.HighPrices.GetRandom());
                        else
                            price = (PreLoadeds.Instance.MediumPrices.GetRandom());

                        // Aynı ürün den varsa tekrar dene
                        if(p.Prices.Exists(pSame => pSame.GoodToBuy == price.GoodToBuy && pSame.GoodToSell == price.GoodToSell))
                        {
                            i--;
                            continue;
                        }
                        p.Prices.Add(price);
                    }
                }
            }
        }


        public bool TryPurchase(string id)
        {
            int diamondCount = Player.Inventory.GetValueOrDefault(PreLoadeds.Instance.Diamond);
            int goldCount = Player.Inventory.GetValueOrDefault(PreLoadeds.Instance.Gold);
#if !UNITY_WEBGL
            if (id == IAP.Cons.Chest3)
            {
                if (diamondCount < 50)
                    return false;

                Player.Inventory[PreLoadeds.Instance.Diamond] -= 50;

                EventManager.TriggerEvent(E.ChestLooted);
                EventManager.TriggerEvent(E.ChestLooted);
                EventManager.TriggerEvent(E.ChestLooted);
                EventManager.TriggerEvent(E.ChestLooted);
                EventManager.TriggerEvent(E.ChestLooted);
                UIManager.Instance.ShowToast("Added 5 Chest", PreLoadeds.Instance.ChestPrefab.ChestIcon);
            }
            else if (id == IAP.Cons.Gold1500)
            {
                if (diamondCount < 75)
                    return false;

                Player.Inventory[PreLoadeds.Instance.Diamond] -= 75;

                Player.AddToInventory(PreLoadeds.Instance.Gold, 5000);
                UIManager.Instance.ShowToast(5000 + "Gold\nPurchased", PreLoadeds.Instance.Gold.Image);
            }
            else if (id == IAP.Cons.HighestPrices)
            {
                if (diamondCount < 250)
                    return false;

                Player.Inventory[PreLoadeds.Instance.Diamond] -= 250;

                HighPriceNextSeason = true;
                UIManager.Instance.ShowToast("Next Season\nPrices will be High");
            }
            else if (id == IAP.Cons.SpeedX3)
            {
                if (diamondCount < 1000)
                    return false;

                Player.Inventory[PreLoadeds.Instance.Diamond] -= 1000;

                Player.Agent.speed = Player.Agent.speed * 3;
                Player.Agent.angularSpeed = Player.Agent.angularSpeed * 3;
                Player.Agent.acceleration = Player.Agent.acceleration * 3;
                UIManager.Instance.ShowToast("3x Speed Purchased");
            }
            EventManager.TriggerEvent(E.InventoryChanged);
            return true;
#else
            return false;
            /*var p = IAPManager.Instance.WebGLProducts.Find(pr => pr.Id == id);
            if (p == null)
                return false;
            
            if (p.MoneyPrice > 0 && goldCount < p.MoneyPrice)
                return false;
            if (p.SpecMoneyPrice > 0 && diamondCount < p.SpecMoneyPrice)
                return false;

            Player.Inventory[PreLoadedAssets.Instance.Gold] -= p.MoneyPrice;
            Player.Inventory[PreLoadedAssets.Instance.Diamond] -= p.SpecMoneyPrice;

            if (p.Id == "chest3")
            {
                EventManager.TriggerEvent(E.ChestLooted);
                EventManager.TriggerEvent(E.ChestLooted);
                EventManager.TriggerEvent(E.ChestLooted);
            }
            else if (p.Id == "chest10")
            {
                EventManager.TriggerEvent(E.ChestLooted);
                EventManager.TriggerEvent(E.ChestLooted);
                EventManager.TriggerEvent(E.ChestLooted);
                EventManager.TriggerEvent(E.ChestLooted);
                EventManager.TriggerEvent(E.ChestLooted);
                EventManager.TriggerEvent(E.ChestLooted);
                EventManager.TriggerEvent(E.ChestLooted);
                EventManager.TriggerEvent(E.ChestLooted);
                EventManager.TriggerEvent(E.ChestLooted);
                EventManager.TriggerEvent(E.ChestLooted);
            }
            else if (p.Id == "health1")
            {
                Player.AddDamage(-1);
            }
            else if (p.Id == "highpriceseason")
            {
                HighPriceNextSeason = true;
            }
            else if (p.Id == "speed2x")
            {
                Player.Agent.speed = Player.Agent.speed * 2;
                Player.Agent.angularSpeed = Player.Agent.angularSpeed * 2;
                Player.Agent.acceleration = Player.Agent.acceleration * 2;
            }
            else if (p.Id == "speed3x")
            {
                Player.Agent.speed = Player.Agent.speed * 3;
                Player.Agent.angularSpeed = Player.Agent.angularSpeed * 3;
                Player.Agent.acceleration = Player.Agent.acceleration * 3;
            }
            else if (p.Id == "destroyhalf")
            {
                int count = ActivePirates.Count / 2;
                var toDestroy = ActivePirates.GetRandomElements(count);
                foreach (var pirate in toDestroy)
                    Destroy(pirate.gameObject);
            }
            else if (p.Id == "diamond10")
            {
                Player.AddToInventory(PreLoadedAssets.Instance.Diamond, 10);
            }
            UIManager.Instance.ShowToast(p.SuccesfullyPurchasedText, p.Image);
            return true;
            */
#endif


        }

    }

}