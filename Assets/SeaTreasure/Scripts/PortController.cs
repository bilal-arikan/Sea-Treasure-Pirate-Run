﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Common.Managers;
using Common.Data;

public class PortController : MonoBehaviour {

    public Text PortNameText;

    public List<Price> Prices = new List<Price>();

    AudioSource audi;

	// Use this for initialization
	void Awake ()
    {
        PortNameText.text = name;
        audi = GetComponent<AudioSource>();
        GameManager.Instance.AllPorts.Add(this);
    }

    void OnDestroy()
    {
        GameManager.Instance.AllPorts.Remove(this);
    }
    

    private void OnTriggerEnter(Collider other)
    {
        var player = other.GetComponent<CaptainPlayer>();
        if(player != null)
        {
            if (audi != null)
                audi.Play();
            player.DockedPort = this;
            GameManager.Instance.ShowTradePanel(this);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        var player = other.GetComponent<CaptainPlayer>();
        if (player != null)
        {
            player.DockedPort = null;
            GameManager.Instance.HideTradePanel();
        }
    }
}
