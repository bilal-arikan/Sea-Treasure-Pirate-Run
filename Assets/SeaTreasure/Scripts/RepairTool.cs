﻿using Common.Managers;
using System.Collections.Generic;
using UnityEngine;

public class RepairTool : MonoBehaviour
{
    public AudioClip RepairSound;

    private void OnTriggerEnter(Collider other)
    {
        var player = other.GetComponent<CaptainPlayer>();
        if (player != null)
        {
            player.AddDamage(-1);

            SoundManager.Instance.PlaySound(RepairSound, transform.position);

            Destroy(gameObject);
        }
    }
}
