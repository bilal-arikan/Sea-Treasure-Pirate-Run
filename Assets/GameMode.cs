﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public enum GameMode
{
    /// <summary>
    /// Campaign
    /// </summary>
    Default = 0,

    /// <summary>
    /// Survival
    /// </summary>
    Survival = 1
}
