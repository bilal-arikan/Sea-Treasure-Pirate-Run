﻿public static partial class IAP
{
    public static partial class Cons
    {
        public const string Chest3 = "chest3";
        public const string Hearth3 = "heart3";
        public const string Gold1500 = "gold1500";
        public const string Diamond150 = "diamond150";
        public const string DestroyHalf = "destroyhalf";
        public const string HighestPrices = "highpriceseason";
        public const string SpeedX3 = "speedx3";
    }

    public static partial class NonCons
    {
        public const string NoAds = "noads";
    }

    [System.Serializable]
    public partial class Product
    {
        public string Id;
        public string Name;
        public UnityEngine.Sprite Image;
        public int MoneyPrice;
        public int SpecMoneyPrice;
        public string IAPid = "";

        public string SuccesfullyPurchasedText;
    }
}
