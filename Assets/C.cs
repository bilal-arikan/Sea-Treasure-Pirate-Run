using Common.Tools;
using Common.Managers;
using System.Collections.Generic;
using Common;
using Common.Data;

namespace Common.Managers
{
    public partial class ConfigManager : Singleton<ConfigManager>
    {

        public StringStringDict FetchedConfigs = new StringStringDict();
        public StringStringDict DefaultConfigs = new StringStringDict()
        {
            {"def_language","en"},
            {"special_day"," "},
            {"invite_title","Aweasome Game"},
            {"invite_message","You Really Have to Try This Game !!!"},
            {"invite_image","https://firebasestorage.googleapis.com/v0/b/sea-treasure-pirate-run.appspot.com/o/Icon.png?alt=media&token=cf8cabae-c8a4-4d65-a7d2-985a6d5b50d1"},
            {"invite_deep_link",""},
            {"website_url","https://www.flopar.com"},
            {"playstore_url","http://play.google.com/store/apps/details?id=com.bilalarikan.stpr"},
            {"applestore_url","itms://itunes.apple.com/us/app/apple-store/{0}?mt=8"},
            {"market_url","market://details?id=com.bilalarikan.stpr"},
            {"facebook_page","https://www.facebook.com"},
            {"twitter_page","https://www.twitter.com"},
            {"googleplus_page","https://www.google.com"},
            {"instagram_page","https://www.instagram.com"},
            {"gamejolt_page","https://gamejolt.com/"},
            {"news_json","[]"},
            {"releases_json","[]"},
            { "googlestore_version","1"},
        };
    }

    public static class C
    {
        public static ConfigManager.CConfigValue googlestore_version { get { return ConfigManager.Instance.GetConfigValue("googlestore_version"); } }
        public static ConfigManager.CConfigValue def_language { get { return ConfigManager.Instance.GetConfigValue("def_language"); } }
        public static ConfigManager.CConfigValue special_day { get { return ConfigManager.Instance.GetConfigValue("special_day"); } }
        public static ConfigManager.CConfigValue invite_title { get { return ConfigManager.Instance.GetConfigValue("invite_title"); } }
        public static ConfigManager.CConfigValue invite_message { get { return ConfigManager.Instance.GetConfigValue("invite_message"); } }
        public static ConfigManager.CConfigValue invite_image { get { return ConfigManager.Instance.GetConfigValue("invite_image"); } }
        public static ConfigManager.CConfigValue invite_deep_link { get { return ConfigManager.Instance.GetConfigValue("invite_deep_link"); } }
        public static ConfigManager.CConfigValue website_url { get { return ConfigManager.Instance.GetConfigValue("website_url"); } }
        public static ConfigManager.CConfigValue playstore_url { get { return ConfigManager.Instance.GetConfigValue("playstore_url"); } }
        public static ConfigManager.CConfigValue applestore_url { get { return ConfigManager.Instance.GetConfigValue("applestore_url"); } }
        public static ConfigManager.CConfigValue market_url { get { return ConfigManager.Instance.GetConfigValue("market_url"); } }
        public static ConfigManager.CConfigValue facebook_page { get { return ConfigManager.Instance.GetConfigValue("facebook_page"); } }
        public static ConfigManager.CConfigValue twitter_page { get { return ConfigManager.Instance.GetConfigValue("twitter_page"); } }
        public static ConfigManager.CConfigValue googleplus_page { get { return ConfigManager.Instance.GetConfigValue("googleplus_page"); } }
        public static ConfigManager.CConfigValue instagram_page { get { return ConfigManager.Instance.GetConfigValue("instagram_page"); } }
        public static ConfigManager.CConfigValue gamejolt_page { get { return ConfigManager.Instance.GetConfigValue("gamejolt_page"); } }
        public static ConfigManager.CConfigValue news_json { get { return ConfigManager.Instance.GetConfigValue("news_json"); } }
        public static ConfigManager.CConfigValue releases_json { get { return ConfigManager.Instance.GetConfigValue("releases_json"); } }
        public static ConfigManager.CConfigValue visible_console_button { get { return ConfigManager.Instance.GetConfigValue("visible_console_button"); } }
        public static ConfigManager.CConfigValue console_show_exceptions { get { return ConfigManager.Instance.GetConfigValue("console_show_exceptions"); } }
        public static ConfigManager.CConfigValue console_show_errors { get { return ConfigManager.Instance.GetConfigValue("console_show_errors"); } }
        public static ConfigManager.CConfigValue reward_show_seconds { get { return ConfigManager.Instance.GetConfigValue("reward_show_seconds"); } }


        public static ConfigManager.CConfigValue starting_gold { get { return ConfigManager.Instance.GetConfigValue("starting_gold"); } }
        public static ConfigManager.CConfigValue starting_chest { get { return ConfigManager.Instance.GetConfigValue("starting_chest"); } }
        public static ConfigManager.CConfigValue reward_gold_amount { get { return ConfigManager.Instance.GetConfigValue("reward_gold_amount"); } }
        public static ConfigManager.CConfigValue player_health { get { return ConfigManager.Instance.GetConfigValue("player_health"); } }
        public static ConfigManager.CConfigValue player_maxhealth { get { return ConfigManager.Instance.GetConfigValue("player_maxhealth"); } }
        public static ConfigManager.CConfigValue max_lootable_count { get { return ConfigManager.Instance.GetConfigValue("max_lootable_count"); } }
        public static ConfigManager.CConfigValue lootable_spawn_time { get { return ConfigManager.Instance.GetConfigValue("lootable_spawn_time"); } }
        public static ConfigManager.CConfigValue pirate_spawn_amount { get { return ConfigManager.Instance.GetConfigValue("pirate_spawn_amount"); } }
        public static ConfigManager.CConfigValue new_season_seconds { get { return ConfigManager.Instance.GetConfigValue("new_season_seconds"); } }
        public static ConfigManager.CConfigValue ports_good_count { get { return ConfigManager.Instance.GetConfigValue("ports_good_count"); } }

    }
}

[System.Serializable]
public class PointTutorialDict : Dict<Point2, Tutorial> { }

[System.Serializable]
public class IntChapterDict : Dict<int, Chapter> { }

[System.Serializable]
public class PublicPassedDict : Dict<Point2, PassedSubCh> { }

[System.Serializable]
public class EventIntDict : Dict<E, int> { }

[System.Serializable]
public class StringStringDict : Dict<string, string> { }

[System.Serializable]
public class StringIntDict : Dict<string, int> { }

[System.Serializable]
public class StringBoolDict : Dict<string, bool> { }

[System.Serializable]
public class StringFloatDict : Dict<string, float> { }

[System.Serializable]
public class StringHS : CHashSet<string> { }

[System.Serializable]
public class SpriteList : CList<UnityEngine.Sprite> { }

[System.Serializable]
public class PointHashSet : CHashSet<Point2> { }

[System.Serializable]
public class LongHashSet : CHashSet<long> { }

